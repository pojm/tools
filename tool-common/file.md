# common 使用说明
####5.file文件
```
private String basePath = System.getProperty("user.home")+"/Desktop/电话号码/";

// 使用方式一: 按行循环
List<String> lines = Lists.newArrayList();
        Paper.create().path(basePath+"1.txt").lineIterator((index,line) -> {
                    if(StringUtils.isBlank(line)){
                        return true;
                    }
                    if( !line.matches("1[0-9]{10}") ){
                        return true;
                    }
                    lines.add(line);
                    if(index % 1000000 == 0){
                        Paper.create().path(basePath+"3.txt").writeLines(lines);
                        lines.clear();
                        LogUtils.info(log,"已获取10000行数据,执行一次写入文件操作");
                    }
                    return true;
                });
// 使用方式二: 按行写入
Paper.create().path(basePath+"3.txt").writeLines(lines);

```