package com.bruce.tool.common.exception;

import com.bruce.tool.common.util.ClassUtils;
import com.bruce.tool.common.util.regex.RegexUtils;
import com.bruce.tool.common.util.string.StringUtils;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.UncategorizedSQLException;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Method;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.Objects;

/**
 * 异常工具类
 * @author : Bruce(刘正航) 上午10:21 2018/2/26
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ExceptionUtils {

	public static ModelAndView printErrorInfo(Throwable ex) {
		printStackTrace(ex);
		BaseResult result = gatherErrorInfos(ex);
		ModelAndView mav = new ModelAndView();
		mav.addObject("code",result.getCode());
		mav.addObject("message",result.getMessage());
		mav.setView(new MappingJackson2JsonView());
		return mav;
	}

	public static BaseResult printErrorInfos(Throwable ex) {
		printStackTrace(ex);
		return gatherErrorInfos(ex);
	}

	/**搜集错误信息**/
	private static BaseResult gatherErrorInfos(Throwable ex) {
		BaseResult result = new BaseResult();
		if( ex instanceof DuplicateKeyException){
			handleDuplicateKeyInfo(ex,result);
		}else if( ex instanceof MethodArgumentTypeMismatchException){
			handleArumentTypeExceptionInfo(ex,result);
		} else if( ex instanceof HttpRequestMethodNotSupportedException){
			handleRequestMethodExceptionInfo(ex,result);
		} else if( ex instanceof UncategorizedSQLException){
			handleSQLExceptionInfo(ex, result);
		} else if( ex instanceof MissingServletRequestParameterException){//必传参数异常
			handleRequiredBindException(ex,result);
		} else if( ex instanceof MissingPathVariableException) { //url路径中的参数校验异常
			handleRequiredPathException(ex, result);
		} else if( ex instanceof MethodArgumentNotValidException){
			handleMethodBindException(ex, result);
		} else if( ex instanceof BindException){//hibernate-validation异常
			handleBindException(ex,result);
		}else{
			handleBusinessExceptionInfo(ex, result);
		}
		return result;
	}

	/**打印堆栈异常**/
	public static void printStackTrace(Throwable e) {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		PrintStream pout = new PrintStream(out);
		e.printStackTrace(pout);
		String sw = new String(out.toByteArray());
		pout.close();
		try {
			out.close();
		} catch (Exception ex) {
			log.error("{}",ex);
		}
		log.error(sw);
	}

    /*
     ****************************************私有方法区*******************************************
                   _               _                           _    _                 _
                  (_)             | |                         | |  | |               | |
      _ __   _ __  _ __   __ __ _ | |_  ___   _ __ ___    ___ | |_ | |__    ___    __| |
     | '_ \ | '__|| |\ \ / // _` || __|/ _ \ | '_ ` _ \  / _ \| __|| '_ \  / _ \  / _` |
     | |_) || |   | | \ V /| (_| || |_|  __/ | | | | | ||  __/| |_ | | | || (_) || (_| |
     | .__/ |_|   |_|  \_/  \__,_| \__|\___| |_| |_| |_| \___| \__||_| |_| \___/  \__,_|
     | |
     |_|
     ****************************************私有方法区*******************************************
     */

    /**唯一主键冲突**/
	private static void handleDuplicateKeyInfo(Throwable ex, BaseResult result) {
		DuplicateKeyException unique = (DuplicateKeyException)ex;
		Throwable throwable = unique.getCause();
		if( throwable instanceof SQLIntegrityConstraintViolationException ){
			handleConstraintViolationInfo(throwable,result);
		}
	}

	/**处理唯一索引冲突的异常**/
	private static void handleConstraintViolationInfo(Throwable ex, BaseResult result) {
		SQLIntegrityConstraintViolationException unique = (SQLIntegrityConstraintViolationException)ex;
		String message = unique.getMessage();
		if( message.contains("Duplicate") ){
			message = "违反数据库唯一索引约束";
		}
		result.setCode(ErrorCode.SQL_UNIQUE_KEY_ERROR.getCode());
		result.setMessage(message);
	}

	/**参数类型错误**/
	private static void handleArumentTypeExceptionInfo(Throwable ex, BaseResult result) {
		MethodArgumentTypeMismatchException argumentTypeException = (MethodArgumentTypeMismatchException)ex;
		result.setCode(ErrorCode.REQUEST_PARAM_TYPE_ERROR.getCode());
		Class<?> requiredType = argumentTypeException.getRequiredType();
		String message = "参数类型错误:"+argumentTypeException.getName()
				+"(" + ((null != requiredType)?requiredType.getName():"null")
				+ "),实际的传入值为:"+argumentTypeException.getValue();
		result.setMessage(message);
	}

    /**请求方法不支持**/
	private static void handleRequestMethodExceptionInfo(Throwable ex, BaseResult result) {
		HttpRequestMethodNotSupportedException methodException = (HttpRequestMethodNotSupportedException)ex;
		result.setCode(ErrorCode.REQUEST_METHOD_ERROR.getCode());
		String message = "该方法不支持"+methodException.getMethod()+"请求";
		result.setMessage(message);
	}

	/**特殊SQL异常，返回值处理**/
	private static void handleSQLExceptionInfo(Throwable ex, BaseResult mav) {
		// 针对特殊符号，做特殊的提示信息:Caused by: java.sql.SQLException: Incorrect string value: '\xF0\x9F\x98\x91' for column 'remark' at row 1
		UncategorizedSQLException e = (UncategorizedSQLException)ex;
		String msg = e.getMessage();
		if( !StringUtils.isEmpty(msg) && msg.contains("Incorrect string value: '\\x") ){
			mav.setCode(ErrorCode.MYSQL_EMOJI_UNSUPPORT_CODE.getCode());
			mav.setMessage(ErrorCode.MYSQL_EMOJI_UNSUPPORT_CODE.getMessage());
		}else{
			handleUnExpectedExceptionInfo(ex, mav);
		}
	}


	/**参数校验异常捕获**/
	private static void handleRequiredBindException(Throwable ex, BaseResult mav) {
		MissingServletRequestParameterException bindException = (MissingServletRequestParameterException)ex;
		mav.setCode(ErrorCode.FORM_VALID_ERROR.getCode());
		String message = "缺少必传参数:" + bindException.getParameterName();
		mav.setMessage(message);
	}

	private static void handleRequiredPathException(Throwable ex, BaseResult mav) {
		MissingPathVariableException pathVariableException = (MissingPathVariableException) ex;
		mav.setCode(ErrorCode.FORM_VALID_ERROR.getCode());
		String message = "缺少必传参数:" + pathVariableException.getVariableName();
		mav.setMessage(message);
	}

	private static void handleMethodBindException(Throwable ex, BaseResult mav) {
		MethodArgumentNotValidException bindException = (MethodArgumentNotValidException)ex;
		handleBindError(mav, bindException.getBindingResult());
	}

	/**参数校验异常捕获**/
	private static void handleBindException(Throwable ex, BaseResult mav) {
		BindException bindException = (BindException)ex;
		handleBindError(mav, bindException.getBindingResult());
	}

	private static void handleBindError(BaseResult mav, BindingResult br) {
		if (br.hasFieldErrors()) {
			FieldError fieldError = br.getFieldError();
			if( null != fieldError && org.apache.commons.lang3.StringUtils.isNotBlank(fieldError.getDefaultMessage()) ){
				String error = fieldError.getDefaultMessage();
				mav.setMessage(error);
			}else{
				mav.setMessage(ErrorCode.FORM_VALID_ERROR.getMessage());
			}
			mav.setCode(ErrorCode.FORM_VALID_ERROR.getCode());
		}
	}

	/**业务异常统一处理**/
	private static void handleBusinessExceptionInfo(Throwable ex, BaseResult mav) {
		Object code = null;
		Method method = ClassUtils.getMethod(ex.getClass(),"getCode");
		try {
			if(Objects.nonNull(method)){
				code = method.invoke(ex);
			}
		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		}
		if( Objects.isNull(code) ){//为空,说明不是业务异常
			handleUnExpectedExceptionInfo(ex,mav);
		}else{
			mav.setCode(code);
			setMessage(ex, mav);
			setData(ex, mav);
		}
	}

	/**未定义系统异常统一处理**/
	private static void handleUnExpectedExceptionInfo(Throwable ex, BaseResult mav) {
		mav.setCode(ErrorCode.SYSTEM_ERROR.getCode());
		String message = ex.getMessage();
		Throwable cause = ex.getCause();
		if (Objects.nonNull(cause)) {
			message = ex.getCause().getMessage();
		}

		if(StringUtils.isBlank(message)){
			return;
		}

		if (RegexUtils.containChinese(message)) {
			mav.setMessage(message);
		} else {
			mav.setMessage(ErrorCode.SYSTEM_ERROR.getMessage());
		}
	}

	private static void setMessage(Throwable ex, BaseResult mav) {
		Object message = null;
		Method method = ClassUtils.getMethod(ex.getClass(),"getMessage");
		try {
			if(Objects.nonNull(method)){
				message = method.invoke(ex);
			}
        } catch (Exception e) {
			log.error("{}",e.getMessage()); // 一般只没有message这个属性
        }
		if( Objects.nonNull(message) ){
			mav.setMessage(String.valueOf(message));
		}
	}

	private static void setData(Throwable ex, BaseResult mav) {
		Object data = null;
		Method method = ClassUtils.getMethod(ex.getClass(),"getData");
		try {
			if( Objects.nonNull(method) ){
				data = method.invoke(ex);
			}
		} catch (Exception e) {
			log.error("{}",e.getMessage()); // 一般只没有data这个属性
		}
		if(Objects.nonNull(data)){
			mav.setData(data);//2016.06.05 因业务需要,在异常抛出时也可以带data返回
		}
	}

}
