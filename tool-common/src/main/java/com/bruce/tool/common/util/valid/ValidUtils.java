package com.bruce.tool.common.util.valid;

import com.bruce.tool.common.exception.BaseRuntimeException;
import com.bruce.tool.common.exception.ErrorCode;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 7:10 PM 2018/12/8
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ValidUtils {
    /**需要校验的对象**/
    public static <T> boolean valid(T obj) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<T>> violations = validator.validate(obj);
        if (violations != null && !violations.isEmpty()) {
            throw new BaseRuntimeException(ErrorCode.SYSTEM_ERROR,violations.iterator().next().getMessage());
        }
        return true;
    }
}
