package com.bruce.tool.common.util.trace;

import com.bruce.tool.common.util.LogUtils;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 功能 :
 * 生成流程串号,用于跟踪
 * @author : Bruce(刘正航) 下午8:24 2018/7/17
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TraceNoUtils {

    private static final SnowFlake SNOW_FLAKE = new SnowFlake(1, 1);

    public static String fetchTraceNo(){
        return String.valueOf(SNOW_FLAKE.nextId());
    }

    public static void main(String[] args) {
        LogUtils.info(log,fetchTraceNo());
    }
}
