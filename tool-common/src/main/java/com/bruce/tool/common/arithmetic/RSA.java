package com.bruce.tool.common.arithmetic;

import com.bruce.tool.common.exception.ExceptionUtils;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.crypto.Cipher;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Objects;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 23:13 2019-01-10
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RSA {

    private static final String KEY_ALGORITHM = "RSA";
    private static final String RSA_SIGNATURE_ALGORITHM = "SHA1withRSA";

    /**
     * 初始化密钥
     */
    public static KeyPair init() {
        try {
            KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance(KEY_ALGORITHM);
            keyPairGen.initialize(1024);
            return keyPairGen.generateKeyPair();
        } catch (Exception e) {
            ExceptionUtils.printStackTrace(e);
        }
        return null;
    }

    /**
     * 取得公钥
     */
    public static String publicKey(KeyPair keyPair) {
        if(Objects.isNull(keyPair) ){ return null; }
        return Base64.encode(keyPair.getPublic().getEncoded());
    }

    /**
     * 取得私钥
     */
    public static String privateKey(KeyPair keyPair) {
        if(Objects.isNull(keyPair)){ return null; }
        return Base64.encode(keyPair.getPrivate().getEncoded());
    }

    /**
     * 用公钥加密
     * @param data 元数据字节码
     * @param base64PublicKey base64编码过的公钥(publicKey)
     * @return 加密结果
     */
    public static byte[] encrypt(byte[] data, String base64PublicKey) {
        try {
            // 对公钥解密
            byte[] keyBytes = Base64.decode(base64PublicKey);
            // 取得公钥
            X509EncodedKeySpec x509KeySpec = new X509EncodedKeySpec(keyBytes);
            KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
            Key publicKey = keyFactory.generatePublic(x509KeySpec);
            // 对数据加密
            Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);

            return cipher.doFinal(data);
        } catch (Exception e) {
            ExceptionUtils.printStackTrace(e);
        }
        return new byte[0];
    }

    /**
     * 用私钥解密
     * @param data 元数据字节码
     * @param base64PrivateKey base64编码过的私钥(privateKey)
     * @return 解码结果
     */
    public static byte[] decrypt(byte[] data, String base64PrivateKey) {
        try {
            // 对密钥解密
            byte[] keyBytes = Base64.decode(base64PrivateKey);

            // 取得私钥
            PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(keyBytes);
            KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
            Key privateKey = keyFactory.generatePrivate(pkcs8KeySpec);

            // 对数据解密
            Cipher cipher = Cipher.getInstance(keyFactory.getAlgorithm());
            cipher.init(Cipher.DECRYPT_MODE, privateKey);

            return cipher.doFinal(data);
        } catch (Exception e) {
            ExceptionUtils.printStackTrace(e);
        }
        return new byte[0];
    }

    /**
     * 签名-私钥签名
     * @param data 元数据字节码
     * @param base64PrivateKey base64编码过的私钥
     * @return 签名结果
     */
    public static byte[] sign(byte[] data, String base64PrivateKey){
        try {
            // Base64 --> Key
            byte[] bytes = Base64.decode(base64PrivateKey);
            PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(bytes);
            KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
            PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
            // Sign
            Signature signature = Signature.getInstance(RSA_SIGNATURE_ALGORITHM);
            signature.initSign(privateKey);
            signature.update(data);
            return signature.sign();
        } catch (Exception e) {
            ExceptionUtils.printStackTrace(e);
        }
        return new byte[0];
    }

    /**
     * 验签-公钥验签
     *
     * @param data 需要验签的字符串
     * @param base64PublicKey base64编码之后的公钥
     * @param sign 需要验证的签名
     * @return
     */
    public static boolean verify(byte[] data, String base64PublicKey, String sign) {
        try {
            // Base64 --> Key
            byte[] bytes = Base64.decode(base64PublicKey);
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(bytes);
            KeyFactory keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
            PublicKey publicKey = keyFactory.generatePublic(keySpec);
            // verify
            Signature signature = Signature.getInstance(RSA_SIGNATURE_ALGORITHM);
            signature.initVerify(publicKey);
            signature.update(data);
            return signature.verify(Base64.decode(sign));
        } catch (Exception e) {
            ExceptionUtils.printStackTrace(e);
        }
        return false;
    }

}
