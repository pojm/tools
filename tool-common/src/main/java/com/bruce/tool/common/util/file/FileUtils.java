package com.bruce.tool.common.util.file;

import com.bruce.tool.common.arithmetic.Base64;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

/**
 * 功能 :
 * 1.字节流-转byte数组
 * @author : Bruce(刘正航) 9:39 上午 2020/1/8
 */
@Slf4j
public class FileUtils {

    /**流->数组**/
    public static byte[] transfer(InputStream in){
        try(ByteArrayOutputStream baos = new ByteArrayOutputStream()){
            int length = 0;
            byte[] buffer = new byte[2048];
            while ((length = in.read(buffer))!= -1){
                baos.write(buffer,0,length);
            }
            return baos.toByteArray();
        } catch (IOException e) {
            log.error("{}",e.getMessage(),e);
        } finally {
            IOUtils.closeQuietly(in);
        }
        return new byte[0];
    }

    /**根据流获取文件类型**/
    public static String fetchType(byte[] bytes){
        String header = Base64.byte2Hex(bytes,0,20);
        FileTypeEnum byCode = FileTypeEnum.getByCode(header);
        if(Objects.nonNull(byCode)){
            return byCode.getType();
        }
        return null;
    }
}
