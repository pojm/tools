package com.bruce.tool.common.exception;

import lombok.Data;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 12:34 PM 2018/11/13
 */
@Data
public class BaseResult {
    protected Object code;
    protected String message;
    protected Object data;
}
