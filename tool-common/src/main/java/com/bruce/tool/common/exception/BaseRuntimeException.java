package com.bruce.tool.common.exception;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.function.Supplier;

/**
 * 所有异常类的父类
 * @author liuzhenghang
 * @author : Bruce(刘正航) 上午10:21 2018/2/26
 */
@Data
@NoArgsConstructor // 默认无参构造函数
public class BaseRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	protected Object code;
	protected String message;
	protected Object data;

	public BaseRuntimeException(Throwable e) {
		super(e);
	}

	public BaseRuntimeException(Supplier<Throwable> e) {
		super(e.get());
	}

	public BaseRuntimeException(Object code,String message) {
		this.code = code;
		this.message = message;
	}

	public BaseRuntimeException(Object code,String message,Throwable e) {
		super(e);
		this.code = code;
		this.message = message;
	}

	public BaseRuntimeException(Object code,String message,Object data) {
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public BaseRuntimeException(Object code,String message,Object data,Throwable e) {
		super(e);
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public BaseRuntimeException(Supplier<Object> code, Supplier<String> message) {
		this.code = code.get();
		this.message = message.get();
	}

	public BaseRuntimeException(Supplier<Object> code, Supplier<String> message,Supplier<Throwable> e) {
		super(e.get());
		this.code = code.get();
		this.message = message.get();
	}

	public BaseRuntimeException(Supplier<Object> code, Supplier<String> message, Object data) {
		this.code = code.get();
		this.message = message.get();
		this.data = data;
	}

	public BaseRuntimeException(Supplier<Object> code, Supplier<String> message, Supplier<Object> data,Supplier<Throwable> e) {
		super(e.get());
		this.code = code.get();
		this.message = message.get();
		this.data = data.get();
	}

}
