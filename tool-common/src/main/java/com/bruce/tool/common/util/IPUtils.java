package com.bruce.tool.common.util;

import com.bruce.tool.common.exception.ExceptionUtils;
import com.bruce.tool.common.util.string.StringUtils;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 下午6:04 2018/8/20
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class IPUtils {

    private static final String UNKNOWN = "unknown";
    private static final String LOCAL = "127.0.0.1";

    /**获取本机-局域网IP**/
    public static String local() {
        try {
            InetAddress addr = InetAddress.getLocalHost();
            return addr.getHostAddress();
        } catch (UnknownHostException e) {
            ExceptionUtils.printStackTrace(e);
        }
        //获取本机IP
        return LOCAL;
    }

    /**获取请求来源IP**/
    public static String request(HttpServletRequest request){
        String ipAddress = request.getHeader("x-forwarded-for");
        if(StringUtils.isBlank(ipAddress) || UNKNOWN.equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("Proxy-Client-IP");
        }
        if(StringUtils.isBlank(ipAddress) || UNKNOWN.equalsIgnoreCase(ipAddress)) {
            ipAddress = request.getHeader("WL-Proxy-Client-IP");
        }
        if(StringUtils.isBlank(ipAddress) || UNKNOWN.equalsIgnoreCase(ipAddress)){
            ipAddress = request.getRemoteAddr();
        }
        //对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
        if( StringUtils.isBlank(ipAddress) ){
            return ipAddress;
        }
        if(ipAddress.equals(LOCAL) || ipAddress.equals("0:0:0:0:0:0:0:1")){
            ipAddress = local();//根据网卡取本机配置的IP
        }
        if( ipAddress.length() <= 15 ){
            return ipAddress;
        }
        if(ipAddress.contains(",")){
            ipAddress = ipAddress.substring(0,ipAddress.indexOf(','));
            return ipAddress;
        }
        return ipAddress;
    }

}