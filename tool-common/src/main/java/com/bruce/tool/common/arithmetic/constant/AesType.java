package com.bruce.tool.common.arithmetic.constant;

/**
 * 功能 :
 * 推荐使用GCM加密方式
 * @author : Bruce(刘正航) 12:35 2019-02-19
 */
public enum AesType {
    ECB,
    CBC,
    GCM
}
