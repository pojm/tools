package com.bruce.tool.common.enums;

import com.bruce.tool.common.util.string.EnumHandler;
import com.bruce.tool.common.util.string.JsonUtils;
import org.junit.Test;

/**
 * 功能 :
 * @author : Bruce(刘正航) 09:52 2019-08-01
 */
public class EnumHandlerTest {

    @Test
    public void testEnumOne(){
        System.out.println(JsonUtils.objToStr(EnumHandler.toList(KeyGroup.values())));
        System.out.println(JsonUtils.objToStr(EnumHandler.toList(KeyGroup.values(),"name")));
        System.out.println(JsonUtils.objToStr(EnumHandler.toList(KeyDemo.values())));
    }
}
