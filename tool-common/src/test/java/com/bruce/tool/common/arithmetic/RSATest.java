package com.bruce.tool.common.arithmetic;

import com.bruce.tool.common.util.LogUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.security.KeyPair;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 14:56 2019-02-19
 */
@Slf4j
public class RSATest {

    /**生成签名**/
    @Test
    public void createKeys() {
        KeyPair keyPair = RSA.init();
        LogUtils.info(log,RSA.publicKey(keyPair));
        LogUtils.info(log,RSA.privateKey(keyPair));
    }

    /**签名和验签**/
    @Test
    public void signAndVerify() {
        String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt71pJokE5OonSSMinE8ZD0Vpp6gZve2GOOHnFOcn9ejhJiF49YB2FpsB7CAsBKxZzkanNZLEyAHbejRoAN13FOMnTQ9GWOjrWa9piYsqAZP1/qeb0HJdcigMFSxicBHfPKkwVXAYx5wZ+DTcS5tuQ6XLMMu2TehB3mojbIlPl0sWWMAOsNOf2+1lvubOWERv9oF2Iz6o720QzvKckOpKXv/hJPN37Bqsxc1bcCWEiTi0Dbs71sCHfGDK1PjBuyWlVOx+RAZ39UOJjFiemnqhfZUexJ4NPGDdFCEn1I4qmTnW7nU+SZ5Isg7/a1ZL3499MbGYJyRoX6u1M4+xUQ0LMwIDAQAB";
        String privateKey = "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC3vWkmiQTk6idJIyKcTxkPRWmnqBm97YY44ecU5yf16OEmIXj1gHYWmwHsICwErFnORqc1ksTIAdt6NGgA3XcU4ydND0ZY6OtZr2mJiyoBk/X+p5vQcl1yKAwVLGJwEd88qTBVcBjHnBn4NNxLm25Dpcswy7ZN6EHeaiNsiU+XSxZYwA6w05/b7WW+5s5YRG/2gXYjPqjvbRDO8pyQ6kpe/+Ek83fsGqzFzVtwJYSJOLQNuzvWwId8YMrU+MG7JaVU7H5EBnf1Q4mMWJ6aeqF9lR7Eng08YN0UISfUjiqZOdbudT5JnkiyDv9rVkvfj30xsZgnJGhfq7Uzj7FRDQszAgMBAAECggEAT0QuhcpcyyjdE+dNeiOBix4F66dQm6hJ3GZQm6rxiVwTLbEJDtiBmVT76lMiyk1UXPIMFsefjy2F8t2C9MSWJ/OrM8pgLeEmD0XmgMkBsTaUzGyggrej6qiuNcA1I2YCaZgP2vwGJK8KYRLk6tU+J37x/rYTit6k8//UEnHWXUzyNmwDy1D5YUIFbiXBJ9tj7G1eIOH31Ssb9Ias85nbkwKQsbVWs/HFM4vG5eNAzNHsdKduQFuWUnlLJ6h2fFuQZl+SEMOPAYfcYBWgOMh2WfAdaRQ7dZbEv3p+bEgoL3Rcn/dLjOuzjh7FcfdQ7mmSRWim69qbWDtJx/qF051A8QKBgQDxvXEQsrtow1gZLX38Mz+EaJxRKNmHzMKmexzO0+S5k4DBIJdi+/f7vMKWMlbseOgrubq9CgxFl4KB/aavXkMoOwSG3V6neOHPgLpl0r5boxVz4N7EMF7e76YbOtXqNE99cecoEw7dyDspokYdSHUW8exYP3qTqIIWFzfpoVg+GQKBgQDClBlcJejfIPR+H0Zdj7jx93EPTJdywtwYv8KO8UAQzPZQKGDlJTotg8brp2sPVHY5ZznPxs+h3OdcTxIghEovEcZ/KYphS3z9rKv5EYrJ1FG7A5q8d5nqmbnl3fV0y+pSWTvd1l8GknvtnEEYgjNbT7UOm3SPoWq0qH7zrmklKwKBgFG4lB9r/JT27LJeQafff0p4vtdk8tzIqMj095En6rudlLo1FVheC8ArWLDDxcTdPseBWxKK8gkYpgLhx9ajDBrukKIy9cuxvsySHPFNJBMAd8EbtNMn8vp0k3fDFQu+sVycA+P1RWJOTUnO1NsTYpnmINrYYszF+2wSX1F4vMcJAoGAHo0GVlOVdsND4DGk6LfSQBur4s0R0nEKAdDKCYuTlY+49OkCHbh0pATWzNVil+uZDHZKu3bpzu5SXUoE/JoI088krX6mPZSOsX3VGqxIcFwn4Qc12nWk8xLj/1WUXpxN1FQydChejzecwbgQ+Hoo+iNPuFVsPckIagkytawi0jUCgYEA4uRXMsrp2UupPL2JcuDDNdofFqhOgzgXCCjxa9dmXWQgWIinmL0cp3MeP77WTDWhBbx431gNG3B8QL8SGUZp8f6ZlV0144mXFzPbWw++/EVbacqIqOd31WSiIrScalRaB3EJkLf2GXFRF9O9OZbQjpEGbufb+CW/wBNWN0/1TbE=";
        String payload = "a=1&b=2&c=3";
        byte[] sign = RSA.sign(payload.getBytes(),privateKey);
        LogUtils.info(log,Base64.encode(sign));
        boolean verify = RSA.verify(payload.getBytes(),publicKey,Base64.encode(sign));
        LogUtils.info(log,verify);
    }

    /**加密解密**/
    @Test
    public void encryptAndDecrypt() {
        String publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCVf5PG0jtXFp9W8evKSJi1EBIEyB/60OcPOx7IQ20Yix9KJ5g2DivGnEjfFyNiP55kjNsyFVGkTnW2IVW/oSj99WV+rekui1MPfGaQNFXYK9ankfPZTA1/Vbeymb86ZAZqA8lI+NRViLGkgVixSfNc/oih8raC92ymab6tVqVFDQIDAQAB";
        String privateKey = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAJV/k8bSO1cWn1bx68pImLUQEgTIH/rQ5w87HshDbRiLH0onmDYOK8acSN8XI2I/nmSM2zIVUaROdbYhVb+hKP31ZX6t6S6LUw98ZpA0Vdgr1qeR89lMDX9Vt7KZvzpkBmoDyUj41FWIsaSBWLFJ81z+iKHytoL3bKZpvq1WpUUNAgMBAAECgYBt0zAromvneX4K8GRI0XYlpa2nB6G+r1LfNI5Tjn0Jx8JvxpCiPVzZZhx+j0/2MEhbE8M/krvMWbtN1kVZJrqmXspB+OygCtKmHpdeSwjoBRP4TajEOrgS4yTryv6HYzKxmgz33aw2nXaTX5C2qPZpU9wf36dnF5nzV5wk7AF8vQJBAMsd2jDGUH1RoR1OMlvhwFO06gTbjFrl/ZWQ0WXNdeW8rPO77lJtSiifRV5aywkruoBmpz7dRGMpymXbYY+LH3cCQQC8a/SXr/5GZZp/izx3hMHWZf8Hrc0xJsD+OK1L0TaTqbzTJSY6nEIWHj73aTYdZ1UExNlGJt9lCG7+AR+BXoibAkEAx38lm/xceAnh9fek7Kv5i/3IUEcXPvxgKjPYB2Za4u+C683s0Ra43Nc6eecxPmutvYmVwN/w2Hjma06jLyqVFwJBAIYs5i+CjzL4NW3v6+48ZoBTf6mrNXxz2WjvWVCtOg0rCSDeyntgPJtdjH9It9V2eQ99Ui/njJt4xvkwOYw5klMCQEacgPDAMG/kVkv2djR58Z2DIt8ji4KNrGDV2jZWmeaw/mZheAknFq3TrqAhwufER7Qtq3JWQuUe8XfGsJREWq4=";
        String payload = "123456";
        byte[] encrypts = RSA.encrypt(payload.getBytes(),publicKey);
        LogUtils.info(log,Base64.encode(encrypts));
        byte[] decrypts = RSA.decrypt(Base64.decode(Base64.encode(encrypts)),privateKey);
        LogUtils.info(log,new String(decrypts));
    }
}
