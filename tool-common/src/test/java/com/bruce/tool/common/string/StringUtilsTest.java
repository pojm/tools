package com.bruce.tool.common.string;

import com.bruce.tool.common.util.LogUtils;
import com.bruce.tool.common.util.string.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 01:14 2019-02-20
 */
@Slf4j
public class StringUtilsTest {

    @Test
    public void formatJson(){
        String content = "{\"name\":\"zhangsan\",\"age\":23,\"email\":\"chentging@aliyun.com\"}";
        LogUtils.info(log,StringUtils.formatJson(content));
    }
}
