package com.bruce.tool.common;

import com.bruce.tool.common.util.IPUtils;
import com.bruce.tool.common.util.LogUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 22:51 2019-02-19
 */
@Slf4j
public class IPUtilsTest {

    @Test
    public void main(){
        long begin = System.currentTimeMillis();
        LogUtils.info(log,"本机的内网IP是：" + IPUtils.local()+",耗时:"+(System.currentTimeMillis()-begin));
    }
}
