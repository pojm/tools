package com.bruce.tool.pay.wechat;

import com.bruce.tool.common.util.SpringBeanUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 下午2:23 2018/1/3
 */
@SpringBootApplication
public class WechatPayApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(WechatPayApplication.class, args);
        SpringBeanUtils.setApplicationContext(context);
    }

}
