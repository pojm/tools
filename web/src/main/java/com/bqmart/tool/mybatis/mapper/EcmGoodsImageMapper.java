package com.bqmart.tool.mybatis.mapper;

import com.bqmart.tool.mybatis.domain.EcmGoodsImage;
import com.bruce.tool.mybatis.orm.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface EcmGoodsImageMapper extends BaseMapper<EcmGoodsImage> {
}