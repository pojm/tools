package com.bqmart.tool.mybatis;

import java.util.List;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 14:07 2019-04-26
 */
public interface DataHelper<T> {

    List<T> select();
}
