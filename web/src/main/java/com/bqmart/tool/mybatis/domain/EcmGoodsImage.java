package com.bqmart.tool.mybatis.domain;

import com.bruce.tool.mybatis.orm.annotation.Column;
import com.bruce.tool.mybatis.orm.annotation.Id;
import com.bruce.tool.mybatis.orm.annotation.Table;
import lombok.Data;
/**
 *
 */
@Data
@Table("ecm_goods_image")
public class EcmGoodsImage {
    /**
     * 
     */
    @Id("image_id")
    private Integer imageId;

    /**
     * 
     */
    @Column("parent_id")
    private Integer parentId;

    /**
     * 
     */
    @Column("goods_id")
    private Integer goodsId;

    /**
     * 倍全标准库goods_id
     */
    @Column("normal_goods_id")
    private Integer normalGoodsId;

    /**
     * sky_goods 的id
     */
    @Column("sky_goods_id")
    private Integer skyGoodsId;

    /**
     * 
     */
    @Column("image_url")
    private String imageUrl;

    /**
     * 
     */
    @Column("thumbnail")
    private String thumbnail;

    /**
     * 
     */
    @Column("sort_order")
    private Integer sortOrder;

    /**
     * 
     */
    @Column("file_id")
    private Integer fileId;

    /**
     * 是否为默认图片
     */
    @Column("if_default")
    private Boolean ifDefault;

    /**
     * 是否为标准库源图片     1 不是  0 是
     */
    @Column("if_goods_resource")
    private Boolean ifGoodsResource;

    /**
     * 是否曾作为主图审核通过
     */
    @Column("if_pass")
    private Boolean ifPass;

    /**
     * 0否1是自动添加的主图
     */
    @Column("if_delete")
    private Boolean ifDelete;

    /**
     * 1:首图，2：规格图，3：细节图，4：实物图，5：场景图
     */
    @Column("type")
    private Integer type;
}