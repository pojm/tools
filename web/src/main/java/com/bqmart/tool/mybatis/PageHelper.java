package com.bqmart.tool.mybatis;

//import com.giants.common.tools.Page;
//import org.apache.commons.lang.math.NumberUtils;

//import java.util.List;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 13:49 2019-04-26
 */
//public class PageHelper<D> extends Page<D> {
//
//    private PageHelper(){
//    }
//
//    public static PageHelper create(){
//        return new PageHelper<>();
//    }
//
//    public static PageHelper create(QueryCond queryCond){
//        PageHelper pageHelper =  new PageHelper<>();
//        pageHelper.setPageNo(queryCond.getPageNo());
//        pageHelper.setPageSize(queryCond.getPageSize());
//        return pageHelper;
//    }
//
//    public PageHelper init(QueryCond queryCond){
//        this.setPageNo(queryCond.getPageNo());
//        this.setPageSize(queryCond.getPageSize());
//        return this;
//    }
//
//    public PageHelper<D> execute(CountHelper countHelper,DataHelper<D> dataHelper){
//        this.setTotal(countHelper.count().intValue());
//        if(NumberUtils.INTEGER_ZERO.equals(this.getTotal())){
//            return this;
//        }
//        this.setResults(dataHelper.select());
//        return this;
//    }
//
//    public static void main(String[] args) {
//        QueryCond queryCond = new QueryCond<>();
//        Page page = PageHelper.create().init(queryCond).execute(new CountHelper() {
//            @Override
//            public Long count() {
//                return null;
//            }
//        }, new DataHelper() {
//            @Override
//            public List select() {
//                return null;
//            }
//        });
//        System.out.println(page);
//    }
//}
