package com.bqmart.tool.mybatis.domain;

import com.bruce.tool.mybatis.orm.annotation.Column;
import com.bruce.tool.mybatis.orm.annotation.Id;
import com.bruce.tool.mybatis.orm.annotation.Table;
import lombok.Data;

import java.io.Serializable;

/**
 * 设备字典表 
 */
@Data
@Table("per_equipment_dict")
public class EquipmentDict implements Serializable {
    /**
     * 
     */
    @Id("id")
    private Integer id;

    /**
     * 参数代码
     */
    @Column("key")
    private String key;

    /**
     * 参数说明
     */
    @Column("name")
    private String name;

    /**
     * 参数值:默认值
     */
    @Column("default_value")
    private String defaultValue;

    /**
     * 范围值:最小值
     */
    @Column("scope_low")
    private String scopeLow;

    /**
     * 
     */
    @Column("scope_high")
    private String scopeHigh;

    /**
     * 参数备注,参数说明
     */
    @Column("note")
    private String note;

    /**
     * 参数分组:0温度,1水位,2继电器开关,3故障代码,4设备状态
     */
    @Column("key_group")
    private Integer keyGroup;

    /**
     * 参数类型:0运行参数,1界面参数,2系统参数
     */
    @Column("key_type")
    private Integer keyType;

    /**
     * 设备型号:控制柜有型号,机组和机器的参数暂无型号区分
     */
    @Column("device_model_no")
    private String deviceModelNo;

    /**
     * 设备类型:控制柜/机组/主从机器
     */
    @Column("device_type")
    private Integer deviceType;

    /**
     * 读写状态:0只读,1可读写
     */
    @Column("readwrite")
    private Integer readwrite;

    /**
     * 参数顺序值,控制参数在终端展示的顺序
     */
    @Column("order")
    private Integer order;
}