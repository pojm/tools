package com.bqmart.tool.mybatis;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 14:03 2019-04-26
 */
public interface CountHelper {
    Long count();
}
