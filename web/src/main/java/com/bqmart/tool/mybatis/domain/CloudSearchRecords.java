package com.bqmart.tool.mybatis.domain;

import com.bruce.tool.mybatis.orm.annotation.Column;
import com.bruce.tool.mybatis.orm.annotation.Id;
import com.bruce.tool.mybatis.orm.annotation.Table;
import lombok.Data;
/**
 *
 */
@Data
@Table("cloud_search_records")
public class CloudSearchRecords {
    /**
     * 主键id
     */
    @Id("id")
    private Integer id;

    /**
     * 检索关键词
     */
    @Column("keyword")
    private String keyword;

    /**
     * 用户id
     */
    @Column("cloud_user_id")
    private Integer cloudUserId;

    /**
     * 修改时间(第一次添加为新增时间)
     */
    @Column("update_time")
    private Integer updateTime;

    /**
     * 是否删除 0:未删除, 非0:删除
     */
    @Column("is_delete")
    private Integer isDelete;
}