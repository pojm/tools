package com.bqmart.tool.mybatis.mapper;

import com.bqmart.tool.mybatis.domain.AdminMenu;
import com.bruce.tool.mybatis.orm.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 01:12 2019-01-23
 */
@Mapper
@Component
public interface AdminMenuMapper extends BaseMapper<AdminMenu> {
}
