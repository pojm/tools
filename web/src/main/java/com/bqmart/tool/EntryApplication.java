package com.bqmart.tool;

import com.bruce.tool.common.util.SpringBeanUtils;
import com.bruce.tool.controller.annotation.UseRequestLimit;
import com.bruce.tool.mail.annotation.UseEmail;
import com.bruce.tool.mq.aliyun.annotation.UseAliyunMQ;
import com.bruce.tool.netty.socket.annotation.UseNettySocket;
import com.bruce.tool.web.filter.annotation.EnableExceptionHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ApplicationContext;

/**
 * 功能 :
 * 测试web项目入口
 * @author : Bruce(刘正航) 下午9:42 2018/1/1
 */
@UseEmail
@UseAliyunMQ
@UseNettySocket
@UseRequestLimit
//@EnableJsonResult
@EnableExceptionHandler
@ServletComponentScan
@SpringBootApplication
public class EntryApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(EntryApplication.class, args);
        SpringBeanUtils.setApplicationContext(context);
    }
}
