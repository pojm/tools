package com.bqmart.tool.web.form;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 功能 :
 * 公用消息对象
 * 1.消息来源(哪个项目,哪个业务模块,谁发送的消息,发送的什么类型的消息),这个需要打印到日志中,方便问题跟踪查询
 * 2.消息体(消息ID,消息内容,消息发送时间)
 * 3.消息ID,需要在发送消息的反馈中记录,方便线上问题跟踪(消息ID+业务标志可以更方便跟踪问题)
 * 4....
 * @author : Bruce(刘正航) 上午10:42 2018/2/26
 */
@Data
public class MessageInfo {

    /**阿里云消息实际的内容*/
    @NotNull(message = "缺少必传参数:消息内容")
    private String body;

}
