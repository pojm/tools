package com.bqmart.tool.web.handler;

import com.bruce.tool.controller.handler.ControllerAspect;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.context.annotation.Configuration;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 下午3:52 2018/5/4
 */
@Slf4j
@Aspect
@Configuration
@ControllerAspect.Form(packages = {
        "com.bqmart.tool"
})
public class ControllerHandler extends ControllerAspect {

    @Around("execution(* com.bqmart.tool.web.*.*Controller.*(..))")
    public Object deepestAround(ProceedingJoinPoint pjp) throws Throwable {
        return super.deeperAround(pjp);
    }

    @Override
    @Around("execution(* com.bqmart.tool.web.*Controller.*(..))")
    public Object deeperAround(ProceedingJoinPoint pjp) throws Throwable {
        return super.deeperAround(pjp);
    }
}
