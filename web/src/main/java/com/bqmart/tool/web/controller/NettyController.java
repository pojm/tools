package com.bqmart.tool.web.controller;

import com.bruce.tool.common.util.string.JsonUtils;
import com.bruce.tool.controller.handler.Result;
import com.bruce.tool.netty.socket.core.Message;
import com.bruce.tool.netty.socket.core.SocketClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 11:37 2019-03-10
 */
@Slf4j
@RestController
@RequestMapping("/netty")
public class NettyController {

    @Autowired
    private SocketClient client;

    @RequestMapping("/connect")
    public String connect(){
        client.addListener(message -> {
            System.out.println(message);
        });
        client.run();
        return Result.success();
    }

    @RequestMapping("/send")
    public String send(String message){
        Message useMessage = Message.builder()
                .from(client.getClientInfo())
                .body(message)
                .build();
        client.send(JsonUtils.objToStr(useMessage));
        return Result.success(message);
    }
}
