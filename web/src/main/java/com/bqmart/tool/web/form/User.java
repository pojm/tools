package com.bqmart.tool.web.form;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 下午3:29 2018/1/3
 */
@Data
public class User implements Serializable{
    private String name;
    private Integer age;
    private String email;
    @DateTimeFormat(pattern = "YYYY-MM-dd")
    private Date startDate;

    public User() {
    }

    public User(String name, Integer age, String email) {
        this.name = name;
        this.age = age;
        this.email = email;
    }
}
