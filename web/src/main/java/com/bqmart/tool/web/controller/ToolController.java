package com.bqmart.tool.web.controller;

import com.bqmart.tool.mybatis.domain.EcmGoodsImage;
import com.bqmart.tool.mybatis.mapper.EcmGoodsImageMapper;
import com.bqmart.tool.web.form.Interface2;
import com.bruce.tool.cache.redis.util.RedisUtils;
import com.bruce.tool.common.exception.BaseRuntimeException;
import com.bruce.tool.common.exception.ErrorCode;
import com.bruce.tool.common.util.LogUtils;
import com.bruce.tool.common.util.string.JsonUtils;
import com.bruce.tool.controller.handler.Result;
import com.bruce.tool.mail.core.MailAdapter;
import com.bruce.tool.office.excel.constant.Version;
import com.bruce.tool.office.excel.core.Excels;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 功能 :
 * 消息队列,调用入口
 * 1.根据不同的标识,路由到不同的业务消息模块
 * 2.传入业务标识,用于打印日志,方便跟踪业务消息流向
 * 3.所有的消息队列配置,放置到同一个地方;后期期望实现统一管理;
 * @author : Bruce(刘正航) 上午10:21 2018/2/26
 */
@Slf4j
@Controller
@RequestMapping("/tool")
public class ToolController {

    @Autowired
    private MailAdapter mailAdapter;

    @Autowired
    private EcmGoodsImageMapper ecmGoodsImageMapper;

    private List<Object> datas = Lists.newArrayList();

    @RequestMapping(value = "/sendEmail" ,method = RequestMethod.POST)
    @ResponseBody
    public String sendEmail(@Valid @RequestParam(required = false) String traceNo,@RequestParam String body) {
        for ( int i = 0; i< 10; i++ ) {
            Map data = Maps.newHashMap();
            data.put("name","测试测试测试"+i);
            data.put("params","https://blog.csdn.net/gaolu/article/details/52708177导入模板格式固定，不得删除标题行，不得对标题行做任何编辑"+i);
            data.put("paramType",i % 2);
            data.put("url","https://blog.csdn.net/gaolu/article/details/52708177"+i);
            data.put("requestType","POST"+i);
            data.put("expectCode","0"+i);
            data.put("result","展示的结果"+i);
            data.put("price",(3.45689 * i));
            datas.add(data);
        }
        // 说明:导出的时候,不管data是List<Map>还是List<Class>,都能够用Class的方式导出
        byte[] bytes = Excels.Export()
                .config(Version.XLS)
                .addSheet(datas,Interface2.class,"前端进度","操作说明：\n" +
                        "1、导入模板格式固定，不得删除标题行，不得对标题行做任何编辑，不得插入、删除标题列，不得调整标题行和列的位置。\n" +
                        "2、导入模板内容仅包括：楼号、单元/楼层、房间号、房间面积、房东姓名、房东手机号、房东身份证号码，7项，不得任意添加其他导入项目，所有项均为必填项。\n" +
                        "3、【楼号】：支持中文、数字和英文，如果是社区，需要输入“11号楼”或“11栋”之类的内容，如果是写楼楼，可以输入“A座”之类的内容； 但无论输入什么内容，必须所有内容统一，否则为显示为2个楼号；\n" +
                        "4、【单元/楼层】【房间号】：仅支持数字和英文；   最终展示的结果为是“5#402”； 如果是社区，建议是“单元号#房间号”，如果是楼层，建议是“楼层#房间号”；\n" +
                        "4、【房间面职】：主要为了区分房间的户型，不需要精确输入房间面积；\n" +
                        "5、【房东姓名】：支持汉字、英文、数字；【房东手机号】：仅支持11位阿拉伯数字，数字前后、之间不得有空格或其他字符；   【房东身份证号】：身份证号码必须是正确的身份证号，错误的号码将无法上传；\n" +
                        "6、导入模板各项内容不符合编辑规则的，导入时，系统将自动跳过，并计为导入失败数据；")
                .toBytes();

        mailAdapter.addTo("nichiailzh@163.com")
                .setSubject("测试邮件")
                .addByteAttachment("测试邮件",bytes)
                .appendToContent(true)
                .send();
        return Result.success();
    }

    @RequestMapping(value = "/goodsInfo" ,method = RequestMethod.POST)
    @ResponseBody
    public String goodsInfo(@Valid @RequestParam String traceNo,@RequestParam String body) {
        EcmGoodsImage info = ecmGoodsImageMapper.findById(EcmGoodsImage.class, 110);
        return Result.success(info);
    }

    @ResponseBody
    @RequestMapping(value = "/retry" ,method = RequestMethod.POST)
    public String retry(HttpServletRequest request) {
        String uri = request.getRequestURI();
        Integer count = RedisUtils.get(uri);
        if( Objects.isNull(count) ){ count = 5; }
        LogUtils.info(log,"请求一次,count={}", count);
        if( count >= 3 ){
            throw new BaseRuntimeException(ErrorCode.SYSTEM_ERROR.getCode(),"测试异常");
        }
        count--;
        RedisUtils.set(uri,count);
        return Result.failure("500","HAHA");
    }

    @ResponseBody
    @RequestMapping(value = "/upload" ,method = RequestMethod.POST)
    public String upload(HttpServletRequest request, String name, MultipartFile file, MultipartFile upload) throws IOException {
        if(Objects.nonNull(file) ){
            LogUtils.info(log,request.getContentType());
            LogUtils.info(log,file.getName());
            LogUtils.info(log,JsonUtils.objToStr(Excels.Import().config(file.getInputStream()).addSheet(Interface2.class).executeForData()));
        }else{
            LogUtils.info(log,"文件未获取到");
        }
        return Result.success();
    }
}
