package com.bruce.tool.web;

import com.bruce.tool.office.excel.annotation.Header;
import lombok.Data;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 下午8:33 2018/5/9
 */
@Data
public class GoodsInfo {

    @Header(name = "商品编号",format = "0")
    private String sourceUniquelyIdentify;
    @Header(name = "商品名称")
    private String goodsName;
    @Header(name = "UPC")
    private String sku;
    @Header(name = "品牌")
    private String brand;
    @Header(name = "贝全四级")
    private String four;
    @Header(name = "贝全三级")
    private String three;
    @Header(name = "贝全二级")
    private String two;
    @Header(name = "贝全一级")
    private String one;
    @Header(name = "补充关键词")
    private String keywords;

}
