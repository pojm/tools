package com.bruce.tool.web;

public class Singleton{

	public volatile static Singleton instance;

	public static Singleton instance(){
		if( null == instance ){
			synchronized(Singleton.class){
				if( null == instance ){
					instance = new Singleton();
				}
			}
		}
		return instance;
	}

}