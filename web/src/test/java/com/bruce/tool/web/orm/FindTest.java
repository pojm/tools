package com.bruce.tool.web.orm;

import com.bqmart.tool.EntryApplication;
import com.bqmart.tool.mybatis.domain.AdminMenu;
import com.bqmart.tool.mybatis.domain.EquipmentDict;
import com.bqmart.tool.mybatis.mapper.AdminMenuMapper;
import com.bqmart.tool.mybatis.mapper.EquipmentDictMapper;
import com.bruce.tool.common.util.LogUtils;
import com.bruce.tool.common.util.SpringBeanUtils;
import com.bruce.tool.common.util.string.JsonUtils;
import com.bruce.tool.mybatis.orm.query.CriteriaQuery;
import io.jsonwebtoken.lang.Assert;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 22:00 2019-01-20
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = EntryApplication.class)
public class FindTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    private AdminMenuMapper adminMenuMapper;

    @Autowired
    private EquipmentDictMapper equipmentDictMapper;

    @Before
    public void init(){
        SpringBeanUtils.setApplicationContext(applicationContext);
    }

    @Test
    public void findByDomain(){
        AdminMenu menu = new AdminMenu();
        menu.setId(2);
        AdminMenu smenu = adminMenuMapper.findByDomainId(menu);
        Assert.notNull(smenu);
        LogUtils.info(log,"{}", JsonUtils.objToStr(smenu));

        EquipmentDict domain = new EquipmentDict();
        domain.setId(1);
        EquipmentDict sdomain = equipmentDictMapper.findByDomainId(domain);
        Assert.notNull(sdomain);
        LogUtils.info(log,"{}", JsonUtils.objToStr(sdomain));
    }

    @Test
    @Transactional(rollbackFor = Exception.class)
    public void findById(){
        EquipmentDict sdomain = equipmentDictMapper.findById(EquipmentDict.class,1);
        Assert.notNull(sdomain);
        LogUtils.info(log,"{}", JsonUtils.objToStr(sdomain));
    }

    @Test
    public void findByColumn(){
        EquipmentDict sdomain = equipmentDictMapper.findByColumn(EquipmentDict.class,"id",1);
        Assert.notNull(sdomain);
        LogUtils.info(log,"{}", JsonUtils.objToStr(sdomain));
        sdomain = equipmentDictMapper.findByColumn(EquipmentDict.class,"key","CN2");
        Assert.notNull(sdomain);
        LogUtils.info(log,"{}", JsonUtils.objToStr(sdomain));
    }

    @Test
    public void findsByColumn(){
        List<EquipmentDict> domains = equipmentDictMapper.findsByColumn(EquipmentDict.class,"key","CN2");
        Assert.notNull(domains);
        Assert.notEmpty(domains);
        LogUtils.info(log,"{}", JsonUtils.objToStr(domains));
    }

    @Test
    public void findByColumns(){
        String json = "{\"id\":344,\"key\":\"CN2\",\"name\":\"排气温度1-N\",\"defaultValue\":\"/\",\"scopeLow\":\"-40\",\"scopeHigh\":\"125\",\"note\":\"温度值\",\"keyGroup\":0,\"keyType\":3,\"deviceType\":2,\"readwrite\":0,\"order\":10000}";
        EquipmentDict domain = JsonUtils.strToObj(json,EquipmentDict.class);
        EquipmentDict sdomain = equipmentDictMapper.findByColumns(domain);
        Assert.notNull(sdomain);
        LogUtils.info(log,"{}", JsonUtils.objToStr(sdomain));
    }

    @Test
    public void findsByColumns(){
        String json = "{\"key\":\"CN2\"}";
        EquipmentDict domain = JsonUtils.strToObj(json,EquipmentDict.class);
        List<EquipmentDict> domains = equipmentDictMapper.findsByColumns(domain);
        Assert.notNull(domains);
        Assert.notEmpty(domains);
        LogUtils.info(log,"{}", JsonUtils.objToStr(domains));
    }

    @Test
    public void findBySQLCriteria(){
        String sql = " select a.id,a.menu_name from admin_menu a join admin_menu_role_middle b on a.id=b.menu_id ";
        CriteriaQuery query = CriteriaQuery.create();
        query.and().where(" a.id = 1");
        Map domains = equipmentDictMapper.findBySQLCriteria(sql,query);
        Assert.notNull(domains);
        Assert.notEmpty(domains);
        LogUtils.info(log,"{}", JsonUtils.objToStr(domains));
    }

    @Test
    public void findsBySQLCriteria(){
        String sql = " select a.id,a.menu_name from admin_menu a join admin_menu_role_middle b on a.id=b.menu_id ";
        CriteriaQuery query = CriteriaQuery.create();
        query.and().where(" a.id = 1");
        List<Map> domains = equipmentDictMapper.findsBySQLCriteria(sql,query);
        Assert.notNull(domains);
        Assert.notEmpty(domains);
        LogUtils.info(log,"{}", JsonUtils.objToStr(domains));
    }

    @Test
    public void findBySQL(){
        String sql = " select a.id,a.menu_name from admin_menu a join admin_menu_role_middle b on a.id=b.menu_id where a.id=1 limit 1";
        Map domains = equipmentDictMapper.findBySQL(sql);
        Assert.notNull(domains);
        Assert.notEmpty(domains);
        LogUtils.info(log,"{}", JsonUtils.objToStr(domains));
    }

    @Test
    public void findsBySQL(){
        String sql = " select a.id,a.menu_name from admin_menu a join admin_menu_role_middle b on a.id=b.menu_id ";
        List<Map> domains = equipmentDictMapper.findsBySQL(sql);
        Assert.notNull(domains);
        Assert.notEmpty(domains);
        LogUtils.info(log,"{}", JsonUtils.objToStr(domains));
    }

}
