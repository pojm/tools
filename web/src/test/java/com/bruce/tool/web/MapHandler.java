package com.bruce.tool.web;

import java.util.HashMap;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 10:54 PM 2018/11/21
 */
public class MapHandler extends HashMap<String,Object> {

    private MapHandler() {
    }

    public static MapHandler build(){
        return new MapHandler();
    }

    public MapHandler add(String key,Object value){
        this.put(key,value);
        return this;
    }

}
