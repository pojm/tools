package com.bruce.tool.web;

import com.bruce.tool.common.exception.BaseException;
import com.bruce.tool.common.exception.BaseResult;
import com.bruce.tool.common.exception.ExceptionUtils;
import com.bruce.tool.common.util.LogUtils;
import com.bruce.tool.common.util.string.JsonUtils;
import com.bruce.tool.controller.handler.Result;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 12:52 PM 2018/11/13
 */
@Slf4j
public class ExceptionTest {

    @Test
    public void exception() {
        BaseException exception = new BaseException("1000","测试异常");
        BaseResult result = ExceptionUtils.printErrorInfos(exception);
        LogUtils.info(log,JsonUtils.objToStr(result));
        LogUtils.info(log,Result.success(new NullClass()));
        LogUtils.info(log,Result.success(MapHandler.build().add("list", Lists.newArrayList()).add("checkedId",Lists.newArrayList())));
    }
}
