package com.bqmart.tool.web;

import com.bqmart.tool.EntryApplication;
import com.bruce.tool.common.util.SpringBeanUtils;
import com.bruce.tool.rpc.http.core.Https;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 下午3:59 2018/6/23
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = EntryApplication.class)
@AutoConfigureMockMvc
@WebAppConfiguration
public class TestMQ extends AbstractJUnit4SpringContextTests {

    @Before
    public void init(){
        SpringBeanUtils.setApplicationContext(applicationContext);
    }

    @Test
    public void sendMQ(){
        Https.create()
                .timeout(20,20,20)
                .url("http://127.0.0.1:8086/mq/sendMQ")
                .addBody("body","测试消息001")
                .post();
    }

    @Test
    public void receiveMQ(){
        Https.create().timeout(20,20,20)
                .url("http://127.0.0.1:8086/mq/receiveMQ").post();
    }

}
