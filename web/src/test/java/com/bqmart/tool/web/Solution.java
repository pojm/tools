package com.bqmart.tool.web;

class Solution {
    public int removeDuplicates(int[] nums) {
        int index = 0;
        for( int i = 0,len = nums.length;i < len;i++ ){
            if( nums[i] != nums[index] ){
                nums[++index] = nums[i];
            }
        }
        return index+1;
    }

    public static void main(String[] args) {
        int[] nums = new int[]{1,1,2,2,3,3,5,5,6,6,6,7,7,7,9,10,10,10,10,10,11,11,11,12,12,14,14,14,14,14,14,14,14};
        Solution solution = new Solution();
        solution.removeDuplicates(nums);
    }
}