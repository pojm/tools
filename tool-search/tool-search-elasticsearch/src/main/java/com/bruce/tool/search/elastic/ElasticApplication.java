package com.bruce.tool.search.elastic;

import com.bruce.tool.common.util.SpringBeanUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 16:47 2019-02-20
 */
@SpringBootApplication
public class ElasticApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(ElasticApplication.class, args);
        SpringBeanUtils.setApplicationContext(context);
    }
}
