package com.bruce.tool.search.solr.update;

import com.bruce.tool.common.exception.BaseRuntimeException;
import com.bruce.tool.common.util.LogUtils;
import com.bruce.tool.search.solr.common.SearchUtils;
import com.bruce.tool.search.solr.domain.FilterQuerys;
import com.bruce.tool.search.solr.domain.KeyWords;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;

import java.io.IOException;
import java.util.Collection;

/**
 * 功能 :
 * 1.提供通用的文档更新方法.
 * @author : Bruce(刘正航) 下午5:17 2018/6/19
 */
@Slf4j
public abstract class AbstractSolrUpdate implements SolrUpdate {

    protected abstract SolrClient getSolrClient();

    @Override
    public UpdateResponse updateDocuments(FilterQuerys querys,Callback callback) {
        SolrDocumentList documents = this.getDocuments(querys);
        Collection<SolrInputDocument> inputDocuments = SearchUtils.transfer(documents);
        inputDocuments = callback.fillUpdateInfo(inputDocuments);
        return this.updateDocuments(inputDocuments);
    }

    @Override
    public UpdateResponse updateDocuments(Collection<SolrInputDocument> documents) {
        if (CollectionUtils.isEmpty(documents)) {
            LogUtils.info(log, "没有查询到对应的商品,直接返回,不更新数据");
            return null;
        }
        long start = System.currentTimeMillis();
        UpdateResponse updateResponse;
        try {
            getSolrClient().add(documents);
            updateResponse = getSolrClient().commit();
        } catch (SolrServerException|IOException e) {
            throw new BaseRuntimeException(e.getCause(), e.getMessage());
        }
        LogUtils.info(log, "更新文档数量:{},耗时:{}ms", documents.size(), System.currentTimeMillis() - start);
        return updateResponse;
    }


    /*
     ****************************************私有方法区*******************************************
                   _               _                           _    _                 _
                  (_)             | |                         | |  | |               | |
      _ __   _ __  _ __   __ __ _ | |_  ___   _ __ ___    ___ | |_ | |__    ___    __| |
     | '_ \ | '__|| |\ \ / // _` || __|/ _ \ | '_ ` _ \  / _ \| __|| '_ \  / _ \  / _` |
     | |_) || |   | | \ V /| (_| || |_|  __/ | | | | | ||  __/| |_ | | | || (_) || (_| |
     | .__/ |_|   |_|  \_/  \__,_| \__|\___| |_| |_| |_| \___| \__||_| |_| \___/  \__,_|
     | |
     |_|
     ****************************************私有方法区*******************************************
     */

    private SolrDocumentList getDocuments(FilterQuerys querys) {
        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setQuery(KeyWords.build().toString());
        SearchUtils.setFilterQuery(solrQuery, querys);
        LogUtils.info(log,"执行搜索，搜索参数： {}", solrQuery.toQueryString());
        QueryResponse response = SearchUtils.executeQuery(getSolrClient(), solrQuery);
        return response.getResults();
    }


}
