package com.bruce.tool.search.solr.domain;

import com.google.common.collect.Lists;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 上午12:44 2018/5/30
 */
@NoArgsConstructor(staticName = "build")
public class FacetFields implements Serializable {

    private Map<String,List<FacetFieldObject>> facetFields = new HashMap<>(3);

    public FacetFields add(String key,String value,Long count){
        if( this.facetFields.containsKey(key) ){
            List<FacetFieldObject> list = facetFields.get(key);
            list.add(FacetFieldObject.build().set(value,count));
        }else{
            this.facetFields.put(key, Lists.newArrayList(FacetFieldObject.build().set(value,count)));
        }
        return this;
    }

    public FacetFields addAll(Map<String,List<FacetFieldObject>> facetFields){
        this.facetFields.putAll(facetFields);
        return this;
    }

    public Map<String,List<FacetFieldObject>> fetchAll(){
        return this.facetFields;
    }

}
