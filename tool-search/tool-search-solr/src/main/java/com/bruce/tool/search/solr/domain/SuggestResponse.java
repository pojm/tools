package com.bruce.tool.search.solr.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * 功能 :
 * 搜索结果对象
 * @author : Bruce(刘正航) 上午1:54 2018/5/8
 */
@Data
public class SuggestResponse<T> extends BaseResponse<T> implements Serializable {
}
