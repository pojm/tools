package com.bruce.tool.search.solr.domain;

import com.bruce.tool.common.util.string.StringUtils;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 上午9:41 2018/5/8
 */
@NoArgsConstructor(staticName = "build")
public class KeyWords implements Serializable {
    @Getter
    private String key;
    private String value;

    public KeyWords set(String key,String value){
        this.key = key;
        this.value = value;
        return this;
    }

    public String toString(){
        if(StringUtils.isBlank(key) || StringUtils.isBlank(value)){
            return "*:*";
        }
        return "(" + key + ":*" + value + "*) or (" + key + ":" + value + ")";
    }
}
