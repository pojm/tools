package com.bruce.tool.search.solr.exception;

import com.bruce.tool.common.exception.ErrorCode;
import com.bruce.tool.common.exception.I18N;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 下午4:36 2018/3/17
 */
@AllArgsConstructor
public enum SearchErrorCode {

    QUERY_HOST_ERROR("SEC_0001","搜索服务连接失败",""),
    QUERY_KEY_NON("SEC_0002","请先设置查询字段名",""),
    QUERY_VALUE_NOT_SUPPORT("SEC_0003","请传入基本数据类型数据",""),

    ;
    @Getter
    private String code;
    @Getter
    private String message;
    @Getter
    private String messageENUS;

    public static String getMessageByCode(String code,I18N i18N){
        SearchErrorCode target = search(code);
        if( null == target ){
            return "";
        }
        return ErrorCode.getMessageByLanguage(i18N,target.getMessage(),target.getMessageENUS());
    }

    public static SearchErrorCode search(String code){
        SearchErrorCode target = null;
        for (SearchErrorCode errorCode  : SearchErrorCode.values() ) {
            if( errorCode.getCode().equals(code) ){
                target = errorCode;
            }
        }
        return target;
    }

}
