package com.bruce.tool.search.solr.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * 功能 :
 * 查询结果集
 * @author : Bruce(刘正航) 上午9:28 2018/5/8
 */
@Data
public class SearchResponse<T> extends BaseResponse<T> implements Serializable {
    private Integer page;
    private Integer size;
}
