package com.bruce.tool.search.solr.update;

import com.bruce.tool.search.solr.domain.FilterQuerys;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;

import java.util.Collection;

/**
 * 功能 :
 * solr更新接口
 * 1.简单的条件查询,根据条件查询主键
 * 2.根据主键更新对应的字段
 * @author : Bruce(刘正航) 下午5:12 2018/6/19
 */
public interface SolrUpdate {

    UpdateResponse updateDocuments(FilterQuerys querys, Callback callback);

    UpdateResponse updateDocuments(Collection<SolrInputDocument> documents);

    /**设置回调函数**/
    interface Callback {
        Collection<SolrInputDocument> fillUpdateInfo(Collection<SolrInputDocument> documents);
    }
}
