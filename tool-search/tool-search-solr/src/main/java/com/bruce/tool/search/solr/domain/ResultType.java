package com.bruce.tool.search.solr.domain;

/**
 * 功能 :
 * 结果集格式
 * @author : Bruce(刘正航) 下午1:38 2018/5/8
 */
public enum ResultType {
    JSON,XML,PHP,PYTHON
}
