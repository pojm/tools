package com.bruce.tool.search.solr.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 上午12:44 2018/5/30
 */
@NoArgsConstructor(staticName = "build")
public class FacetFieldObject implements Serializable{

    /**属性名称:例如:分类的ID对应的名称**/
    @Getter@Setter
    private String name;
    /**属性值:例如:分类的ID**/
    @Getter
    private String value;
    /**属性值对应的结果集数量:分类ID为198的有2个**/
    @Getter
    private Long count;

    public FacetFieldObject set(String value, Long count){
        /*初始化的时候,值赋值给字段名称*/
        this.name = value;
        this.value = value;
        this.count = count;
        return this;
    }
}
