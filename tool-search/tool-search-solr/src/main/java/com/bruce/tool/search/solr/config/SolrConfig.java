package com.bruce.tool.search.solr.config;

import lombok.Data;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.ConcurrentUpdateSolrClient;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * 功能 :
 * Solr配置
 * @author : Bruce(刘正航) 下午3:41 2018/5/29
 */
@Data
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "tool.solr")
public class SolrConfig {

    private String host;

    @Bean("solrClient")
    public SolrClient solrClient() {
        return new HttpSolrClient.Builder().withBaseSolrUrl(host).allowCompression(true).build();
    }

    @Bean("updateSolrClient")
    public SolrClient updateSolrClient() {
        return new ConcurrentUpdateSolrClient.Builder(host).build();
    }
}
