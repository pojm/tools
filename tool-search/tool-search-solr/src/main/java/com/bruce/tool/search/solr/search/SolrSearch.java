package com.bruce.tool.search.solr.search;

import com.bruce.tool.search.solr.domain.*;

/**
 * 功能 :
 * 搜索标准接口
 * @author : Bruce(刘正航) 上午1:48 2018/5/8
 */
public interface SolrSearch<S,R> {

    /**1.建议列表**/
    SuggestResponse<S> suggest(PageQuery pageQuery, KeyWords keyWords, FilterQuerys filterQuerys);

    /**2.搜索接口**/
    SearchResponse<R> search(PageQuery pageQuery, KeyWords keyWords, FilterQuerys filterQuerys);

    /**3.搜索接口**/
    SearchResponse<R> search(PageQuery pageQuery, KeyWords keyWords, FilterQuerys filterQuerys, FacetQuerys facetQuerys);

}
