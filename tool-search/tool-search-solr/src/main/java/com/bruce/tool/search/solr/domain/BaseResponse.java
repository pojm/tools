package com.bruce.tool.search.solr.domain;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 功能 :
 * 基础结果集
 * @author : Bruce(刘正航) 上午9:29 2018/5/8
 */
@Data
public abstract class BaseResponse<T>{
    /**总结果数**/
    protected Long total = 0L;
    /**返回结果集合**/
    protected List<T> response = new ArrayList<>(10);
    /**返回的facet字段列表**/
    protected Map<String,List<FacetFieldObject>> facetFields = new HashMap<>(3);
}
