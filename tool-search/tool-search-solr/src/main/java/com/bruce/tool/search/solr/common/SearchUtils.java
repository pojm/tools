package com.bruce.tool.search.solr.common;

import com.bruce.tool.common.exception.BaseRuntimeException;
import com.bruce.tool.common.util.LogUtils;
import com.bruce.tool.search.solr.domain.FacetQuerys;
import com.bruce.tool.search.solr.domain.FilterQuery;
import com.bruce.tool.search.solr.domain.FilterQuerys;
import com.bruce.tool.search.solr.domain.PageQuery;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrRequest;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

/**
 * 功能 :
 * 查询工具类
 * 1.抽取重复的方法,作为公共方法
 * @author : Bruce(刘正航) 下午5:22 2018/6/19
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SearchUtils {
    /**
     * 查询结果集
     **/
    public static QueryResponse executeQuery(SolrClient solrClient,SolrQuery query) {
        QueryResponse response;
        try {
            if( solrClient instanceof HttpSolrClient){
                HttpSolrClient httpSolrClient = (HttpSolrClient) solrClient;
                LogUtils.info(log,"Solr请求地址：" + httpSolrClient.getBaseURL());
            }
            response = solrClient.query(query, SolrRequest.METHOD.POST);
        } catch (SolrServerException|IOException e) {
            throw new BaseRuntimeException(e.getCause(),e.getMessage());
        }
        return response;
    }

    /**设置字段过滤查询参数**/
    public static void setFilterQuery(SolrQuery solrQuery, FilterQuerys querys) {
        if (Objects.isNull(querys) ) {
            return;
        }
        for (FilterQuery query : querys.queries()) {
            solrQuery.addFilterQuery(query.toString());
        }
    }

    /**设置字段facet查询参数**/
    public static void setFacetQuery(SolrQuery solrQuery, FacetQuerys facetQuerys){
        if( !facetQuerys.isFacetOn() ){
            return;
        }
        solrQuery.setFacet(facetQuerys.isFacetOn());
        solrQuery.setFacetMinCount(facetQuerys.getMinCount());
        solrQuery.addFacetField(facetQuerys.toArray());
    }

    /**分页查询参数配置**/
    public static void setPageQuery(SolrQuery solrQuery, PageQuery pageQuery){
        solrQuery.setStart(pageQuery.getPage()).setRows(pageQuery.getSize());
        List<PageQuery.Sort> sorts = pageQuery.getSorts();
        for ( PageQuery.Sort sort : sorts ) {
            solrQuery.addSort(sort.getField(),sort.getOrder());
        }
    }

    /**文档格式转换**/
    public static List<SolrInputDocument> transfer(SolrDocumentList documents){
        List<SolrInputDocument> list = new ArrayList<>(documents.size());
        SolrInputDocument inputDocument;
        for ( SolrDocument document : documents ) {
            inputDocument = new SolrInputDocument();
            Collection<String> fieldNames = document.getFieldNames();
            for ( String fieldName : fieldNames ) {
                Object value = document.getFieldValue(fieldName);
                inputDocument.addField(fieldName,value);
            }
            list.add(inputDocument);
        }
        return list;
    }
}
