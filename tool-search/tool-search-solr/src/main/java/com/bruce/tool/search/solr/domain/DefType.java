package com.bruce.tool.search.solr.domain;

/**
 * 功能 :
 * SOLR查询类型
 * @author : Bruce(刘正航) 下午1:36 2018/5/8
 */
public enum DefType {

    EDISMAX,DISMAX
}
