package com.bruce.tool.search.solr;

import com.bruce.tool.common.util.SpringBeanUtils;
import com.bruce.tool.search.solr.annotation.UseSolr;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.solr.SolrAutoConfiguration;
import org.springframework.context.ApplicationContext;

/**
 * 搜索入口
 * @author : Bruce(刘正航) 上午12:49 2018/2/11
 */
@UseSolr
@SpringBootApplication(exclude = SolrAutoConfiguration.class)
public class SolrApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(SolrApplication.class, args);
        SpringBeanUtils.setApplicationContext(context);
    }
}
