package com.bruce.tool.search.solr.annotation;

import com.bruce.tool.search.solr.config.SolrConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 功能 :
 * 自动初始化SolrClient
 * 配置文件中需要有 spring.data.solr.host 值
 * @author : Bruce(刘正航) 下午7:13 2018/5/29
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({SolrConfig.class})
public @interface UseSolr {
}
