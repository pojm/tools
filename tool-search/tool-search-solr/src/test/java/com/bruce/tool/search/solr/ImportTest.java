package com.bruce.tool.search.solr;

import com.bruce.tool.search.solr.domain.FilterQuerys;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.IOException;
import java.util.List;

/**
 * 功能 :
 * 更新指定文档的内容
 * @author : Bruce(刘正航) 上午11:51 2018/6/15
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SolrApplication.class)
@AutoConfigureMockMvc
@WebAppConfiguration
public class ImportTest {

    @Autowired
    @Qualifier("solrClient")
    private SolrClient solrClient;

    @Autowired
    @Qualifier("updateSolrClient")
    private SolrClient updateSolrClient;

    @Autowired
    private CloudUpdate cloudUpdate;

    @Test
    public void testImport() {
        double price = 13.5;
        FilterQuerys querys = FilterQuerys.build()
                .addOne("sellerId",12)
                .addOne("spu","0000000001086");

        UpdateResponse response = cloudUpdate.updateDocuments(querys, documents -> {
            for ( SolrInputDocument document : documents ) {
                Object sellerId = document.getFieldValue("sellerId");
                Object spu = document.getFieldValue("spu");
                String key = sellerId + "-" + spu;
                if( key.equals("12-0000000001086") ){
                    document.setField("price",price);
                }
            }
            return documents;
        });

        if( null != response ){
            System.out.println(response.getStatus());
        }
    }

    @Test
    public void testDelete() {
        try {
            List<String> ids = Lists.newArrayList(
                    "41cc459b-c5ab-4a12-9cf6-92a71d975e20",
                    "ec99aa27-e8a9-482e-9e45-62d6c251a192",
                    "a48adb3b-cfa6-4915-aa22-edb6e63d54c3",
                    "cd15d36d-bbfe-4418-a146-1d8022ac1514"
            );
            UpdateResponse response = solrClient.deleteById(ids);
            System.out.println(response.getStatus());
        } catch (SolrServerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
