package com.bruce.tool.search.solr;

import com.bruce.tool.search.solr.update.AbstractSolrUpdate;
import org.apache.solr.client.solrj.SolrClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 下午11:32 2018/6/19
 */
@Service
public class CloudUpdate extends AbstractSolrUpdate{

    @Autowired
    @Qualifier("updateSolrClient")
    private SolrClient updateSolrClient;

    @Override
    protected SolrClient getSolrClient() {
        return updateSolrClient;
    }

}
