package com.bruce.tool.search.solr.domain;

import lombok.Data;
import org.apache.solr.client.solrj.beans.Field;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 上午11:03 2018/5/8
 */
@Data
public class CloudInfo {
    @Field
    private String goodsName;
}
