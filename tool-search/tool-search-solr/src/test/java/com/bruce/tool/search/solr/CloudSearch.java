package com.bruce.tool.search.solr;

import com.bruce.tool.search.solr.domain.CloudInfo;
import com.bruce.tool.search.solr.search.AbstractSolrSearch;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 下午2:36 2018/5/8
 */
@Service
public class CloudSearch extends AbstractSolrSearch<CloudInfo,CloudInfo>{

    @Autowired
    private SolrClient solrClient;

    @Override
    protected SolrClient getSolrClient() {
        return solrClient;
    }

    @Override
    protected List<CloudInfo> getSuggestBeans(QueryResponse response) {
        return response.getBeans(CloudInfo.class);
    }

    @Override
    protected List<CloudInfo> getSearchBeans(QueryResponse response) {
        return response.getBeans(CloudInfo.class);
    }

}
