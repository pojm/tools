package com.bruce.tool.search.solr;

import com.bruce.tool.common.util.string.JsonUtils;
import com.bruce.tool.search.solr.domain.*;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

/**
 * 功能 :
 * 测试方法
 * @author : Bruce(刘正航) 上午11:52 2018/2/26
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SolrApplication.class)
@AutoConfigureMockMvc
@WebAppConfiguration
public class QueryTest {

    @Autowired
    private CloudSearch cloudSearch;

    @Test
    public void testQuery() {
        List<Integer> ids = Lists.newArrayList(1921,1922,1923,1924,1925,1926,1927,1928,1929,1930);
        List<Integer> cateIds = Lists.newArrayList(780);
        List<String> brands = Lists.newArrayList("妙杰","妙洁","妙洁","妙杰","倍全","倍全","倍全","倍全","天堂","倍全");
        FilterQuerys querys = FilterQuerys.build()
//                .addOne("isSale",1)
//                .addOne("cateId1",1)
//                .addOne("cateId2",1)
//                .addOne("brand",1)
//                .addOne("price",1)
//                .addRange("goodsId",1924,1929)
                .addAll("id",ids)
//                .addAll("cateId1",FilterQuery.FORWARD,cateIds)
//                .addAll("brand",brands)
                ;
        FacetQuerys facetQuerys = FacetQuerys.build().add("cateId1","cateId2","brand","price");
        FacetQuerys.addExcludeField("cateId1","cateId2","brand","price");
        SearchResponse response = cloudSearch.search(PageQuery.build().set(0,10).addSort("goodsId","desc"), KeyWords.build(),querys,facetQuerys);
        System.out.println(JsonUtils.objToStr(response));
    }

}
