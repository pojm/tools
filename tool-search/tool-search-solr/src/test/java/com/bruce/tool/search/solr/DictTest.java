package com.bruce.tool.search.solr;

import com.bruce.tool.office.excel.core.Excels;
import com.bruce.tool.common.util.string.StringUtils;
import com.bruce.tool.search.solr.domain.GoodsInfo;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 功能 :
 * 根据商品池中,所有商品名称,商品属性
 * 提取出关键字,作为建索引的词典
 * 目的:
 * 提升搜索命中率
 * @author : Bruce(刘正航) 上午11:49 2018/2/27
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DictTest.class)
@AutoConfigureMockMvc
@WebAppConfiguration
public class DictTest {

    private static final String KEYWORD_REGEX =
            "([0-9]+ml)|([0-9]+g)|([0-9]+克)|([0-9]+粒)|([0-9]+段)|([0-9]+片)" + //规格
            "(罐)|(盒)|(袋)|(桶)|(木质)|(木制)|(一段)|(二段)|(三段)|" + //中文单位
            "(加大号XL)|(大号L)|(中号M)|(小号S)|(加大号)|(大号)|(中号)|(小号)|" + //中文单位
            "(美素)|(佳儿)|(港版)|(有机)|(干爽)|(纸尿裤)|(裤子)|(拉拉裤)|(学步裤)" + //搜索习惯关键字
            "(金装)|" + //搜索习惯关键字
            "(国产)|(澳大利亚)|(澳洲)|(美国)|(欧洲)|(进口)|" + // 产地相关
            "(超薄)|(男)|(女)"; //属性相关
    private static final String CHINESE_REGEX = "([\\u3001-\\u3003\\u4E00-\\u9FA5\\uFE30-\\uFFA0])+";

    @Test
    public void loadDictInfo() throws Exception {
        List<GoodsInfo> list = Excels.Import()
                .config(new FileInputStream("/Users/bruce/Documents/倍全产品线/5月份需求/外包项目进度/商品池180425.xlsx"),5000)
                .addSheet(GoodsInfo.class)
                .executeForData();
        Set<String> dictWords = new TreeSet<>();
        Map<String,String> tags = new HashMap<>();
        for ( GoodsInfo info : list ) {
            Set<String> lineWords = new HashSet<>();
            fetchGoodsName(dictWords, lineWords, info.getGoodsName());
            fetchItem(dictWords, lineWords, info.getBrand());
            fetchItem(dictWords, lineWords, info.getOne());
            fetchItem(dictWords, lineWords, info.getTwo());
            fetchItem(dictWords, lineWords, info.getThree());
            fetchItem(dictWords, lineWords, info.getFour());
            fetchItem(dictWords, lineWords, info.getKeywords());
            String lineTags = ","+StringUtils.join(lineWords,",");
            lineTags = lineTags.replaceAll(",,",",");
            tags.put(info.getSourceUniquelyIdentify(),lineTags);
        }

        FileWriter dictWriter = new FileWriter("brand_cate_keywords.dic");
        for ( String dictword : dictWords ) {
            if( StringUtils.isBlank(dictword) ){ continue; }
            dictWriter.write(dictword+"\r\n");
        }
        dictWriter.flush();
        dictWriter.close();
        FileWriter updateSQLWriter = new FileWriter("cloud_goods_update.sql");
        for ( Map.Entry<String,String> entry : tags.entrySet() ) {
            String sql = "update cloud_goods set tags = concat(tags,\""+entry.getValue()+"\") where source_uniquely_identify='"+entry.getKey()+"';";
            updateSQLWriter.write(sql+"\r\n");
        }
        updateSQLWriter.flush();
        updateSQLWriter.close();
    }

    private void fetchGoodsName(Set<String> dictWords, Set<String> lineWords, String goodsName) {
        List<String> units = fetchUnit(goodsName);
        dictWords.addAll(units);
        lineWords.addAll(units);
    }

    /**获取系列关键字**/
    private void fetchItem(Set<String> dictWords, Set<String> words, String item) {
        item = item.replaceAll("[()（），\\\\+/\\\\ ]+",",");
        List<String> items = StringUtils.splitToList(item);
        List<String> cutss = replaceChinese(items,words);
        dictWords.addAll(cutss);
        words.addAll(items);
    }

    private List<String> replaceChinese(List<String> brands, Set<String> words) {
        Iterator<String> iter = brands.iterator();
        List<String> results = new ArrayList<>();
        while( iter.hasNext() ){
            String split = iter.next();
            if( split.length() == 1 ){
                words.add(split);
                continue;
            }
            List<String> cuts = splitChinese(split);
            if( cuts.size() > 0 ){
                for ( String cut : cuts ) {
                    if( cut.length() == 1 ){
                        words.add(split);
                        continue;
                    }
                    results.add(cut);
                }
            }else{
                results.add(split);
            }
        }
        return results;
    }

    private List<String> splitChinese(String keywords){
        return fetchByRegex(keywords, CHINESE_REGEX,true);
    }

    private List<String> fetchUnit(String keywords){
        return fetchByRegex(keywords, KEYWORD_REGEX,false);
    }

    private List<String> fetchByRegex(String keywords,String regex,boolean addSelf){
        //艾惟诺 （Aveeno） 天然燕麦每日倍护润肤乳(无香型)225ml 原装进口
        List<String> items = new ArrayList<>();

        Pattern p1 = Pattern.compile(regex);
        Matcher m1 = p1.matcher(keywords);

        while (m1.find()) {
            if(m1.groupCount()>0){
                String group = m1.group(0);
                if( keywords.length() > group.length() ){
                    items.add(group);
                    addSpecialWords(items, group);
                    if( addSelf ){
                        items.add(keywords.replaceAll(group,""));
                    }
                }
            }
        }
        return items;
    }

    /**设置特殊的关键词**/
    private void addSpecialWords(List<String> items, String keywords) {
        if( "一段".equals(keywords) ){
            items.add("1段");
        }
        if( "二段".equals(keywords) ){
            items.add("2段");
        }
        if( "三段".equals(keywords) ){
            items.add("3段");
        }
        if( "1段".equals(keywords) ){
            items.add("一段");
        }
        if( "2段".equals(keywords) ){
            items.add("二段");
        }
        if( "3段".equals(keywords) ){
            items.add("三段");
        }
    }

}
