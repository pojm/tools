package com.bruce.tool.rpc.http.handler;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 下午3:07 2018/7/26
 */
public enum Arithmetic {
    HMAC_SHA_256, HMAC_SHA_512, HMAC_SHA_1
    ;
}
