package com.bruce.tool.rpc.http.handler.response;

import com.bruce.tool.common.util.LogUtils;
import lombok.extern.slf4j.Slf4j;
import okhttp3.Response;
import org.springframework.http.HttpStatus;

import java.io.IOException;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 下午11:43 2018/7/25
 */
@Slf4j
public class DefaultResponseHandler extends ResponseAdapter<String> {
    @Override
    public String handle(Response response) throws IOException {
        if( HttpStatus.NOT_FOUND.value() == response.code() ){
            String result = response.body().string();
            LogUtils.info(log,"{}",result);
            return "404:"+HttpStatus.NOT_FOUND.getReasonPhrase();
        }
        return response.body().string();
    }
}
