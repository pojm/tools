package com.bruce.tool.rpc.http.constant;

import lombok.Getter;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 18:32 2019-02-22
 */
public enum HttpErrorCode {
    ERROR_SSL("HE0001","SSL配置错误"),
    ERROR_BUSINESS("HE0002","业务错误"),
    ERROR_HTTP("HE0003","系统错误"),
    URL_IS_NULL("HE0004","URL为空"),


    ;
    @Getter
    private String code;
    @Getter
    private String message;

    HttpErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
