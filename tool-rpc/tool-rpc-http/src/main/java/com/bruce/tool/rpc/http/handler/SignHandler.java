package com.bruce.tool.rpc.http.handler;

import com.bruce.tool.rpc.http.handler.request.RequestInfo;
import com.bruce.tool.rpc.http.handler.sign.SignInfo;
import okhttp3.HttpUrl;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

/**
 * 功能 :
 * 签名处理类
 * @author : Bruce(刘正航) 上午12:33 2018/7/26
 */
public interface SignHandler {
    /**默认签名放置到请求头中**/
    void sign(SignInfo signInfo, RequestInfo requestInfo);

}
