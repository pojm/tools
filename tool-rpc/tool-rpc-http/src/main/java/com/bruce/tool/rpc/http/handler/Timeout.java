package com.bruce.tool.rpc.http.handler;

import lombok.Builder;
import lombok.Data;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 上午4:06 2018/8/18
 */
@Data
@Builder
public class Timeout {
    private Integer connectTimeout = 5;
    private Integer writeTimeout = 5;
    private Integer readTimeout = 5;
}
