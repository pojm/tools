package com.bruce.tool.rpc.http.handler;

import lombok.Data;

/**
 * 功能 :
 * http请求的日志对象
 * @author : Bruce(刘正航) 下午12:43 2018/8/19
 */
@Data
public class LogInfo {
    private boolean print=true;
    private String begin;
    private String headerInfo;
    private String requestInfo;
    private String urlParams;
    private String bodyParams;
    private String responseInfo;
    private String end;
    /**请求耗时**/
    private Long duration;
}
