package com.bruce.tool.rpc.http.handler;


import com.bruce.tool.rpc.http.handler.response.RetryResponse;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 下午5:22 2018/9/6
 */
public interface RetryHandler {

    RetryResponse needRetry(Object response);
}
