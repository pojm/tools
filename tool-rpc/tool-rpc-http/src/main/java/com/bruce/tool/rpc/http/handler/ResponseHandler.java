package com.bruce.tool.rpc.http.handler;

import okhttp3.Response;

import java.io.IOException;

/**
 * 功能 :
 * 结果集处理类
 * @author : Bruce(刘正航) 下午11:42 2018/7/25
 */
public interface ResponseHandler<T> {
    T handle(Response response) throws IOException;
}
