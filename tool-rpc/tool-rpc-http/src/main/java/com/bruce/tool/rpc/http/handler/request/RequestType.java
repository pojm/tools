package com.bruce.tool.rpc.http.handler.request;

/**
 * 功能 :
 * 请求方式:form/json
 * @author : Bruce(刘正航) 上午4:45 2018/8/18
 */
public enum RequestType {
    URL,FORM,JSON,XML,FILE,
    ;
}
