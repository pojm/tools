package com.bruce.tool.rpc.http.handler.response;

import lombok.extern.slf4j.Slf4j;
import okhttp3.Response;

import java.io.IOException;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 上午2:30 2018/8/30
 */
@Slf4j
public class PingResponseHandler extends ResponseAdapter<Boolean> {
    @Override
    public Boolean handle(Response response) throws IOException {
        return validStatus(response);
    }
}
