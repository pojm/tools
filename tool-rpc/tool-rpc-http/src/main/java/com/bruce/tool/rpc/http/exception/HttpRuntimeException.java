package com.bruce.tool.rpc.http.exception;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.function.Supplier;

/**
 * HTTP异常类
 * @author liuzhenghang
 * @author : Bruce(刘正航) 上午10:21 2018/2/26
 */
@Data
@NoArgsConstructor // 默认无参构造函数
public class HttpRuntimeException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	protected Object code;
	protected String message;
	protected Object data;

	public HttpRuntimeException(Throwable e) {
		super(e);
	}

	public HttpRuntimeException(Supplier<Throwable> e) {
		super(e.get());
	}

	public HttpRuntimeException(Object code, String message) {
		super(message);
		this.code = code;
		this.message = message;
	}

	public HttpRuntimeException(Object code, String message, Object data) {
		super(message);
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public HttpRuntimeException(Supplier<Object> code, Supplier<String> message) {
		super(message.get());
		this.code = code.get();
		this.message = message.get();
	}

	public HttpRuntimeException(Supplier<Object> code, Supplier<String> message, Object data) {
		super(message.get());
		this.code = code.get();
		this.message = message.get();
		this.data = data;
	}

}
