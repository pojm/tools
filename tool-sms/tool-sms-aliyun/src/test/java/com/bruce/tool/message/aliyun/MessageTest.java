package com.bruce.tool.message.aliyun;

import com.bruce.tool.common.util.random.Random;
import com.bruce.tool.message.aliyun.adapter.MessageAdapter;
import com.bruce.tool.message.aliyun.core.Message;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 1:42 PM 2018/12/3
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AliyunMessageApplication.class)
@AutoConfigureMockMvc
@WebAppConfiguration
public class MessageTest {

    @Autowired
    private MessageAdapter messageAdapter;

    @Test
    public void aliyun(){
        Message.create()
                .auth("LTAIalFoIv0p4FYS","p2yLGsc7j0SNO318HCXacdIywsujM5")
                .signature("王亚彬").template("SMS_76950018")
                .phone("13522204027")
                .param("name", "王亚彬")
                .param("shuzi", Random.number(6))
                .send();
    }

    @Test
    public void yun(){
        messageAdapter
                .phone("13522204027")
                .param("name", "王亚彬")
                .param("shuzi", Random.number(6))
                .send();
    }
}
