package com.bruce.tool.message.aliyun.config;

import org.springframework.context.annotation.ComponentScan;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 09:23 2019-01-31
 */
@ComponentScan(value = {"com.bruce.tool.message.aliyun"})
public class AliMessageRegister {
}
