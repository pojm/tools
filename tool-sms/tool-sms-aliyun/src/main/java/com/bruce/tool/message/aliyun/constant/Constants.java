package com.bruce.tool.message.aliyun.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 17:18 2019-01-11
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constants {

    public static final String MESSAGE_URL = "https://eco.taobao.com/router/rest";

    //产品名称:云通信短信API产品,开发者无需替换
    public static final String PRODUCT = "Dysmsapi";
    //产品域名,开发者无需替换
    public static final String DOMAIN = "dysmsapi.aliyuncs.com";

    public static final String SMS_READ_TIMEOUT = "sun.net.client.defaultReadTimeout";
    public static final String SMS_CONNECT_TIMEOUT = "sun.net.client.defaultConnectTimeout";
}
