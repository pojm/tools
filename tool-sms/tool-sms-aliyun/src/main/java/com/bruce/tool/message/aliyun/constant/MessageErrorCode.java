package com.bruce.tool.message.aliyun.constant;

import com.bruce.tool.common.exception.ErrorCode;
import com.bruce.tool.common.exception.I18N;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 下午4:36 2018/3/17
 */
@AllArgsConstructor
public enum MessageErrorCode {
    SUCCESS("OK","请求成功",""),
    CLIENT_ERROR("SMS0001","客户端异常","Cient Exception"),
    RESULT_ERROR("SMS0002","未知错误,短信发送失败","send failed"),
    ERROR1("isp.RAM_PERMISSION_DENY","RAM权限DENY",""),
    ERROR2("isv.OUT_OF_SERVICE","业务停机",""),
    ERROR3("isv.PRODUCT_UN_SUBSCRIPT","未开通云通信产品的阿里云客户",""),
    ERROR4("isv.PRODUCT_UNSUBSCRIBE","产品未开通",""),
    ERROR5("isv.ACCOUNT_NOT_EXISTS","账户不存在",""),
    ERROR6("isv.ACCOUNT_ABNORMAL","账户异常",""),
    ERROR7("isv.SMS_TEMPLATE_ILLEGAL","短信模板不合法",""),
    ERROR8("isv.SMS_SIGNATURE_ILLEGAL","短信签名不合法",""),
    ERROR9("isv.INVALID_PARAMETERS","参数异常",""),
    ERROR10("isp.SYSTEM_ERROR","系统错误",""),
    ERROR11("isv.MOBILE_NUMBER_ILLEGAL","非法手机号",""),
    ERROR12("isv.MOBILE_COUNT_OVER_LIMIT","手机号码数量超过限制",""),
    ERROR13("isv.TEMPLATE_MISSING_PARAMETERS","模板缺少变量",""),
    ERROR14("isv.BUSINESS_LIMIT_CONTROL","业务限流",""),
    ERROR15("isv.INVALID_JSON_PARAM","JSON参数不合法，只接受字符串值",""),
    ERROR16("isv.BLACK_KEY_CONTROL_LIMIT","黑名单管控",""),
    ERROR17("isv.PARAM_LENGTH_LIMIT","参数超出长度限制",""),
    ERROR18("isv.PARAM_NOT_SUPPORT_URL","不支持URL",""),
    ERROR19("isv.AMOUNT_NOT_ENOUGH","账户余额不足",""),
    ;

    @Getter
    private String code;
    @Getter
    private String message;
    @Getter
    private String messageENUS;

    public static String getMessageByCode(String code,I18N i18N){
        MessageErrorCode target = search(code);
        if( null == target ){
            return "";
        }
        return ErrorCode.getMessageByLanguage(i18N,target.getMessage(),target.getMessageENUS());
    }

    public static MessageErrorCode search(String code){
        MessageErrorCode target = null;
        for (MessageErrorCode messageErrorCode : MessageErrorCode.values() ) {
            if( messageErrorCode.getCode().equals(code) ){
                target = messageErrorCode;
            }
        }
        return target;
    }

}
