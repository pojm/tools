package com.bruce.tool.message.aliyun;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 2:55 PM 2018/12/11
 */
@SpringBootApplication(scanBasePackages = {"com.bruce.tool.message"})
public class AliyunMessageApplication {

    public static void main(String[] args) {
        SpringApplication.run(AliyunMessageApplication.class, args);
    }
}
