package com.bruce.tool.message.aliyun.adapter;

/**
 * 功能 :
 * 返回结果解析接口
 * @author : Bruce(刘正航) 17:19 2019-01-11
 */
public interface ResponseAdapter<T> {
    T transfer(String result);
}
