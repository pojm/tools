package com.bruce.tool.message.aliyun.adapter;

import com.bruce.tool.message.aliyun.config.MessageConfig;
import com.bruce.tool.message.aliyun.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 功能 :
 * 默认阿里云-云通信短信
 * @author : Bruce(刘正航) 18:34 2019-01-11
 */
@Component("messageAdapter")
public class MessageAdapter {
    @Autowired
    private MessageConfig messageConfig;

    private Message message = Message.create();

    private void init(){
        message.auth(messageConfig.getKey(), messageConfig.getSecret())
                .template(messageConfig.getTemplate())
                .signature(messageConfig.getSignature());
    }

    public MessageAdapter phone(String phone){
        message.phone(phone);
        return this;
    }

    public MessageAdapter param(String key, String value){
        message.param(key,value);
        return this;
    }

    public MessageAdapter params(Map<String,Object> params){
        message.params(params);
        return this;
    }

    public MessageAdapter region(String region){
        message.region(region);
        return this;
    }

    public MessageAdapter endpoint(String endpoint){
        message.endpoint(endpoint);
        return this;
    }

    public MessageAdapter extend(String extend){
        message.extend(extend);
        return this;
    }

    /**执行邮件发送操作**/
    public void send(){
        this.init();
        this.message.send();
    }
}
