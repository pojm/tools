package com.bruce.tool.message.aliyun.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 17:08 2019-01-11
 */
@Data
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "tool.message.aliyun")
public class MessageConfig {
    /**用户唯一标识**/
    private String key;
    /**用户秘钥**/
    private String secret;
    /**短息模板**/
    private String template;
    /**短息签名**/
    private String signature;
}
