package com.bruce.tool.message.aliyun.annotation;

import com.bruce.tool.message.aliyun.config.AliMessageRegister;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 09:21 2019-01-31
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(AliMessageRegister.class)
public @interface UseAliMessage {
}
