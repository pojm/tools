# tool-sms 短信工具包
####1.工具中心,项目常用工具类集合
```
使用方式见测试类:

import com.bruce.tool.common.util.random.Random;
import com.bruce.tool.message.aliyun.adapter.MessageAdapter;
import com.bruce.tool.message.aliyun.core.Message;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AliyunMessageApplication.class)
@AutoConfigureMockMvc
@WebAppConfiguration
public class MessageTest {

    @Autowired
    private MessageAdapter messageAdapter;

    @Test
    public void aliyun(){
        Message.create()
                .auth(appKey,appSecret)
                .signature(短信签名).template(短信模板编号)
                .phone(电话号码)
                .param(模板参数1, 模板参数值1)
                .param(模板参数2, 短信随机数)
                .send();
    }

    @Test
    public void yun(){
        messageAdapter
                .phone(电话号码)
                .param(模板参数1, 模板参数值1)
                .param(模板参数2, 短信随机数)
                .send();
    }
}
```