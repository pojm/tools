package com.bruce.tool.controller.util;

import com.bruce.tool.common.arithmetic.MD5;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * 功能 :
 * 请求参数,工具类
 * @author : Bruce(刘正航) 下午10:56 2018/5/2
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RequestLimitUtils {

    /**
     * 生成每一个请求,唯一的KEY值
     */
    public static String orderKey(HttpServletRequest request, String method) {
        return requestKey(request,method) + "_ORDER";
    }

    /**
     * 生成每一个请求,唯一的KEY值
     */
    public static String duplicateKey(HttpServletRequest request, String method) {
        return requestKey(request,method) + "_DUPLICATE";
    }

    /**文件下载的时候,用于缓存byte数据的key**/
    public static String orderData(HttpServletRequest request, String method) {
        return orderKey(request,method) + "_DATA";
    }

    /**文件下载的时候,用于缓存文件名称的key**/
    public static String orderFileName(HttpServletRequest request, String method) {
        return orderKey(request,method) + "_FILENAME";
    }

    /**
     * 生成每一个请求,唯一的KEY值
     */
    private static String requestKey(HttpServletRequest request, String method) {

        StringBuilder key = new StringBuilder();
        // 拼接请求路径
        key.append(request.getRequestURI());
        // 拼接目标方法名称
        key.append(method);
        // 请求方式(get/post)
        key.append(request.getMethod());

        jointParams(request, key);

        return MD5.encode(key.toString(), MD5.Case.UPPER);
    }

    private static void jointParams(HttpServletRequest request, StringBuilder key) {
        Map<String, String[]> parameterMap = request.getParameterMap();
        if (parameterMap != null) {
            // 参数排序处理
            TreeMap<String, String[]> treeMap = new TreeMap<>(parameterMap);
            Set<Map.Entry<String, String[]>> entries = treeMap.entrySet();
            for (Map.Entry<String, String[]> entry : entries) {
                key.append(entry.getKey()).append(Arrays.toString(entry.getValue()));
            }
        }
    }
}
