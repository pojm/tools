package com.bruce.tool.controller.handler;

import com.bruce.tool.common.util.ClassUtils;
import com.bruce.tool.common.util.LogUtils;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;

import java.lang.annotation.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 功能 :
 * Controller层方法切面
 * 继承此类,实现类似方法,可以对controller做切面
 * @author : Bruce(刘正航) 下午3:23 2018/4/27
 */
@Slf4j
public class ControllerAspect {

    /**
     * 功能 :
     * 根据注解,获取form数据对象
     * @author : Bruce(刘正航) 下午3:12 2018/4/28
     */
    @Inherited
    @Documented
    @Target({ElementType.TYPE})
    @Retention(RetentionPolicy.RUNTIME)
    public @interface Form {
        String[] packages() default "";
    }

    public Object deeperAround(ProceedingJoinPoint pjp) throws Throwable {
        Map<Integer,Object> params = fetchParams(pjp);
        if(params.isEmpty()){
            return printResultInfo(pjp);
        }

        ParamsHandler.build().handle(params);

        for (int i = 0,length=pjp.getArgs().length; i < length; i++) {
            if(Objects.nonNull(params.get(i)) ){
                pjp.getArgs()[i] = params.get(i);
            }
        }

        return printResultInfo(pjp);
    }

    public Object deeperAround(ProceedingJoinPoint pjp,ParamsListener listener) throws Throwable {
        Map<Integer,Object> params = fetchParams(pjp);
        if(params.isEmpty()){
            return printResultInfo(pjp);
        }

        listener.on(params);

        for (int i = 0,length=pjp.getArgs().length; i < length; i++) {
            if(Objects.nonNull(params.get(i)) ){
                pjp.getArgs()[i] = params.get(i);
            }
        }

        return printResultInfo(pjp);
    }

    private Object printResultInfo(ProceedingJoinPoint pjp) throws Throwable {
        Object result = pjp.proceed(pjp.getArgs());
        LogUtils.info(log,"返回结果: {}",result);
        return result;
    }

    /**获取请求的所有参数**/
    private Map<Integer,Object> fetchParams(ProceedingJoinPoint pjp) {

        String[] formPackages = fetchFormPackage();

        Map<Integer,Object> params = new HashMap<>(0);

        if( null == formPackages || formPackages.length == 0 ){
            return params;
        }
        /**非基础数据类型的参数**/
        /**基础数据类型的参数**/
        for ( int i = 0,length=pjp.getArgs().length;i<length;i++ ) {
            Object arg = pjp.getArgs()[i];
            if( null == arg ){continue;}
            Class<?> clazz = arg.getClass();
            // 基本数据类型的获取
            if( ClassUtils.isBaseDataType(clazz) ){
                params.put(i,arg);
                continue;
            }
            // 普通form表单对象的获取
            fetchByPackage(formPackages, params, i, arg);
            /** TODO 特别说明
             * 除基本数据类型,自定义form表单对象类型之外
             * Date类型,InputStream,MultipartFile等类型,不能通过这个方式打印对象内容
             */
        }

        return params;
    }

    /**获取切面配置的包路径**/
    private String[] fetchFormPackage(){
        Form form = this.getClass().getAnnotation(Form.class);
        return form.packages();
    }

    /**根据包路径获取表单对象**/
    private void fetchByPackage(String[] formPackage, Map<Integer, Object> params, int i, Object arg) {
        String clsPackage = arg.getClass().getPackage().getName();
        for ( String packageName : formPackage ) {
            if( clsPackage.contains(packageName) ){
                params.put(i,arg);
                break;
            }
        }
    }

}