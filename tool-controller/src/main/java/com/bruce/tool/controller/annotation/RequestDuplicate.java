package com.bruce.tool.controller.annotation;

import java.lang.annotation.*;

/**
 * @Prject: cloud
 * @Package: com.cloud.annotation
 * @Description: TODO
 * @author: wuyujia
 * @Date: 2017/11/23 00:29
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestDuplicate {

    long value() default 0;

    /**
     * 指定多少时间以内不能重复提交
     * -1 表示不进行处理
     * @return
     */
    long seconds();
}