package com.bruce.tool.controller.exception;

import com.bruce.tool.common.exception.BaseRuntimeException;
import com.bruce.tool.common.exception.ErrorCode;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 08:58 2019-02-20
 */
public class ExportLimitException extends BaseRuntimeException {
    public ExportLimitException(Integer maxDownload) {
        super(ErrorCode.EXPORT_NO_RECORD.getCode(), "本次导出请求超过"+maxDownload+"条数据，每次最多允许导出"+maxDownload+"条数据！");
    }
}
