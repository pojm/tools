package com.bruce.tool.controller.annotation;

import java.lang.annotation.*;

/**
 * 功能 :
 * 后台功能正在执行中
 * @author : Bruce(刘正航) 上午1:34 2018/4/21
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface RequestOrder {

    long value() default 0;

    long release() default 60 * 10;
}
