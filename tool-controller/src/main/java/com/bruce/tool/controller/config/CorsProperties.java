package com.bruce.tool.controller.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 下午1:48 2018/7/4
 */
@Data
@Component
@Configuration
@ConfigurationProperties(prefix = "tool.cors")
public class CorsProperties {

    private String mapping = "/**";
    private String[] allowedOrigins = new String[]{"127.0.0.1"};
    private String[] allowedMethods = new String[]{"POST", "GET", "PUT", "OPTIONS", "DELETE"};
    private Boolean allowCredentials = true;
    private String[] allowedHeaders = new String[]{"Cache-Control","Content-Language","Content-Type","Expires","Last-Modified","Pragma"};
    private String[] exposedHeaders = new String[]{};
    private Integer maxAge = 3600;

}
