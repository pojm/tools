package com.bruce.tool.controller.config;

import com.bruce.tool.controller.interceptor.DuplicateInterceptor;
import com.bruce.tool.controller.interceptor.OrderInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

public class InterceptorConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new DuplicateInterceptor()).addPathPatterns("/**");
        registry.addInterceptor(new OrderInterceptor()).addPathPatterns("/**");
    }
}