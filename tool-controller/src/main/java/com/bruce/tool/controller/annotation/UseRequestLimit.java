package com.bruce.tool.controller.annotation;

import com.bruce.tool.controller.config.InterceptorConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 功能 :
 * 开启重复请求限制
 * 1.开启之后,只要在方法上添加@Running注解即可实现防重复提交
 * @author : Bruce(刘正航) 下午5:32 2018/5/25
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({InterceptorConfig.class})
public @interface UseRequestLimit {
}
