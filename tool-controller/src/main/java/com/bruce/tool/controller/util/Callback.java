package com.bruce.tool.controller.util;

import com.bruce.tool.common.exception.BaseRuntimeException;

import java.util.List;

public interface Callback<T>{
    List<T> fetch() throws BaseRuntimeException;
}