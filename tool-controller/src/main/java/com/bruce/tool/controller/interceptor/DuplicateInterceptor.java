package com.bruce.tool.controller.interceptor;

import com.bruce.tool.cache.redis.constant.ExpireTime;
import com.bruce.tool.cache.redis.util.RedisUtils;
import com.bruce.tool.common.exception.BaseException;
import com.bruce.tool.common.exception.ErrorCode;
import com.bruce.tool.common.util.LogUtils;
import com.bruce.tool.controller.annotation.RequestDuplicate;
import com.bruce.tool.controller.util.RequestLimitUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 重复请求阻止拦截器
 * @author : wuyujia 2017/04/27
 * @author : Bruce(刘正航) 下午3:23 2018/4/27
 */
@Slf4j
@Component("duplicateInterceptor")
public class DuplicateInterceptor extends HandlerInterceptorAdapter {

    /**
     * 拦截重复提交的请求
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if (handler instanceof HandlerMethod) {
            HandlerMethod method = (HandlerMethod) handler;
            RequestDuplicate resubmit = method.getMethodAnnotation(RequestDuplicate.class);
            if (resubmit == null) {
                return true;
            } else {
                Long seconds = resubmit.seconds();
                String key = RequestLimitUtils.duplicateKey(request,request.getMethod());
                String value = RedisUtils.get(key);
                LogUtils.debug(log,"重复提交,KEY:{}",key);
                LogUtils.debug(log,"重复提交,KEY:{},VALUE:{}",key,value);
                if (StringUtils.isBlank(value)) {
                    RedisUtils.set(key, seconds.toString(), ExpireTime.ONE_MIN);
                    return true;
                } else {
                    log.error("重复提交:KEY:{},触发重复提交限制",key);
                    throw new BaseException(ErrorCode.RESUBMIT.getCode(), ErrorCode.RESUBMIT.getMessage());
                }
            }
        }
        return true;
    }

}