package com.bruce.tool.controller.handler;

import java.util.Map;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 01:22 2019-01-11
 */
public interface ParamsListener {
    void on(Map<Integer,Object> params);
}
