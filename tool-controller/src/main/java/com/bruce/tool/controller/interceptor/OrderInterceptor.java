package com.bruce.tool.controller.interceptor;

import com.bruce.tool.cache.redis.constant.ExpireTime;
import com.bruce.tool.cache.redis.util.RedisUtils;
import com.bruce.tool.common.exception.BaseException;
import com.bruce.tool.common.exception.ErrorCode;
import com.bruce.tool.controller.annotation.RequestOrder;
import com.bruce.tool.controller.util.RequestLimitUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 顺序请求
 * 返回时间过长的请求,做状态监控
 * 继承此类,实现对重复提交请求的处理
 */
@Slf4j
@Component("orderInterceptor")
public class OrderInterceptor extends HandlerInterceptorAdapter {

    /**
     * 拦截重复提交的请求
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if (handler instanceof HandlerMethod) {
            HandlerMethod method = (HandlerMethod) handler;
            RequestOrder requestOrder = method.getMethodAnnotation(RequestOrder.class);

            if (requestOrder == null) {
                return true;
            }

            if (fetchDownloadFileNameFromCache(request)) { return true; }

            /**锁自动释放时间**/
            Long seconds = requestOrder.release();
            String key = RequestLimitUtils.orderKey(request, request.getMethod());
            Object value = RedisUtils.get(key);
            log.debug("重入锁,KEY:{}",key);
            log.debug("重入锁,KEY:{},VALUE:{}",key,value);
            if (null == value) {
                log.debug("重入锁,KEY:{},开启...........................",key);
                RedisUtils.set(key, seconds.toString(), ExpireTime.ONE_MIN.getTime());
                return true;
            } else {
                log.debug("重入锁,KEY:{},排队...........................",key);
                throw new BaseException(ErrorCode.RUNNING.getCode(), ErrorCode.RUNNING.getMessage());
            }
        }

        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        if (handler instanceof HandlerMethod) {
            // 请求完成释放锁
            HandlerMethod method = (HandlerMethod) handler;
            RequestOrder requestOrder = method.getMethodAnnotation(RequestOrder.class);

            if (requestOrder == null) {
                return;
            }

            String key = RequestLimitUtils.orderKey(request, request.getMethod());
            Object value = RedisUtils.get(key);
            log.debug("重入锁,KEY:{},afterCompletion",key);
            log.debug("重入锁,KEY:{},afterCompletion,VALUE:{}",key,value);
            if (null != value) {
                RedisUtils.delete(key);
            }
        }
    }

    /**
     * 从缓存获取文件名称
     **/
    private boolean fetchDownloadFileNameFromCache(HttpServletRequest request) throws IOException {
        String fileNameKey = RequestLimitUtils.orderFileName(request, request.getMethod());
        Object fileName = RedisUtils.get(fileNameKey);
        if (null != fileName) {
            log.debug("重入锁,KEY:{},同时返回的时候,从缓存中获取数据.....",fileNameKey);
            return true;
        }
        return false;
    }
}