package com.bruce.tool.controller.handler;

import com.bruce.tool.common.exception.ExceptionUtils;
import com.bruce.tool.common.util.string.JsonUtils;
import com.bruce.tool.common.util.LogUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * 功能 :
 * 异常处理;
 * 继承此类,可进行大部分异常的处理
 * @ControllerAdvice (在类上加此注解)
 * 在方法上加一下注解
 * @ResponseBody
 * @ExceptionHandler(Throwable.class)
 * @author : Bruce(刘正航) 下午3:23 2018/4/27
 */
@Slf4j
public class DefaultExceptionHandler {

    public String exceptionHandler(Throwable ex) {
        String exceptionInfo = JsonUtils.objToStr(ExceptionUtils.printErrorInfo(ex).getModelMap());
        LogUtils.info(log,"返回结果: {}",exceptionInfo);
        return exceptionInfo;
    }

}