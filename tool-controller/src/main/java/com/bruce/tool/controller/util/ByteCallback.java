package com.bruce.tool.controller.util;

import com.bruce.tool.common.exception.BaseRuntimeException;

public interface ByteCallback{
    byte[] fetch() throws BaseRuntimeException;
}