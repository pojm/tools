package com.bruce.tool.controller.handler;

import com.bruce.tool.common.exception.ExceptionUtils;
import com.bruce.tool.common.util.ClassUtils;
import com.bruce.tool.common.util.LogUtils;
import com.bruce.tool.common.util.string.JsonUtils;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.PropertyUtils;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

/**
 * 功能 :
 * 参数工具类
 * 1.参数中英文转换
 * 2.目前只支持form表单封装,基本数据类型不支持
 * 3.使用场景:通过切面获取到controller层方法参数,调用此类可以做对应的操作.
 * @author : Bruce(刘正航) 下午12:11 2018/4/28
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ParamsHandler {

    public static ParamsHandler build(){
        return new ParamsHandler();
    }

    /**仅仅打印参数**/
    public void print(Map<Integer,Object> params){
        this.handle(params, this::printHandler);
    }

    /**对form表单做复杂处理**/
    public void handle(Map<Integer,Object> params){
        printHandler(params);
        this.handler(params, this::simpleFormHandler);
    }

    public void handle(Map<Integer,Object> params,FormHandler formHandler){
        this.handler(params,formHandler);
    }

    /*
     ****************************************私有方法区*******************************************
                   _               _                           _    _                 _
                  (_)             | |                         | |  | |               | |
      _ __   _ __  _ __   __ __ _ | |_  ___   _ __ ___    ___ | |_ | |__    ___    __| |
     | '_ \ | '__|| |\ \ / // _` || __|/ _ \ | '_ ` _ \  / _ \| __|| '_ \  / _ \  / _` |
     | |_) || |   | | \ V /| (_| || |_|  __/ | | | | | ||  __/| |_ | | | || (_) || (_| |
     | .__/ |_|   |_|  \_/  \__,_| \__|\___| |_| |_| |_| \___| \__||_| |_| \___/  \__,_|
     | |
     |_|
     ****************************************私有方法区*******************************************
     */

    /**对参数进行特殊处理**/
    private Object printHandler(Object obj) {
        LogUtils.info(log,"请求参数: {}", JsonUtils.objToStr(obj));
        return obj;
    }

    /**对参数进行特殊处理**/
    private Object simpleFormHandler(Object arg) {
        /**如果是基础数据类型:则直接返回;有字符串,就执行解码操作**/
        if(ClassUtils.isBaseDataType(arg.getClass()) ){
            if (arg instanceof String) {
                return handleStringField(arg);
            }
            return arg;
        }
        /**如果是form表单对象,则做对象属性赋值**/
        List<Field> fields = ClassUtils.getAllFields(arg.getClass());
        for ( Field field : fields ) {

            field.setAccessible(true);

            Object fieldValue = fetchFieldValue(arg, field);

            if (null == fieldValue) {
                continue;
            }

            if (fieldValue instanceof String) {
                String newValue = handleStringField(fieldValue);
                updatePropertyValue(arg, field, newValue);
            }
        }
        return arg;
    }

    /**字符串类型字段的处理**/
    private String handleStringField(Object fieldValue) {
        return decodeChinese(String.valueOf(fieldValue));
    }

    /**获取字段值**/
    private Object fetchFieldValue(Object arg, Field field) {
        Object fieldValue = null;
        try {
            fieldValue = field.get(arg);
        } catch (IllegalAccessException e) {
            ExceptionUtils.printStackTrace(e);
        }
        return fieldValue;
    }


    /**对有中文的值进行解码**/
    private String decodeChinese(String valueStr) {

        try {
            return URLDecoder.decode(valueStr,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            ExceptionUtils.printStackTrace(e);
        }

        return valueStr;
    }

    /**更新字段值**/
    private void updatePropertyValue(Object arg, Field field, String value) {
        try {
            PropertyUtils.setSimpleProperty(arg, field.getName(), value);
        } catch (Exception e) {
            ExceptionUtils.printStackTrace(e);
        }
    }

    private void handler( Map<Integer,Object> params, FormHandler formHandler){
        for ( Map.Entry<Integer,Object> entry : params.entrySet() ) {
            Object arg = entry.getValue();
            Object value = formHandler.handle(arg);
            params.put(entry.getKey(),value);
        }
    }

    private interface FormHandler{
        Object handle(Object arg);
    }
}
