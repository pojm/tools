package com.bruce.tool.controller.annotation;

import com.bruce.tool.controller.config.CorsConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 功能 :
 * 自动配置cors开关
 * 在Application类上注解即可
 * @author : Bruce(刘正航) 下午12:53 2018/6/25
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({CorsConfig.class})
public @interface UseCors {
}
