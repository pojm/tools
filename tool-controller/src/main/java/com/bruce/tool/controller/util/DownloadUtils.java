package com.bruce.tool.controller.util;

import com.bruce.tool.cache.redis.constant.ExpireTime;
import com.bruce.tool.cache.redis.util.RedisUtils;
import com.bruce.tool.common.exception.ExceptionUtils;
import com.bruce.tool.office.excel.constant.Version;
import com.bruce.tool.office.excel.core.Excels;
import com.bruce.tool.controller.exception.ExportLimitException;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

/**
 * 文件下载工具类
 * 注意事项,使用之前,请初始化redis
 * @author : Bruce(刘正航) 上午10:21 2018/2/26
 */
@Slf4j
public class DownloadUtils {

    private static ThreadLocal<DownloadConfig> downloadConfig = new ThreadLocal<>();
    /**默认最大可导出数量限制**/
    private static final Integer DEFAULT_MAX_DOWNLOAD_NUM = 20000;

    @Data
    public class DownloadConfig{
        private Integer maxDownload;
    }

    /**设置下载配置:最大下载限制**/
    public static void setMaxLine(Integer maxDownload){
        DownloadConfig config = downloadConfig.get();
        if( null == config ){
            config = new DownloadUtils().new DownloadConfig();
            downloadConfig.set(config);
        }
        config.setMaxDownload(maxDownload);
    }

    public static Integer getMaxLine(){
        Integer maxDownlaod = DEFAULT_MAX_DOWNLOAD_NUM;
        DownloadConfig config = downloadConfig.get();
        if( null != config && null != config.getMaxDownload() ){
            maxDownlaod = config.getMaxDownload();
        }
        return maxDownlaod;
    }

    public static <T> void download(HttpServletResponse response, String fileName, Class<T> clazz, List<T> list) {
        Excels.Export().config(Version.XLSX,5000).addSheet(list,clazz,fileName).toResponse(response,fileName);
    }

    /**
     * 文件下载
     * 配合@Running注解使用,达到防止重复请求,导致服务器卡死的情况出现
     */
    public static <T> void download(
                                Class<T> clazz,
                                String fileName,
                                HttpServletRequest request,
                                HttpServletResponse response,
                                Callback<T> callback) throws InvocationTargetException, NoSuchMethodException, IllegalAccessException, IOException {
        download(clazz,null,fileName,request,response,callback);
    }

    /**
     * 文件下载
     * 配合@Running注解使用,达到防止重复请求,导致服务器卡死的情况出现
     */
    public static <T> void download(
            Class<T> clazz,
            String methodName,
            String fileName,
            HttpServletRequest request,
            HttpServletResponse response,
            Callback<T> callback) throws IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        // 从缓存中获取数据
        if (DownloadUtils.fetchDataFromCache(request, response, fileName)) return;

        List<T> list = callback.fetch();

        if (!CollectionUtils.isEmpty(list)) {
            Integer maxDownload = getMaxLine();
            if( list.size() > maxDownload ){
                throw new ExportLimitException(maxDownload);
            }
            if( StringUtils.isNotBlank(methodName) ){
                T t = list.get(0);
                Method method = t.getClass().getMethod(methodName);
                fileName = method.invoke(t) + fileName;
            }
        }

        byte[] data = DownloadUtils.saveDataToCache(request, fileName, clazz, list);

        DownloadUtils.setExcelDownloadInfo(response,fileName);

        response.getOutputStream().write(data);

        log.debug("重入锁,下载完毕......................");
    }

    /**
     * 文件下载
     * 配合@Running注解使用,达到防止重复请求,导致服务器卡死的情况出现
     */
    public static void download(
            String fileName,
            HttpServletRequest request,
            HttpServletResponse response,
            ByteCallback callback) throws IOException {
        // 从缓存中获取数据
        if (DownloadUtils.fetchDataFromCache(request, response, fileName)) return;

        byte[] data = callback.fetch();

        DownloadUtils.saveDataToCache(request,fileName,data);

        DownloadUtils.setExcelDownloadInfo(response,fileName);

        response.getOutputStream().write(data);

        log.debug("重入锁,下载完毕......................");
    }


    public static void validMaxDownload(LimitCallback callback) {

        Page<?> pages = PageHelper.startPage(1, 1);

        callback.execute();

        Integer maxDownload = getMaxLine();
        if( pages.getTotal() > maxDownload ){
            throw new ExportLimitException(maxDownload);
        }

        pages.clear();

    }

    /**设置下载信息**/
    public static void setMaxLine(HttpServletResponse response, String fileName) {
        //1.设置文件ContentType类型，这样设置，会自动判断下载文件类型
        response.setContentType("application/form-data");
        try {
            // 转码中文
            fileName = new String(fileName.getBytes("utf-8"), "iso8859-1");
        } catch (UnsupportedEncodingException e) {
            ExceptionUtils.printStackTrace(e);
        }
        // TODO: 2017/4/27 文件扩展名
        response.setHeader("Content-Disposition", "attachment;fileName=" + fileName);
    }

    /**设置下载信息**/
    public static void setExcelDownloadInfo(HttpServletResponse response, String fileName) {
        setMaxLine(response, fileName+".xlsx");
    }

     /*
     ****************************************私有方法区*******************************************
                   _               _                           _    _                 _
                  (_)             | |                         | |  | |               | |
      _ __   _ __  _ __   __ __ _ | |_  ___   _ __ ___    ___ | |_ | |__    ___    __| |
     | '_ \ | '__|| |\ \ / // _` || __|/ _ \ | '_ ` _ \  / _ \| __|| '_ \  / _ \  / _` |
     | |_) || |   | | \ V /| (_| || |_|  __/ | | | | | ||  __/| |_ | | | || (_) || (_| |
     | .__/ |_|   |_|  \_/  \__,_| \__|\___| |_| |_| |_| \___| \__||_| |_| \___/  \__,_|
     | |
     |_|
     ****************************************私有方法区*******************************************
     */

    /**存数据到缓存**/
    private static void saveDataToCache(HttpServletRequest request, String fileName, byte[] data) {
        String dataKey = RequestLimitUtils.orderData(request, request.getMethod());

        //放置数据到redis
        RedisUtils.set(dataKey,data, ExpireTime.ONE_MIN);

        String fileNameKey = RequestLimitUtils.orderFileName(request, request.getMethod());
        RedisUtils.set(fileNameKey,fileName, ExpireTime.ONE_MIN.getTime());
    }

    /**存数据到缓存**/
    private static <T> byte[] saveDataToCache(HttpServletRequest request, String fileName, Class<T> clazz, List<T> list) {
        String dataKey = RequestLimitUtils.orderData(request, request.getMethod());
        // 输出excel
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        Excels.Export().config(Version.XLSX,5000).addSheet(list,clazz,fileName).toStream(bos);

        byte[] data = bos.toByteArray();

        //放置数据到redis
        RedisUtils.set(dataKey,data, ExpireTime.ONE_MIN);

        String fileNameKey = RequestLimitUtils.orderFileName(request, request.getMethod());
        RedisUtils.set(fileNameKey,fileName, ExpireTime.ONE_MIN.getTime());
        return data;
    }

    /**从缓存获取数据**/
    private static boolean fetchDataFromCache(HttpServletRequest request, HttpServletResponse response, String fileName) throws IOException {
        String dataKey = RequestLimitUtils.orderData(request, request.getMethod());
        byte[] bytes = RedisUtils.get(dataKey);

        if( null != bytes && bytes.length > 0 ){
            String fileNameKey = RequestLimitUtils.orderFileName(request,request.getMethod());
            String cacheFileName = RedisUtils.get(fileNameKey);
            if( StringUtils.isNotBlank(cacheFileName) ){
                fileName = cacheFileName;
            }
            DownloadUtils.setExcelDownloadInfo(response,fileName);
            response.getOutputStream().write(bytes);
            return true;
        }

        return false;
    }
}