package com.bruce.tool.web.interceptor.config;

import org.springframework.context.annotation.ComponentScan;

/**
 * 功能 : 
 * web层拦截器注册扫描包路径
 * @author : Bruce(刘正航) 23:25 2019-05-14
 */
@ComponentScan(value = {"com.bruce.tool.web.interceptor"})
public class WebInterceptorRegister {
}