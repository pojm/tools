package com.bruce.tool.web.swagger.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.*;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能 :
 * @author : Bruce(刘正航) 10:07 上午 2019/9/30
 */
@Configuration
public class SwaggerConfig {

    @Autowired
    private SwaggerProperties swagger;

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName(swagger.getProperty("groupName"))
                .host(swagger.getProperty("host"))
                //加载配置信息
                .apiInfo(apiInfo())
                //设置全局参数
//                .globalOperationParameters(globalParamBuilder())
                //设置全局响应参数
                .globalResponseMessage(RequestMethod.GET, responseBuilder())
                .globalResponseMessage(RequestMethod.POST, responseBuilder())
                .globalResponseMessage(RequestMethod.PUT, responseBuilder())
                .globalResponseMessage(RequestMethod.DELETE, responseBuilder())
                .select()
                //加载swagger 扫描包
                .apis(RequestHandlerSelectors.basePackage(swagger.getProperty("basePackage")))
                .paths(PathSelectors.any()).build()
                //设置安全认证
//                .securitySchemes(security())
                ;
    }

    private ApiInfo apiInfo() {
        Contact contact = new Contact(swagger.getProperty("contactName"), swagger.getProperty("ContactUrl"), swagger.getProperty("contactEmail"));
        return new ApiInfoBuilder()
                .title(swagger.getProperty("title"))
                .description(swagger.getProperty("description"))
                .contact(contact)
                .version(swagger.getProperty("version"))
                .build();
    }

    /** 安全认证参数 **/
    private List<ApiKey> security() {
        List<ApiKey> list = new ArrayList<>();
        list.add(new ApiKey("token", "token", "header"));
        return list;
    }

    /**
     * 构建全局参数列表
     */
    private List<Parameter> globalParamBuilder() {
        List<Parameter> pars = new ArrayList<>();
        pars.add(parameterBuilder("token", "令牌", "string", "header", false).build());
        return pars;
    }

    /**
     * 创建参数
     */
    private ParameterBuilder parameterBuilder(String name, String desc, String type, String parameterType, boolean required) {
        ParameterBuilder tokenPar = new ParameterBuilder();
        tokenPar.name(name)
                .description(desc)
                .modelRef(new ModelRef(type))
                .parameterType(parameterType)
                .required(required)
                .build();
        return tokenPar;
    }

    /**
     * 创建全局响应值
     */
    private List<ResponseMessage> responseBuilder() {
        List<ResponseMessage> responseMessageList = new ArrayList<>();
        responseMessageList.add(new ResponseMessageBuilder().code(200).message("响应成功").build());
        responseMessageList.add(new ResponseMessageBuilder().code(500).message("服务器内部错误").build());
        return responseMessageList;
    }
}
