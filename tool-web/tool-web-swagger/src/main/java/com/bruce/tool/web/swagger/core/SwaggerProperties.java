package com.bruce.tool.web.swagger.core;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

/**
 * 功能 :
 * @author : Bruce(刘正航) 10:16 上午 2019/9/30
 */
@Data
@Configuration
@ConfigurationProperties("swagger")
public class SwaggerProperties extends Properties {
}
