package com.bruce.tool.web.swagger.annotation;

import com.bruce.tool.web.swagger.config.SwaggerRegister;
import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import org.springframework.context.annotation.Import;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.lang.annotation.*;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 11:24 2019-02-14
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@EnableSwagger2
@EnableSwaggerBootstrapUI
@Import({SwaggerRegister.class})
public @interface UseSwagger {
}
