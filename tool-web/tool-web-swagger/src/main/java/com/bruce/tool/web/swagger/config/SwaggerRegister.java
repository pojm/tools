package com.bruce.tool.web.swagger.config;

import org.springframework.context.annotation.ComponentScan;

/**
 * 功能 :
 * @author : Bruce(刘正航) 9:43 上午 2019/9/30
 */
@ComponentScan(value = {"com.bruce.tool.web.swagger"})
public class SwaggerRegister {
}
