package com.bruce.tool.web.swagger;

import com.bruce.tool.common.util.SpringBeanUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.ApplicationContext;

/**
 * 功能 :
 * @author : Bruce(刘正航) 9:55 上午 2019/9/30
 */
@ServletComponentScan
@SpringBootApplication
public class SwaggerApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(SwaggerApplication.class, args);
        SpringBeanUtils.setApplicationContext(context);
    }
}
