package com.bruce.tool.web.filter.core;

import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 功能 : 
 *
 * @author : Bruce(刘正航) 23:12 2019-05-14
 */
public class ParamCaseInsensitiveFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain) throws ServletException, IOException {
        filterChain.doFilter(new ParamCaseInsensitiveRequestWraper(request),response);
    }

}
