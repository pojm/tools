package com.bruce.tool.web.filter.annotation;

import com.bruce.tool.web.filter.core.ParamLoggerFilter;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 功能 :
 * 开启web层日志功能
 * @author : Bruce(刘正航) 09:21 2019-01-31
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import({ParamLoggerFilter.class})
public @interface OpenRequestLogger {
}
