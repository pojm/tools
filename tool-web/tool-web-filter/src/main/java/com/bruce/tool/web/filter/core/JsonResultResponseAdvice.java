package com.bruce.tool.web.filter.core;

import com.bruce.tool.web.filter.vo.ErrorCode;
import com.bruce.tool.web.filter.vo.JsonResult;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * 功能 :
 * @author : Bruce(刘正航) 11:03 上午 2020/1/8
 */
@ControllerAdvice
public class JsonResultResponseAdvice implements ResponseBodyAdvice {
    @Override
    public boolean supports(MethodParameter methodParameter, Class aClass) {
        return true;
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType,
            MediaType selectedContentType, Class selectedConverterType,
            ServerHttpRequest request, ServerHttpResponse response) {
        JsonResult result = new JsonResult();
        result.setCode(ErrorCode.SUCCESS);
        result.setMessage("请求成功");
        result.setData(body);
        return result;
    }
}
