package com.bruce.tool.web.filter.vo;

import lombok.Data;

/**
 * 功能 :
 * @author : Bruce(刘正航) 11:19 上午 2020/1/8
 */
@Data
public class JsonpResult extends JsonResult{
    private String function;
}