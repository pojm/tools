package com.bruce.tool.web.filter.config;

import com.bruce.tool.web.filter.convert.DateConverter;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 功能 :
 * 开启请求参数不敏感配置
 * @author : Bruce(刘正航) 00:54 2019-05-15
 */
public class DateFormatConfigurer implements WebMvcConfigurer {
    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new DateConverter());
    }
}