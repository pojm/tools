package com.bruce.tool.web.filter.core;

import org.springframework.util.LinkedCaseInsensitiveMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Map;

/**
 * 功能 : 
 *
 * @author : Bruce(刘正航) 23:13 2019-05-14
 */
public class ParamCaseInsensitiveRequestWraper extends HttpServletRequestWrapper {

    private final LinkedCaseInsensitiveMap<String[]> map = new LinkedCaseInsensitiveMap<>();

    public ParamCaseInsensitiveRequestWraper(final HttpServletRequest request) {
        super(request);
        map.putAll(request.getParameterMap());
    }

    @Override
    public String getParameter(final String name) {
        String[] array = this.map.get(name);
        if (array != null && array.length > 0) {
            return array[0];
        }
        return null;
    }

    @Override
    public Map<String, String[]> getParameterMap() {
        return Collections.unmodifiableMap(this.map);
    }

    @Override
    public Enumeration<String> getParameterNames() {
        return Collections.enumeration(this.map.keySet());
    }

    @Override
    public String[] getParameterValues(String name) {
        return this.map.get(name);
    }
}
