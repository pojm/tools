package com.bruce.tool.web.filter.vo;

/**
 * 功能 :
 * @author : Bruce(刘正航) 11:10 上午 2020/1/8
 */
public class ErrorCode {
    /**成功，没有错误**/
    public final static byte SUCCESS = 0;
    /**没有登录，或没有进行认证**/
    public final static byte NOT_LOGGED_IN = 1;
    /**数据验证失败**/
    public final static byte DATA_CHECK_FAILURE = 2;
    /**业务操作异常**/
    public final static byte BUSINESS_EXCEPTION = 3;
    /**没有操作权限**/
    public final static byte NOT_AUTHORITY = 4;
    /**数据操作异常**/
    public final static byte DATA_OPERATION_EXCEPTION = 5;
    /**显示层异常**/
    public final static byte VIEW_EXCEPTION = 6;
    /**授权码非法**/
    public final static byte TOKEN_EXCEPTION = 7;
    /**系统错误**/
    public final static byte SYSTEM_ERROR = 127;
}
