package com.bruce.tool.web.filter.annotation;

/**
 * 功能 :
 * @author : Bruce(刘正航) 11:29 上午 2020/1/8
 */

import com.bruce.tool.web.filter.core.JsonResultResponseAdvice;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import({JsonResultResponseAdvice.class})
public @interface EnableJsonResult {
}
