package com.bruce.tool.web.filter.core;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

/**
 * 功能 : 
 *
 * @author : Bruce(刘正航) 00:04 2019-05-16
 */
@Slf4j
public class ParamLoggerFilter extends CommonsRequestLoggingFilter {
    public ParamLoggerFilter() {
        super.setIncludeQueryString(true);
        super.setIncludeClientInfo(true);
        super.setIncludePayload(true);
        super.setIncludeHeaders(true);
        super.setMaxPayloadLength(500);
    }
}
