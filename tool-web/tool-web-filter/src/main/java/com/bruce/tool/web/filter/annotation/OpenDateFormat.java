package com.bruce.tool.web.filter.annotation;

import com.bruce.tool.web.filter.config.DateFormatConfigurer;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 功能 :
 * 开启请求参数不敏感配置
 * @author : Bruce(刘正航) 09:21 2019-01-31
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import({DateFormatConfigurer.class})
public @interface OpenDateFormat {
}
