package com.bruce.tool.web.filter.vo;

import com.bruce.tool.common.exception.BaseResult;
import com.bruce.tool.common.util.ClassUtils;
import com.bruce.tool.common.util.string.JsonUtils;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * 功能:
 * 结果集
 * @author : Bruce(刘正航) 上午10:21 2018/2/26
 */
@Data
@NoArgsConstructor(access = AccessLevel.PRIVATE)  //私有的无参构造函数
public class Result extends BaseResult {
	
	private static Result newInstance(){
		return new Result();
	}

	public static String success() {
		Result result = newInstance();
		result.setData("");
		result.setCode(String.valueOf(0));
		result.setMessage("success");
		return JsonUtils.objToStr(result,true);
	}

	public static String success(Object data) {
		Result result = newInstance();
		result.setData(data);
		result.setCode(String.valueOf(0));
		result.setMessage("success");
		if (data == null) {
			result.setData("");
		}else{
			if( !ClassUtils.isExtendDataType(data) && !ClassUtils.hasField(data) ){
				result.setData("");
			}
		}

        return JsonUtils.objToStr(result,true);
    }

    public static String failure(Object errorCode, String message) {
    	Result result = newInstance();
        result.setCode(errorCode);
        result.setMessage(message);
		return JsonUtils.objToStr(result,true);
    }

	public static String failure(Object errorCode, String message, Object data) {
		Result result = newInstance();
		result.setCode(errorCode);
		result.setMessage(message);
		result.setData(data);
		if (data == null) {
			result.setData("");
		}
		return JsonUtils.objToStr(result,true);
	}
}
