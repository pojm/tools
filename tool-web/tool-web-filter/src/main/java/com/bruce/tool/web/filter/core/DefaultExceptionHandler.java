package com.bruce.tool.web.filter.core;

import com.bruce.tool.common.exception.BaseResult;
import com.bruce.tool.common.exception.ExceptionUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 功能 :
 * 异常处理;
 * 继承此类,可进行大部分异常的处理
 * 在方法上加一下注解
 * @author : Bruce(刘正航) 下午3:23 2018/4/27
 */
@Slf4j
@ControllerAdvice
public class DefaultExceptionHandler {

    @ResponseBody
    @ExceptionHandler(Throwable.class)
    public BaseResult exceptionHandler(Throwable ex) {
        return ExceptionUtils.printErrorInfos(ex);
    }

}