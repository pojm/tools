package com.bruce.tool.web.filter.annotation;

import com.bruce.tool.web.filter.core.DefaultExceptionHandler;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 功能 :
 * @author : Bruce(刘正航) 12:55 下午 2020/1/8
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import({DefaultExceptionHandler.class})
public @interface EnableExceptionHandler {
}
