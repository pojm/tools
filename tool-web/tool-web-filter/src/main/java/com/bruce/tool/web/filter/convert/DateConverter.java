package com.bruce.tool.web.filter.convert;

import com.bruce.tool.common.util.DateUtils;
import com.bruce.tool.common.util.regex.RegexUtils;
import com.bruce.tool.common.util.string.StringUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;

import java.util.Date;

/**
 * 功能 :
 * 日期转换
 * @author : Bruce(刘正航) 23:25 2019-05-14
 */
public class DateConverter implements Converter<String, Date> {
     @Override
     public Date convert(@Nullable String source) {
         if(StringUtils.isBlank(source)){ return null; }
         if( RegexUtils.validDate(source, DateUtils.Parttern.FORMAT_YYMM_MID) ){
             return DateUtils.parse(source,DateUtils.Parttern.FORMAT_YYMM_MID).toDate();
         }
         if( RegexUtils.validDate(source,DateUtils.Parttern.FORMAT_YYMMDD_MID) ){
             return DateUtils.parse(source,DateUtils.Parttern.FORMAT_YYMMDD_MID).toDate();
         }
         if( RegexUtils.validDate(source,DateUtils.Parttern.FORMAT_YYMMDDH_MID) ){
             return DateUtils.parse(source,DateUtils.Parttern.FORMAT_YYMMDDH_MID).toDate();
         }
         if( RegexUtils.validDate(source,DateUtils.Parttern.FORMAT_YYMMDDHM_MID) ){
             return DateUtils.parse(source,DateUtils.Parttern.FORMAT_YYMMDDHM_MID).toDate();
         }
         if( RegexUtils.validDate(source,DateUtils.Parttern.FORMAT_YYMMDDHMS_MID) ){
             return DateUtils.parse(source,DateUtils.Parttern.FORMAT_YYMMDDHMS_MID).toDate();
         }
         if( RegexUtils.validDate(source,DateUtils.Parttern.FORMAT_YYMMDDHMSS_MID) ){
             return DateUtils.parse(source,DateUtils.Parttern.FORMAT_YYMMDDHMSS_MID).toDate();
         }
         if( RegexUtils.validTimeStamp(source) ){
             if( source.length() == 10 ){
                 return DateUtils.create(Integer.parseInt(source)*1000).toDate();
             }
             if( source.length() == 13 ){
                 return DateUtils.create(Integer.parseInt(source)).toDate();
             }
         }
         return null;
     }
 }