package com.bruce.tool.address.mysql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 *
 * 功能 :
 * @author : Bruce(刘正航) 上午12:49 2018/11/16
 */
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
public class AddressDaoApplication {

    public static void main(String[] args) {
        SpringApplication.run(AddressDaoApplication.class);
    }
}
