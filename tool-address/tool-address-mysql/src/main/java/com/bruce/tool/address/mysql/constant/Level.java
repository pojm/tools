package com.bruce.tool.address.mysql.constant;

import lombok.Getter;

/**
 * 功能 :
 * 地址级别(全国0,省1,市2,区3,乡镇4)
 * @author : Bruce(刘正航) 3:16 PM 2018/11/19
 */
public enum Level {
    //
    COUNTRY(0,"全国"),
    //
    PROVINCE(1,"省"),
    //
    CITY(2,"市"),
    //
    COUNTY(3,"区"),
    //
    TOWN(4,"乡镇"),
    //
    STREET(5,"街道"),

    ;
    @Getter
    private Integer code;
    @Getter
    private String desc;

    Level(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static String valueOf(Integer code){
        for (Level level : values()){
            if(level.getCode().equals(code)){
                return level.getDesc();
            }
        }
        return null;
    }
}
