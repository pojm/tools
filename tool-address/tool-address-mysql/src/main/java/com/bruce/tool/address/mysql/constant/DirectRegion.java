package com.bruce.tool.address.mysql.constant;

import lombok.Getter;

/**
 * 功能 :
 * 直辖市
 * @author : Bruce(刘正航) 3:19 PM 2018/11/19
 */
public enum DirectRegion {
    //
    BJ("北京市","BEIJING"),
    //
    SH("上海市","SHANGHAI"),
    //
    TJ("天津市","TIANJING"),
    //
    CQ("重庆市","CHONGQING"),

    ;
    @Getter
    private String name;
    @Getter
    private String pinyin;

    DirectRegion(String name, String pinyin) {
        this.name = name;
        this.pinyin = pinyin;
    }
    /**是否是直辖市判断**/
    public static boolean isDirect(String name){
        for (DirectRegion region : DirectRegion.values()) {
            if( region.getName().contains(name) ){
                return true;
            }
        }
        return false;
    }
}
