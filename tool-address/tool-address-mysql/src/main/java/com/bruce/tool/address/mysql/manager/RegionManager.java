package com.bruce.tool.address.mysql.manager;

import com.bruce.tool.address.mysql.constant.Level;
import com.bruce.tool.address.mysql.domain.Region;
import com.bruce.tool.address.mysql.domain.RegionExample;
import com.bruce.tool.address.mysql.service.RegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 功能 : 
 *
 * @author : Bruce(刘正航) 14:08 2019-05-08
 */
@Component
public class RegionManager {

    @Autowired
    private RegionService regionService;

    /**获取所有省**/
    public List<Region> findProvinces(){
        RegionExample example = new RegionExample().createCriteria()
                .andLevelEqualTo(Level.PROVINCE.getCode())
                .example();
        return this.regionService.findsByExample(example);
    }

    /**根据省,获取下面所有的城市**/
    public List<Region> findCitiesByProvinceCode(String provinceCode){
        return this.findRegionsByPcode(provinceCode);
    }

    /**根据城市,获取下面所有的区/县**/
    public List<Region> findCountiesByCityCode(String cityCode){
        return this.findRegionsByPcode(cityCode);
    }

    /**根据区/县,获取下面所有的乡镇**/
    public List<Region> findTownsByCountyCode(String countyCode){
        return this.findRegionsByPcode(countyCode);
    }

    /**根据乡镇,获取下面所有的街道**/
    public List<Region> findStreetsByTownCode(String townCode){
        return this.findRegionsByPcode(townCode);
    }

    /**根据父节点,获取下面所有的子节点**/
    public List<Region> findRegionsByPcode(String regionCode){
        RegionExample example = new RegionExample().createCriteria()
                .andPcodeEqualTo(regionCode)
                .example();
        return this.regionService.findsByExample(example);
    }
}
