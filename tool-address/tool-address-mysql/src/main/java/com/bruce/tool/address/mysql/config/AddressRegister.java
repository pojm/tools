package com.bruce.tool.address.mysql.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 09:53 2019-01-31
 */
@MapperScan(basePackages = {"com.bruce.tool.address.mysql.mapper"})
@ComponentScan(basePackages = {
        "com.bruce.tool.address.mysql.manager",
        "com.bruce.tool.address.mysql.service"
})
public class AddressRegister {
}
