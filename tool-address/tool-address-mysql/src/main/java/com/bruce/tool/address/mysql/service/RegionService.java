package com.bruce.tool.address.mysql.service;

import com.bruce.tool.address.mysql.domain.Region;
import com.bruce.tool.orm.mybatis.core.service.BaseService;

public interface RegionService extends BaseService<Region> {
}