package com.bruce.tool.address.mysql.mapper;

import com.bruce.tool.address.mysql.domain.Region;
import com.bruce.tool.orm.mybatis.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RegionMapper extends BaseMapper<Region> {
}