package com.bruce.tool.address.mysql.annotation;

import com.bruce.tool.address.mysql.config.AddressRegister;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 09:52 2019-01-31
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import({AddressRegister.class})
public @interface UseAddress {
}
