CREATE TABLE `global_region` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `level` int(10) DEFAULT NULL COMMENT '地址级别(全国0,省1,市2,区3,乡镇4)',
  `is_direct` int(4) DEFAULT NULL COMMENT '是否是直辖市(1是,0不是)',
  `pcode` varchar(64) DEFAULT NULL COMMENT '父节点编码',
  `code` varchar(64) DEFAULT NULL COMMENT '当前地址节点编码',
  `name` varchar(64) DEFAULT NULL COMMENT '地址名称',
  `href` varchar(512) DEFAULT NULL COMMENT '地址抓取路径',
  `referer` varchar(512) DEFAULT NULL COMMENT '地址路径父页面路径',
  `longitude` double(8,8) DEFAULT NULL COMMENT '经度',
  `latitude` double(8,8) DEFAULT NULL COMMENT '维度',
  PRIMARY KEY (`id`),
  KEY `idx_level` (`level`),
  KEY `idx_isdirect` (`is_direct`),
  KEY `idx_pcode` (`pcode`),
  KEY `idx_code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3662 DEFAULT CHARSET=utf8 COMMENT='地址码表-基础表';