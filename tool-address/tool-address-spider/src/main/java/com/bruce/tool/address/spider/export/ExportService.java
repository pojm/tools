package com.bruce.tool.address.spider.export;

import com.bruce.tool.address.mysql.constant.Level;
import com.bruce.tool.address.mysql.domain.Region;
import com.bruce.tool.address.mysql.domain.RegionExample;
import com.bruce.tool.address.mysql.service.RegionService;
import com.bruce.tool.common.util.sort.SortUtils;
import com.bruce.tool.office.excel.constant.Version;
import com.bruce.tool.office.excel.core.Excels;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 功能 :
 * 导出地址源数据
 * @author : Bruce(刘正航) 4:44 下午 2020/1/6
 */
@Service
public class ExportService {

    @Autowired
    private RegionService regionService;

    public void doExport(){
        String file = System.getProperty("user.home") + "/Desktop/address-2018.xlsx";
        List<Region> regions = regionService.findsByExample(new RegionExample().createCriteria()
                .example());
        List<Region> sorted = SortUtils.tree(regions, "code", "pcode", "000000000000");
        List<RegionExport> exports = Lists.newArrayList();
        for (Region region : sorted){
            if(Level.CITY.getCode().equals(region.getLevel())){
                region.setName("        "+region.getName());
            }
            if(Level.COUNTY.getCode().equals(region.getLevel())){
                region.setName("                "+region.getName());
            }
            RegionExport export = new RegionExport();
            export.setName(region.getName());
            export.setIsDirect(region.getIsDirect() == 1?"是":"否");
            export.setLevel(Level.valueOf(region.getLevel()));
            export.setPcode(region.getPcode());
            export.setCode(region.getCode());
            exports.add(export);
        }
        Excels.Export().config(Version.XLSX)
                .addSheet(exports,RegionExport.class,"2018")
                .toFile(file);
    }
}
