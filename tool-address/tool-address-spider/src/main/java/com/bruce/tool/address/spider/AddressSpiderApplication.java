package com.bruce.tool.address.spider;

import com.bruce.tool.address.mysql.annotation.UseAddress;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 *
 * 功能 :
 * @author : Bruce(刘正航) 上午12:49 2018/11/16
 */
@UseAddress
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
public class AddressSpiderApplication {

    public static void main(String[] args) {
        SpringApplication.run(AddressSpiderApplication.class, args);
    }
}
