package com.bruce.tool.address.spider.handler;

import com.bruce.tool.address.mysql.domain.Region;
import com.bruce.tool.address.spider.constant.RegionType;
import com.bruce.tool.address.spider.resolver.HrefCrawler;
import com.bruce.tool.address.spider.resolver.HtmlDecoder;
import com.google.common.collect.Lists;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 3:10 PM 2018/11/19
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RegoinFetcher {

    /**获取年入口**/
    public static String fetchYear(String year,String url){
        byte[] first = HrefCrawler.fetchHtmlByUrl(url,"");
        String content = HtmlDecoder.decodeHtml(first);
        return RegionParser.parseYear(content,year);
    }

    /**获取并解析省市区数据**/
    public static List<Region> fetchRegions(RegionType regionType, String href, String referer, Region parent){
        byte[] first = HrefCrawler.fetchHtmlByUrl(href,referer);
        String content = HtmlDecoder.decodeHtml(first);
        switch(regionType){
            case PROVINCE:
                return RegionParser.parseProvinces(content,href);
            case CITY:
                return RegionParser.parseCities(content,href,parent);
            case COUNTY:
                return RegionParser.parseCounties(content,href,parent);
            case TOWN:
                return RegionParser.parseTowns(content,href,parent);
            case STREET:
                return RegionParser.parseStreets(content,href,parent);
            default:
                break;
        }
        return Lists.newArrayList();
    }

}
