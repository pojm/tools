package com.bruce.tool.address.spider.resolver;

import com.bruce.tool.common.util.LogUtils;
import com.bruce.tool.rpc.http.core.Https;
import com.bruce.tool.rpc.http.handler.request.RequestType;
import com.bruce.tool.rpc.http.handler.response.ByteResponseHandler;
import com.google.common.base.Strings;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Objects;
import java.util.Random;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 3:00 PM 2018/11/19
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class HrefCrawler {

    private static Random random = new Random();

    /**根据url地址获取元数据**/
    public static byte[] fetchHtmlByUrl(String url, String referer) {
        int delay = random.nextInt(100);
        try {
            Thread.sleep(delay);
        } catch (InterruptedException e) {
            LogUtils.error(log,e);
            Thread.currentThread().interrupt();
        }

        LogUtils.info(log,"延迟:{}毫秒,请求地址:{}", Strings.padStart(String.valueOf(delay),4,'0'),url);
        byte[] first = Https.create().print(false)
                .url(url)
                .addHeader("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*\\/*;q=0.8")
                .addHeader("Accept-Language","zh-CN,zh;q=0.9,en;q=0.8,id;q=0.7,ru;q=0.6,sq;q=0.5,zh-TW;q=0.4")
                .addHeader("Cache-Control","max-age=0")
                .addHeader("Connection","keep-alive")
                .addHeader("Cookie","AD_RS_COOKIE=20085416; _trs_uv=jtgmdpit_6_ja7; _trs_ua_s_1=jtgmdpis_6_2vlb")
                .addHeader("Host","www.stats.gov.cn")
                .addHeader("If-Modified-Since","Thu, 31 Jan 2019 05:53:29 GMT")
                .addHeader("If-None-Match","1c98-580baa54b4840-gzip")
                .addHeader("Upgrade-Insecure-Requests","1")
                .addHeader("User-Agent","Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36")
                .addHeader("Referer",referer)
                .retry(10,new Integer[]{200,500,1000,1500})
                .execute(RequestMethod.GET, RequestType.URL, new ByteResponseHandler());
        Https.create().print(false).url("http://www.stats.gov.cn/images/banner.jpg");
        Https.create().print(false).url("http://www.stats.gov.cn/favicon.ico");
        Https.create().print(false).url("http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2017/images/borderBottom.gif");
        Https.create().print(false).url("http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2017/images/rightBorder.gif");
        Https.create().print(false).url("http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2017/images/rightBorder.gif");
        Https.create().print(false).url("http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2017//images/topLine.gif");
        if(Objects.isNull(first) || first.length == 0 ){
            return new byte[0];
        }
        return first;
    }

}
