package com.bruce.tool.address.spider.dto;

import com.bruce.tool.address.mysql.domain.Region;
import com.google.common.collect.Lists;
import lombok.Data;

import java.util.List;

/**
 * 功能 :
 * 地址码表
 * @author : Bruce(刘正航) 3:45 PM 2018/11/17
 */
@Data
public class RegionTable {

    /**省**/
    private List<Region> provinces = Lists.newArrayList();
    /**市**/
    private List<Region> cities = Lists.newArrayList();
    /**区/县**/
    private List<Region> counties = Lists.newArrayList();
    /**乡镇**/
    private List<Region> towns = Lists.newArrayList();
    /**街道**/
    private List<Region> streets = Lists.newArrayList();

}
