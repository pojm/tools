package com.bruce.tool.address.spider;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 2:54 PM 2018/11/19
 */
public interface SpiderRunner {
    void start();
}
