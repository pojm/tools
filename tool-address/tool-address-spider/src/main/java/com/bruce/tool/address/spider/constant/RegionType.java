package com.bruce.tool.address.spider.constant;

import lombok.Getter;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 11:22 2019-03-20
 */
public enum RegionType {
    //
    PROVINCE("省"),
    CITY("市"),
    COUNTY("区/县"),
    TOWN("乡镇"),
    STREET("街道")
    ;

    RegionType(String name) {
        this.name = name;
    }
    @Getter
    private String name;
}
