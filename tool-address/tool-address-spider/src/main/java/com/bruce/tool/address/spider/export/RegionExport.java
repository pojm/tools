package com.bruce.tool.address.spider.export;

import com.bruce.tool.office.excel.annotation.Header;
import lombok.Data;
import org.apache.poi.ss.usermodel.HorizontalAlignment;

/**
 * 功能 :
 * @author : Bruce(刘正航) 6:14 下午 2020/1/6
 */
@Data
public class RegionExport {
    @Header(name = "名称",align = HorizontalAlignment.LEFT)
    private String name;
    @Header(name = "是否直辖市")
    private String isDirect;
    @Header(name = "层级")
    private String level;
    @Header(name = "上级编码")
    private String pcode;
    @Header(name = "地址编码")
    private String code;
}
