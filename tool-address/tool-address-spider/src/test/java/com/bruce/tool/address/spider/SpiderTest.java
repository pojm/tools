package com.bruce.tool.address.spider;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 4:18 PM 2018/11/16
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AddressSpiderApplication.class)
public class SpiderTest {

    @Autowired
    private Spider spider;

    @Test
    public void crawl(){
        spider.start();
    }
}
