package com.bruce.tool.address.spider.export;

import com.bruce.tool.address.spider.AddressSpiderApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 功能 :
 * @author : Bruce(刘正航) 5:23 下午 2020/1/6
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = AddressSpiderApplication.class)
public class ExportServiceTest {

    @Autowired
    private ExportService exportService;

    @Test
    public void doExport() {
        exportService.doExport();
    }
}