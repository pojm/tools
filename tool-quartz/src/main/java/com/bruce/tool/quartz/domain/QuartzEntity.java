package com.bruce.tool.quartz.domain;

import lombok.Data;

/**
 * 任务类
 * @author : Bruce(刘正航) 上午12:49 2018/11/16
 */
@Data
public class QuartzEntity {
	/**任务名称**/
	private String jobName;
	/**任务分组**/
	private String jobGroup;
	/**任务描述**/
	private String description;
	/**执行类**/
	private String jobClassName;
	/**执行时间**/
	private String cronExpression;
	/**执行时间**/
	private String triggerName;
	/**任务状态**/
	private String triggerState;
	/**任务名称 用于修改**/
	private String oldJobName;
	/**任务分组 用于修改**/
	private String oldJobGroup;

	private String triggerUrl;
	private String statusUrl;

	public QuartzEntity() {
		super();
	}
	public QuartzEntity(String jobName, String jobGroup, String description, String jobClassName, String cronExpression, String triggerName) {
		super();
		this.jobName = jobName;
		this.jobGroup = jobGroup;
		this.description = description;
		this.jobClassName = jobClassName;
		this.cronExpression = cronExpression;
		this.triggerName = triggerName;
	}

}
