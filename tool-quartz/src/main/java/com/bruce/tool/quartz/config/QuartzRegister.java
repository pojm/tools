package com.bruce.tool.quartz.config;

import org.springframework.context.annotation.ComponentScan;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 09:23 2019-01-31
 */
@ComponentScan(value = {"com.bruce.tool.quartz"})
public class QuartzRegister {
}
