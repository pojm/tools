package com.bruce.tool.quartz;

import com.bruce.tool.common.util.LogUtils;
import com.bruce.tool.common.util.string.JsonUtils;
import com.bruce.tool.quartz.core.QuartzService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 16:12 2019-02-20
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = QuartzApplication.class)
public class ConfigTest {

    @Autowired
    private QuartzService quartzService;

    @Test
    public void config(){
        LogUtils.info(log, JsonUtils.objToStr(quartzService.findAll()));
    }

}
