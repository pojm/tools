package com.bruce.tool.cache.redis;

import com.bruce.tool.cache.redis.constant.ExpireTime;
import com.bruce.tool.cache.redis.util.RedisUtils;
import com.bruce.tool.common.util.SpringBeanUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 5:15 PM 2018/12/18
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RedisApplication.class)
@AutoConfigureMockMvc
@WebAppConfiguration
public class RedisTest extends AbstractJUnit4SpringContextTests {

    @Before
    public void init(){
        SpringBeanUtils.setApplicationContext(applicationContext);
    }

    @Test
    public void add(){
        RedisUtils.set("name_begin","3219321");
    }

    @Test
    public void addExpire(){
        RedisUtils.set("name_begin","3219321", ExpireTime.FIVE_MIN);
    }

    @Test
    public void get(){
        System.out.println(RedisUtils.get("name_begin").toString());
    }
}
