package com.bruce.tool.cache.redis.lock;

import com.bruce.tool.common.exception.ExceptionUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/**
 * 功能 :
 * redis实现分布式锁
 * 使用流程:
 * 1.加锁
 *      加锁失败,抛异常
 * 2.业务逻辑
 *      业务逻辑异常,自动超时解锁
 * 3.解锁
 *      解锁失败,自动超时解锁
 * @author : Bruce(刘正航) 上午12:49 2018/1/7
 */
@Slf4j
@Component
public class RedisLock {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     *
     * @param key 锁KEY
     * @param value 当前时间+超时时间
     * @return
     */
    public boolean lock(String key, Long value) {
        if (stringRedisTemplate.opsForValue().setIfAbsent(key, value.toString())) {
            return true;//加锁成功就返回true
        }
        //不加下面这个可能出现死锁情况
        //代码value加了过期时间* @core value 当前时间+超时时间
        //获取上一个锁的时间,并判断是否小于当前时间,小于就下一步判断,就返回true加锁成功
        //currentValue=A 这两个线程的value都是B 其中一个线程拿到锁
        String currentValue = stringRedisTemplate.opsForValue().get(key);
        //如果锁过期
        if (!StringUtils.isEmpty(currentValue)
                && Long.parseLong(currentValue) < System.currentTimeMillis()) {//存储时间要小于当前时间
            // 出现死锁的另一种情况,当多个线程进来后都没有返回true,接着往下执行,执行代码有先后,而if判断里只有一个线程才能满足条件
            // oldValue=currentValue
            // 多个线程进来后只有其中一个线程能拿到锁(即oldValue=currentValue),其他的返回false
            // 获取上一个锁的时间
            String oldValue = stringRedisTemplate.opsForValue().getAndSet(key, value.toString());
            //上一个时间不为空,并且等于当前时间
            if (!StringUtils.isEmpty(oldValue) && oldValue.equals(currentValue)) {
                return true;
            }
        }
        log.error("[redis分布式锁] 加锁失败");
        return false;
    }


    /**
     * 执行删除可能出现异常需要捕获
     */
    public void unlock(String key, Long value) {
        try {
            String currentValue = stringRedisTemplate.opsForValue().get(key);
            //如果不为空,就删除锁
            if (!StringUtils.isEmpty(currentValue) && currentValue.equals(value.toString())) {
                stringRedisTemplate.opsForValue().getOperations().delete(key);
            }
        } catch (Exception e) {
            ExceptionUtils.printStackTrace(e);
        }
    }
}