package com.bruce.tool.cache.redis.util;

import com.bruce.tool.cache.redis.constant.ExpireTime;
import com.bruce.tool.common.exception.ExceptionUtils;
import com.bruce.tool.common.util.SpringBeanUtils;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.concurrent.TimeUnit;

/**
 * redis缓存工具类
 * 注意事项:
 * 使用之前,请初始化redis链接
 * @author Lee 2017/8/24
 * @author : Bruce(刘正航) 下午10:59 2018/5/2
 */
@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RedisUtils {

    private static RedisTemplate<String, Object> redisTemplate = (RedisTemplate<String, Object>) SpringBeanUtils.getBean("redisTemplate");

    /**
     * 将value对象写入缓存
     * @param key
     * @param value
     * @param time 失效时间(秒)
     */
    public static void set(String key,Object value,ExpireTime time){
        set(key,value,time.getTime());
    }

    /**
     * 将value对象写入缓存
     * @param key
     * @param value
     * @param seconds 失效时间(秒)
     */
    public static void set(String key,Object value,int seconds){
        redisTemplate.opsForValue().set(key, value);
        if(seconds > 0){
            redisTemplate.expire(key, seconds, TimeUnit.SECONDS);
        }
    }

    /**
     * 将value对象写入缓存,不失效
     * @param key
     * @param value
     */
    public static void set(String key,Object value){
        try {
            ValueOperations<String, Object> operations = redisTemplate.opsForValue();
            operations.set(key, value);
        } catch (Exception e) {
            ExceptionUtils.printStackTrace(e);
        }
    }

    /**
     * 获取缓存<br>
     * 注：基本数据类型(Character除外)，请直接使用get(String key, Class<T> clazz)取值
     * @param key
     * @return
     */
    public static <T> T get(String key){
        return (T)redisTemplate.boundValueOps(key).get();
    }

    /**删除**/
    public static void delete(String key) {
        redisTemplate.delete(key);
    }
}