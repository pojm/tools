package com.bruce.tool.orm.mybatis.core.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;

import java.util.List;

/**
 * 功能 :
 * mybatis数据更新功能
 * 1.update(domain)
 * 2.updateSelective(domain)
 * 3.updateAll(List<domain> domains)
 * 4.updateBySQL(sql)
 * @author : Bruce(刘正航) 21:15 2019-01-20
 */
@SuppressWarnings("all")
public interface UpdateMapper<T> {

    @UpdateProvider(type = com.bruce.tool.orm.mybatis.core.provider.UpdateProvider.class, method = "update")
    Integer update(T domain);

    @UpdateProvider(type = com.bruce.tool.orm.mybatis.core.provider.UpdateProvider.class, method = "updateBySelective")
    Integer updateBySelective(T domain);

    @UpdateProvider(type = com.bruce.tool.orm.mybatis.core.provider.UpdateProvider.class, method = "updateAll")
    Integer updateAll(@Param("list") List<T> list);
}
