package com.bruce.tool.orm.mybatis.interceptor;

import com.google.common.collect.Lists;
import lombok.Setter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.DefaultReflectorFactory;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.SystemMetaObject;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.type.TypeHandlerRegistry;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

/**
 * 功能 :
 * 1.借用Mybatis的xml组装可执行的SQL.
 * 2.将可执行的远程SQL,作为方法结果返回.
 * @author : Bruce(刘正航) 08:44 2019-09-05
 */
@Component
@Intercepts({@Signature(type = StatementHandler.class, method = "prepare", args = {Connection.class, Integer.class})})
public class SqlJointInterceptor implements Interceptor {

    @Setter
    private String methodPrefix;

    @Setter
    private String methodSuffix;

    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {
    }

    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        StatementHandler statementHandler = (StatementHandler) invocation.getTarget();

        MetaObject metaObject = MetaObject.forObject(statementHandler, SystemMetaObject.DEFAULT_OBJECT_FACTORY, SystemMetaObject.DEFAULT_OBJECT_WRAPPER_FACTORY,new DefaultReflectorFactory());
        MappedStatement mappedStatement = (MappedStatement) metaObject.getValue("delegate.mappedStatement");

        String methodId = mappedStatement.getId();
        String methodName = StringUtils.reverse(StringUtils.reverse(methodId).replaceFirst("\\..*",""));
        if(!methodId.endsWith(methodSuffix) && !methodName.startsWith(methodPrefix)){
            return invocation.proceed();
        }

        //获取sql语句
        String remoteSql = fillDataToSQL(statementHandler, mappedStatement);
        String executeSql = createNewSql(remoteSql);

        // 设置新的SQL
        resetSql2Invocation(statementHandler, mappedStatement, executeSql);

        return invocation.proceed();
    }

    /**填充参数到SQL中**/
    private String fillDataToSQL(StatementHandler statementHandler, MappedStatement mappedStatement) {

        BoundSql boundSql = statementHandler.getBoundSql();

        Configuration configuration = mappedStatement.getConfiguration();

        Object parameterObject = boundSql.getParameterObject();

        List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();

        String sql = boundSql.getSql().replaceAll("[\\s]+", " ");

        if(CollectionUtils.isEmpty(parameterMappings) || null == parameterObject){
            return sql;
        }

        TypeHandlerRegistry typeHandlerRegistry = configuration.getTypeHandlerRegistry();

        if (typeHandlerRegistry.hasTypeHandler(parameterObject.getClass())) {
            sql = sql.replaceFirst("\\?", getParameterValue(parameterObject));
        } else {
            MetaObject metaObject = configuration.newMetaObject(parameterObject);
            for (ParameterMapping parameterMapping : parameterMappings) {
                String propertyName = parameterMapping.getProperty();
                if (metaObject.hasGetter(propertyName)) {
                    Object obj = metaObject.getValue(propertyName);
                    sql = sql.replaceFirst("\\?", getParameterValue(obj));
                } else if (boundSql.hasAdditionalParameter(propertyName)) {
                    Object obj = boundSql.getAdditionalParameter(propertyName);
                    sql = sql.replaceFirst("\\?", getParameterValue(obj));
                }
            }
        }
        return sql;
    }

    private String getParameterValue(Object obj) {
        String value = null;
        if (obj instanceof String) {
            value = "'" + obj.toString() + "'";
        } else if (obj instanceof Date) {
            DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, Locale.CHINA);
            value = "'" + formatter.format(obj) + "'";
        } else {
            if (obj != null) {
                value = obj.toString();
            } else {
                value = "";
            }

        }
        return value;
    }

    /**创建返回远程SQL的本地SQL**/
    public String createNewSql(String sql){
        sql = "\""+sql.replaceAll("\"","\\\\\"")+"\"";
        return "select " + sql + "as remoteSql;";
    }

    /**
     * 将新的sql,设置到invocation对象中,让旧的SQL失效
     */
    private void resetSql2Invocation(StatementHandler statementHandler, MappedStatement mappedStatement, String sql) throws SQLException {
        BoundSql boundSql = statementHandler.getBoundSql();
        Configuration configuration = mappedStatement.getConfiguration();
        BoundSql newBoundSql = new BoundSql(configuration,sql, Lists.newArrayList(),boundSql.getParameterObject());
        MappedStatement newStatement = createNewStatement(mappedStatement, new BoundSqlSqlSource(newBoundSql));
        MetaObject metaObject = MetaObject.forObject(statementHandler, SystemMetaObject.DEFAULT_OBJECT_FACTORY, SystemMetaObject.DEFAULT_OBJECT_WRAPPER_FACTORY,new DefaultReflectorFactory());
        metaObject.setValue("delegate.mappedStatement",newStatement);
    }

    /**创建新的mysql查询对象**/
    private MappedStatement createNewStatement(MappedStatement ms, SqlSource newSqlSource) {
        MappedStatement.Builder builder =
                new MappedStatement.Builder(ms.getConfiguration(), ms.getId(), newSqlSource, ms.getSqlCommandType());
        builder.resource(ms.getResource());
        builder.fetchSize(ms.getFetchSize());
        builder.statementType(ms.getStatementType());
        builder.keyGenerator(ms.getKeyGenerator());
        if (ms.getKeyProperties() != null && ms.getKeyProperties().length != 0) {
            StringBuilder keyProperties = new StringBuilder();
            for (String keyProperty : ms.getKeyProperties()) {
                keyProperties.append(keyProperty).append(",");
            }
            keyProperties.delete(keyProperties.length() - 1, keyProperties.length());
            builder.keyProperty(keyProperties.toString());
        }
        builder.timeout(ms.getTimeout());
        builder.parameterMap(ms.getParameterMap());
        builder.resultMaps(ms.getResultMaps());
        builder.resultSetType(ms.getResultSetType());
        builder.cache(ms.getCache());
        builder.flushCacheRequired(ms.isFlushCacheRequired());
        builder.useCache(ms.isUseCache());

        return builder.build();
    }

    static class BoundSqlSqlSource implements SqlSource {
        private BoundSql boundSql;
        BoundSqlSqlSource(BoundSql boundSql) {
            this.boundSql = boundSql;
        }
        @Override
        public BoundSql getBoundSql(Object parameterObject) {
            return boundSql;
        }
    }

}

