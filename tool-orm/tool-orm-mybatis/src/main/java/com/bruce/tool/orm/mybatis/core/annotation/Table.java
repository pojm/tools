package com.bruce.tool.orm.mybatis.core.annotation;

import java.lang.annotation.*;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 上午11:40 2018/4/10
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
@Documented
public @interface Table {
    String value();
}