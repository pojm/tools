package com.bruce.tool.orm.mybatis.generator.plugin;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.*;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 注解方式:表-->实体类反向生成工具
 * 功能 : 
 * 1.增加字段注释
 * 2.增加字段注解
 * 3.增加表字段枚举类
 * 4.增加序列化标识和接口
 * @author : Bruce(刘正航) 10:22 2019-05-11
 */
public class AnnotationDomainPlugin extends PluginAdapter {

    private boolean suppressAllComments = false;

    /**包引入**/
    private static final String IMPORT_ID = "com.bruce.tool.orm.mybatis.core.annotation.Id";
    private final List<String> imports = Lists.newArrayList(
            "lombok.Data",
            "com.bruce.tool.orm.mybatis.core.annotation.Table",
            "com.bruce.tool.orm.mybatis.core.annotation.Column"
    );

    /**注释**/
    private static final String ANNOTATION_ID = "@Id";
    private static final String ANNOTATION_COLUMN = "@Column";
    private static final String ANNOTATION_TABLE = "@Table";
    private final List<String> lombokAnnotations = Lists.newArrayList("@Data");

    private static final String field = "field";
    private static final String column = "column";

    @Override
    public boolean validate(final List<String> list) {
        String suppress =  this.context.getProperty("suppressAllComments");
        if((Boolean.TRUE.toString().equals(suppress) || Boolean.FALSE.toString().equals(suppress))){
            suppressAllComments = Boolean.valueOf(suppress);
        }
        return true;
    }
    /**生成实体**/
    @Override
    public boolean modelBaseRecordClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        // 清空所有方法(getter/setter方法)
        topLevelClass.getMethods().clear();
        for (String annotation : lombokAnnotations) {
            topLevelClass.addAnnotation(annotation);
        }
        for (String needImport : imports) {
            topLevelClass.addImportedType(needImport);
        }

        topLevelClass.addAnnotation(ANNOTATION_TABLE +"(\""+introspectedTable.getFullyQualifiedTableNameAtRuntime()+"\")");

        if( introspectedTable.hasPrimaryKeyColumns() ){ topLevelClass.addImportedType(IMPORT_ID); }

        addJavaDocLine(topLevelClass, introspectedTable.getRemarks());

        // 添加字段枚举类
        addFieldsEnum(topLevelClass ,introspectedTable);
        return true;
    }

    /**添加字段枚举类**/
    private void addFieldsEnum(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        InnerEnum innerEnum = new InnerEnum(new FullyQualifiedJavaType(Field.class.getSimpleName()+"s"));
        innerEnum.setVisibility(JavaVisibility.PUBLIC);

        for (IntrospectedColumn introspectedColumn : introspectedTable.getAllColumns()) {
            String enumKey = this.humpToUnderline(introspectedColumn.getJavaProperty()).toUpperCase();
            String fieldName = introspectedColumn.getJavaProperty();
            String columnName = introspectedColumn.getActualColumnName();
            if(!suppressAllComments){ innerEnum.addEnumConstant("//"+introspectedColumn.getRemarks()); }
            innerEnum.addEnumConstant(enumKey+"(\""+fieldName+"\",\""+columnName+"\")");
        }

        addEnumField(innerEnum,field);
        addEnumField(innerEnum,column);
        addEnumConstructor(innerEnum);
        addEnumGetMethod(innerEnum,field);
        addEnumGetMethod(innerEnum,column);

        topLevelClass.addInnerEnum(innerEnum);
    }

    /**添加字段**/
    private void addEnumField(final InnerEnum innerEnum, final String field) {
        Field field1 = new Field(field,new FullyQualifiedJavaType("String"));
        field1.setVisibility(JavaVisibility.PRIVATE);
        innerEnum.addField(field1);
    }

    /**添加get方法**/
    private void addEnumGetMethod(final InnerEnum innerEnum, String field) {
        Method method = new Method(field);
        method.setVisibility(JavaVisibility.PUBLIC);
        method.addBodyLine("return this."+field+";");
        method.setReturnType(new FullyQualifiedJavaType("String"));
        innerEnum.addMethod(method);
    }

    /**添加构造函数**/
    private Method addEnumConstructor(InnerEnum innerEnum) {
        Method method = new Method(Field.class.getSimpleName()+"s");
        method.setConstructor(true);
        method.addParameter(new Parameter(new FullyQualifiedJavaType("String"),field));
        method.addParameter(new Parameter(new FullyQualifiedJavaType("String"),column));
        method.addBodyLine("this."+field+" = "+field+";");
        method.addBodyLine("this."+column+" = "+column+";");
        innerEnum.addMethod(method);
        return method;
    }

    /**生成字段**/
    @Override
    public boolean modelFieldGenerated(final Field field, final TopLevelClass topLevelClass, final IntrospectedColumn introspectedColumn, final IntrospectedTable introspectedTable, final ModelClassType modelClassType) {
        addJavaDocLine(field, StringUtils.replace(introspectedColumn.getRemarks(),"\n",""));
        List<IntrospectedColumn> primaryKeyColumns = introspectedTable.getPrimaryKeyColumns();
        if(null != primaryKeyColumns && primaryKeyColumns.size() == 1){
            String primaryKey = primaryKeyColumns.get(0).getActualColumnName();
            if(introspectedColumn.getActualColumnName().equals(primaryKey)){
                introspectedColumn.setIdentity(true);
            }
        }
        // 是主键
        if( introspectedColumn.isIdentity() ){
            field.addAnnotation(ANNOTATION_ID +"(\""+introspectedColumn.getActualColumnName()+"\")");
            return true;
        }
        field.addAnnotation(ANNOTATION_COLUMN +"(\""+introspectedColumn.getActualColumnName()+"\")");
        // 不是主键
        return true;
    }

    /**添加表注释**/
    private void addJavaDocLine(final TopLevelClass topLevelClass, final String remarks) {
        if(suppressAllComments){ return; }
        topLevelClass.getJavaDocLines().clear();
        if( StringUtils.isNotBlank(remarks) ){
            topLevelClass.addJavaDocLine("/**"+remarks+"**/");
        }
    }

    /**添加行注释**/
    private void addJavaDocLine(final Field field, final String remarks) {
        if(suppressAllComments){ return; }
        field.getJavaDocLines().clear();
        if( StringUtils.isNotBlank(remarks) ){
            field.addJavaDocLine("/**"+remarks+"**/");
        }
    }

    public String humpToUnderline(String humpString) {
        if (humpString == null || humpString.isEmpty()) {
            return "";
        }
        String prefix = "";
        if (humpString.charAt(0) == '_') {
            humpString = humpString.substring(1);
            prefix = "_";
        }
        String regexStr = "[A-Z0-9]";
        Matcher matcher = Pattern.compile(regexStr).matcher(humpString);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            String g = matcher.group();
            matcher.appendReplacement(sb, "_" + g.toLowerCase());
        }
        matcher.appendTail(sb);
        if (sb.charAt(0) == '_') {
            sb.delete(0, 1);
        }
        sb.insert(0, prefix);
        return sb.toString();
    }
}
