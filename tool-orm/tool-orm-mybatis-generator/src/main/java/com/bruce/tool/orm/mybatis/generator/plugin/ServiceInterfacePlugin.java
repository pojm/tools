package com.bruce.tool.orm.mybatis.generator.plugin;

import com.bruce.tool.orm.mybatis.generator.creater.core.BaseCreater;
import org.codehaus.plexus.util.StringUtils;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.JavaVisibility;
import org.mybatis.generator.api.dom.java.TopLevelClass;

import java.util.List;

/**
 * 功能 :
 * 1.生成service层业务接口,提供统一公共的方法
 * @author : Bruce(刘正航) 6:50 下午 2020/1/4
 */
public class ServiceInterfacePlugin extends BaseCreater {

    private String filePath;
    private String servicePackage;

    @Override
    public boolean validate(List<String> warnings) {
        filePath = super.getProperties().getProperty("service.path");
        servicePackage = super.getProperties().getProperty("service.package");
        return true;
    }

    @Override
    public boolean modelBaseRecordClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
        if(StringUtils.isBlank(filePath) || StringUtils.isBlank(servicePackage)){
            return true;
        }
        FullyQualifiedJavaType domainType = topLevelClass.getType();
        String serviceName = domainType.getShortName() + "Service";
        Interface dubboService = new Interface(new FullyQualifiedJavaType(servicePackage+"."+serviceName));
        dubboService.setVisibility(JavaVisibility.PUBLIC);

        String baseService = "com.bruce.tool.orm.mybatis.core.service.BaseService";
        dubboService.addImportedType(new FullyQualifiedJavaType(baseService));
        dubboService.addImportedType(domainType);
        dubboService.addSuperInterface(new FullyQualifiedJavaType("BaseService<"+domainType.getShortName()+">"));
        createClassFile(filePath, dubboService);
        return true;
    }


}
