package com.bruce.tool.orm.mybatis.generator;

import com.bruce.tool.common.util.string.JsonUtils;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.ShellCallback;
import org.mybatis.generator.api.ShellRunner;
import org.mybatis.generator.api.dom.DefaultJavaFormatter;
import org.mybatis.generator.api.dom.java.*;
import org.mybatis.generator.config.MergeConstants;
import org.mybatis.generator.exception.ShellException;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.io.*;
import java.util.List;

import static org.mybatis.generator.internal.util.messages.Messages.getString;

/**
 * 功能 : 
 *
 * @author : Bruce(刘正航) 09:03 2019-05-11
 */
public class PluginsTest {
    @Test
    public void run(){
        String config = this.getClass().getClassLoader().getResource("generatorConfig.xml").getFile();
        String[] arg = { "-configfile", config, "-overwrite" };
        ShellRunner.main(arg);
    }

    private List<String> warnings = Lists.newArrayList();
    private ShellCallback shellCallback = new DefaultShellCallback(true);

    @Test
    public void createClassFile() throws IOException {
        String filePath = "src/test/java";
        String packages = "com.bruce.tool.orm.mybatis.generator";
        String baseService = "com.bruce.tool.orm.mybatis.core.service.BaseService";
        Interface dubboService = new Interface(new FullyQualifiedJavaType(packages+".service.AdminMenuService"));
        dubboService.setVisibility(JavaVisibility.PUBLIC);
        dubboService.addImportedType(new FullyQualifiedJavaType(baseService));
        dubboService.addImportedType(new FullyQualifiedJavaType(packages+".domain.AdminMenu"));
        dubboService.addSuperInterface(new FullyQualifiedJavaType("BaseService<AdminMenu>"));
        createClassFile(filePath, dubboService);

        TopLevelClass dubboServiceImpl = new TopLevelClass(new FullyQualifiedJavaType(packages+".service.AdminMenuServiceImpl"));
        dubboServiceImpl.setVisibility(JavaVisibility.PUBLIC);
        dubboServiceImpl.setSuperClass(new FullyQualifiedJavaType("com.bruce.tool.orm.mybatis.core.service.AbstractBaseService<AdminMenu>"));
        dubboServiceImpl.addSuperInterface(new FullyQualifiedJavaType("AdminMenuService"));
        dubboServiceImpl.addImportedType("com.bruce.tool.orm.mybatis.core.mapper.BaseMapper");
        dubboServiceImpl.addImportedType("com.bruce.tool.orm.mybatis.core.service.AbstractBaseService");
        dubboServiceImpl.addImportedType("com.bruce.tool.orm.mybatis.generator.domain.AdminMenu");
        dubboServiceImpl.addImportedType("com.bruce.tool.orm.mybatis.generator.mapper.AdminMenuMapper");
        dubboServiceImpl.addImportedType("org.springframework.beans.factory.annotation.Autowired");
        dubboServiceImpl.addImportedType("org.springframework.stereotype.Component");
        dubboServiceImpl.addJavaDocLine("@Component");
        Field field = new Field("adminMenuMapper",new FullyQualifiedJavaType("AdminMenuMapper"));
        field.setVisibility(JavaVisibility.PRIVATE);
        field.addJavaDocLine("@Autowired");
        dubboServiceImpl.addField(field);

        Method clazz = new Method("clazz");
        clazz.setVisibility(JavaVisibility.PROTECTED);
        clazz.setReturnType(new FullyQualifiedJavaType("Class<AdminMenu>"));
        clazz.addBodyLine("return AdminMenu.class;");
        clazz.addJavaDocLine("@Override");
        dubboServiceImpl.addMethod(clazz);

        Method baseMapper = new Method("baseMapper");
        baseMapper.setVisibility(JavaVisibility.PROTECTED);
        baseMapper.setReturnType(new FullyQualifiedJavaType("BaseMapper<AdminMenu>"));
        baseMapper.addBodyLine("return adminMenuMapper;");
        baseMapper.addJavaDocLine("@Override");
        dubboServiceImpl.addMethod(baseMapper);
        createClassFile(filePath, dubboServiceImpl);
        System.out.println(JsonUtils.objToStr(warnings));
    }

    /**创建类,并保存到磁盘**/
    private void createClassFile(String filePath, CompilationUnit topLevelClass) throws IOException {
        GeneratedJavaFile gjf = new GeneratedJavaFile(topLevelClass,filePath,new DefaultJavaFormatter());
        try {
            String source;
            File directory = shellCallback.getDirectory(gjf.getTargetProject(), gjf.getTargetPackage());
            File targetFile = new File(directory, gjf.getFileName());
            if (targetFile.exists()) {
                if (shellCallback.isMergeSupported()) {
                    source = shellCallback.mergeJavaFile(gjf
                                    .getFormattedContent(), targetFile,
                            MergeConstants.getOldElementTags(),
                            gjf.getFileEncoding());
                } else if (shellCallback.isOverwriteEnabled()) {
                    source = gjf.getFormattedContent();
                    warnings.add(getString("Warning.11", //$NON-NLS-1$
                            targetFile.getAbsolutePath()));
                } else {
                    source = gjf.getFormattedContent();
                    targetFile = getUniqueFileName(directory, gjf
                            .getFileName());
                    warnings.add(getString(
                            "Warning.2", targetFile.getAbsolutePath())); //$NON-NLS-1$
                }
            } else {
                source = gjf.getFormattedContent();
            }

            writeFile(targetFile, source, gjf.getFileEncoding());
        } catch (ShellException e) {
            warnings.add(e.getMessage());
        }
    }

    private void writeFile(File file, String content, String fileEncoding) throws IOException {
        FileOutputStream fos = new FileOutputStream(file, false);
        OutputStreamWriter osw;
        if (fileEncoding == null) {
            osw = new OutputStreamWriter(fos);
        } else {
            osw = new OutputStreamWriter(fos, fileEncoding);
        }

        BufferedWriter bw = new BufferedWriter(osw);
        bw.write(content);
        bw.close();
    }

    private File getUniqueFileName(File directory, String fileName) {
        File answer = null;

        // try up to 1000 times to generate a unique file name
        StringBuilder sb = new StringBuilder();
        for (int i = 1; i < 1000; i++) {
            sb.setLength(0);
            sb.append(fileName);
            sb.append('.');
            sb.append(i);

            File testFile = new File(directory, sb.toString());
            if (!testFile.exists()) {
                answer = testFile;
                break;
            }
        }

        if (answer == null) {
            throw new RuntimeException(getString(
                    "RuntimeError.3", directory.getAbsolutePath())); //$NON-NLS-1$
        }

        return answer;
    }
}
