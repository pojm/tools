package com.bruce.tool.orm.mybatis.generator.mapper;

import com.bruce.tool.orm.mybatis.core.mapper.BaseMapper;
import com.bruce.tool.orm.mybatis.generator.domain.AdminMenu;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface AdminMenuMapper extends BaseMapper<AdminMenu> {
}