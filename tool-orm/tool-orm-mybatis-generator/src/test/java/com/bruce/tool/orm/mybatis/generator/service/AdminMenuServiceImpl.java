package com.bruce.tool.orm.mybatis.generator.service;

import com.bruce.tool.orm.mybatis.core.mapper.BaseMapper;
import com.bruce.tool.orm.mybatis.core.service.AbstractBaseService;
import com.bruce.tool.orm.mybatis.generator.domain.AdminMenu;
import com.bruce.tool.orm.mybatis.generator.mapper.AdminMenuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AdminMenuServiceImpl extends AbstractBaseService<AdminMenu> implements AdminMenuService {
    @Autowired
    private AdminMenuMapper adminMenuMapper;

    @Override
    protected Class<AdminMenu> clazz() {
        return AdminMenu.class;
    }

    @Override
    protected BaseMapper<AdminMenu> baseMapper() {
        return adminMenuMapper;
    }
}