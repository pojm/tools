package com.bruce.tool.orm.mybatis.generator.service;

import com.bruce.tool.orm.mybatis.core.service.BaseService;
import com.bruce.tool.orm.mybatis.generator.domain.AdminMenu;

public interface AdminMenuService extends BaseService<AdminMenu> {
}