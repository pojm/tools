package com.bruce.tool.orm.mybatis.generator.service;

import com.bruce.tool.orm.mybatis.core.service.BaseService;
import com.bruce.tool.orm.mybatis.generator.domain.AdminUser;

public interface AdminUserService extends BaseService<AdminUser> {
}