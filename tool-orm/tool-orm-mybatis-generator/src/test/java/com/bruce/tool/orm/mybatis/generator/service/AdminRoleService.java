package com.bruce.tool.orm.mybatis.generator.service;

import com.bruce.tool.orm.mybatis.core.service.BaseService;
import com.bruce.tool.orm.mybatis.generator.domain.AdminRole;

public interface AdminRoleService extends BaseService<AdminRole> {
}