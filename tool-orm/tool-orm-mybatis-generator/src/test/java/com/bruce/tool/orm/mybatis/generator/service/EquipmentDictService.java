package com.bruce.tool.orm.mybatis.generator.service;

import com.bruce.tool.orm.mybatis.core.service.BaseService;
import com.bruce.tool.orm.mybatis.generator.domain.EquipmentDict;

public interface EquipmentDictService extends BaseService<EquipmentDict> {
}