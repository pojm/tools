package com.bruce.tool.orm.mybatis.generator.service;

import com.bruce.tool.orm.mybatis.core.service.BaseService;
import com.bruce.tool.orm.mybatis.generator.domain.Region;

public interface RegionService extends BaseService<Region> {
}