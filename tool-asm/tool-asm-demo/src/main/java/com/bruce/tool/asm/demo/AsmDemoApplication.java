package com.bruce.tool.asm.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 5:09 PM 2018/12/18
 */
@SpringBootApplication
public class AsmDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(AsmDemoApplication.class, args);
    }

}
