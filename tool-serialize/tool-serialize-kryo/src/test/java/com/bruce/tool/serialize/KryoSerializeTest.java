package com.bruce.tool.serialize;

import com.alibaba.fastjson.JSON;
import com.bruce.tool.common.util.string.JsonUtils;
import com.bruce.tool.serialize.kryo.KryoResolver;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import org.assertj.core.util.Lists;

import java.util.List;
import java.util.Map;

/**
 * 功能 :
 * @author : Bruce(刘正航) 5:44 下午 2019/11/26
 */
public class KryoSerializeTest {

    public static void main(String[] args){
        List<Object> datas = createData();
        useFastJson(datas);
        useJackson(datas);
        useGson(datas);
        useKryo(datas);
    }

    private static List<Object> createData() {
        List<Object> datas = Lists.newArrayList();
        for (int i = 0;i< 1000000;i++){
            Map<String,Integer> data = Maps.newHashMap();
            data.put("zhang0", 1);
            data.put("zhang1", 3);
            Simple simple = new Simple("name"+i,i,data);
            datas.add(simple);
        }
        return datas;
    }

    private static void useFastJson(List<Object> datas) {
        long start =  System.currentTimeMillis();
        for (Object data : datas){
            JSON.toJSONString(data);
        }
        System.out.println("fastjson耗时:"+(System.currentTimeMillis()-start));
    }

    private static void useJackson(List<Object> datas) {
        long start =  System.currentTimeMillis();
        for (Object data : datas){
            JsonUtils.objToStr(data);
        }
        System.out.println("jackson耗时:"+(System.currentTimeMillis()-start));
    }

    private static void useGson(List<Object> datas) {
        long start;
        start =  System.currentTimeMillis();
        Gson gson = new Gson();
        for (Object data : datas){
            gson.toJson(data);
        }
        System.out.println("gson耗时:"+(System.currentTimeMillis()-start));
    }

    private static void useKryo(List<Object> datas) {
        long start;
        start =  System.currentTimeMillis();
        KryoResolver resolver = new KryoResolver();
        for (Object data : datas){
            resolver.serialize(data);
        }
        System.out.println("kryo耗时:"+(System.currentTimeMillis()-start));
    }

}
