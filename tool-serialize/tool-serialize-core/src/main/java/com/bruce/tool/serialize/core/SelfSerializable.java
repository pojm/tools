package com.bruce.tool.serialize.core;

/**
 * 功能 :
 * 序列化,反序列化
 * @author : Bruce(刘正航) 5:36 下午 2019/11/26
 */
public interface SelfSerializable {
    /**序列化**/
    byte[] serialize(Object data);
    /**反序列化**/
    <T> T deserialize(byte[] bytes,Class<T> clazzs);
}
