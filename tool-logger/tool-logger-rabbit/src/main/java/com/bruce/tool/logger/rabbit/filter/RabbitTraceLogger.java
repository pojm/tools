package com.bruce.tool.logger.rabbit.filter;

import com.bruce.tool.common.util.string.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

/**
 * 功能 :
 * @author : Bruce(刘正航) 10:17 下午 2019/12/3
 */
@Slf4j
@Component
public class RabbitTraceLogger {

    @RabbitListener(bindings = {@QueueBinding(
            value = @Queue(value="${docker.message.routekey}", durable = "false"),
            exchange = @Exchange(value="${docker.message.exchange}", durable = "false", type= ExchangeTypes.FANOUT),
            key = "${docker.message.routekey}"
    )})
    public void log(Message message){
        log.info(JsonUtils.objToStr(message));
        String queue = message.getMessageProperties().getConsumerQueue();
        String routeKey = message.getMessageProperties().getReceivedRoutingKey();
        String messageId = message.getMessageProperties().getMessageId();
        String exchange = message.getMessageProperties().getReceivedExchange();
        String messageInfo = new String(message.getBody(), StandardCharsets.UTF_8);
    }
}
