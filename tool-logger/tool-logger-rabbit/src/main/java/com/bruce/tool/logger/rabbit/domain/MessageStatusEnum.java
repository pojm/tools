package com.bruce.tool.logger.rabbit.domain;

/**
 * 功能 :
 * @author : Bruce(刘正航) 10:45 上午 2019/11/1
 */
public enum MessageStatusEnum {
    // 消息状态
    SEND_FAIL(0,"发送失败"),
    SEND_SUCCESS(1,"发送成功"),
    RECEIVED(2,"已接收"),
    CONSUMPTION_SUCCESS(3,"消费成功"),
    CONSUMPTION_FAIL(4,"消费失败"),
    CONSUMPTION_FAIL_TIMES(5,"补偿失败");

    private Integer code;
    private String desc;

    MessageStatusEnum(Integer code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public static MessageStatusEnum getByCode(Integer code){
        if(null == code){
            return null;
        }
        for (MessageStatusEnum statusEnum:values()){
            if(statusEnum.getCode().equals(code)){
                return statusEnum;
            }
        }
        return null;
    }
}
