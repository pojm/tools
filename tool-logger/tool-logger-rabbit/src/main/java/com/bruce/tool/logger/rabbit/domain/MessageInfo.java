package com.bruce.tool.logger.rabbit.domain;

import lombok.Data;

/**
 * 功能 :
 * @author : Bruce(刘正航) 10:13 上午 2019/11/1
 */
@Data
public class MessageInfo {
    /**消息ID**/
    private String messageId;
    /**消息内容**/
    private String messageContent;
    private String exchange;
    private String consumeQueue;
    private String routeKey;
    /**消费状态**/
    private Integer status;
    /**消费方IP**/
    private String sendIp;
    /**消费方IP**/
    private String consumeIp;
    /**发送方应用名称**/
    private String sendApplicationName;
    /**消费方应用名称**/
    private String consumeApplicationName;
    /**创建时间**/
    private String createTime;
    /**更新时间**/
    private String updateTime;
    /**错误信息**/
    private String errorMsg;
}
