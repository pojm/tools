package com.bruce.tool.logger.rabbit;

import com.google.common.collect.Maps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Map;

/**
 * 功能 :
 * @author : Bruce(刘正航) 10:29 下午 2019/12/4
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = RabbitLoggerApplication.class)
public class SendMessageTest{

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Test
    public void testSend(){
        Map<String,Object> message = Maps.newConcurrentMap();
        message.put("name","测试");
        message.put("age",18);
        amqpTemplate.convertAndSend("docker-message","docker",message);
    }
}
