package com.bruce.tool.security.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 *
 * 功能 :
 * @author : Bruce(刘正航) 上午12:49 2018/11/16
 */
@SpringBootApplication(scanBasePackages = {"com.bruce.tool.security.spring"},exclude={DataSourceAutoConfiguration.class})
public class SpringSecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringSecurityApplication.class, args);
    }
}
