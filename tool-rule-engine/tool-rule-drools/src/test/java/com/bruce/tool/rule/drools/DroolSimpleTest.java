package com.bruce.tool.rule.drools;

import com.google.common.collect.Maps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.utils.KieHelper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

/**
 * 功能 :
 * @author : Bruce(刘正航) 11:44 上午 2019/11/26
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DroolsApplication.class})
public class DroolSimpleTest {

    @Test
    public void dynamicRuleTest(){
        String myRule = "import java.util.Map\n" +
                "\n" +
                "dialect  \"mvel\"\n" +
                "\n" +
                "rule \"age\"\n" +
                "    when\n" +
                "        $domain:Map(this['age']<16 || this['age']>50)\n" +
                "    then\n" +
                "        $domain.set('result','这个人的年龄不符合要求！')" +
//                "        System.out.println(\"这个人的年龄不符合要求！（基于动态加载）\");\n" +
                "end\n";
        KieHelper helper = new KieHelper();
        helper.addContent(myRule, ResourceType.DRL);
        KieSession ksession = helper.build().newKieSession();
        Map<String,Object> domain = Maps.newHashMap();
        domain.put("age",12);
        domain.put("name","测试");
        ksession.insert(domain);
        ksession.fireAllRules();
        ksession.dispose();
        System.out.println(domain.get("result"));
    }
}
