package com.bruce.tool.mq.aliyun.util;

import com.aliyun.openservices.ons.api.*;
import com.bruce.tool.mq.aliyun.config.AliMQConfig;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.Properties;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 14:26 2019-02-14
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AliMQUtils {

    /**初始化发送者**/
    public static Producer initSender(AliMQConfig config){
        Properties properties = new Properties();
        properties.setProperty(PropertyKeyConst.GROUP_ID, config.getGroup());
        // AccessKey 阿里云身份验证，在阿里云服务器管理控制台创建
        properties.put(PropertyKeyConst.AccessKey,config.getAccess());
        // SecretKey 阿里云身份验证，在阿里云服务器管理控制台创建
        properties.put(PropertyKeyConst.SecretKey, config.getSecret());
        //设置发送超时时间，单位毫秒
        properties.setProperty(PropertyKeyConst.SendMsgTimeoutMillis, "3000");
        // 设置 TCP 接入域名，到控制台的实例基本信息中查看
        properties.put(PropertyKeyConst.NAMESRV_ADDR, config.getUrl());

        Producer producer = ONSFactory.createProducer(properties);
        // 在发送消息前，必须调用 start 方法来启动 Producer，只需调用一次即可
        producer.start();
        return producer;
    }

    /**初始化接收者**/
    public static Consumer initReceiver(AliMQConfig config){
        Properties properties = new Properties();
        // 您在控制台创建的 Group ID
        properties.put(PropertyKeyConst.GROUP_ID, config.getGroup());
        // AccessKey 阿里云身份验证，在阿里云服务器管理控制台创建
        properties.put(PropertyKeyConst.AccessKey, config.getAccess());
        // SecretKey 阿里云身份验证，在阿里云服务器管理控制台创建
        properties.put(PropertyKeyConst.SecretKey, config.getSecret());
        // 设置 TCP 接入域名，进入控制台的实例管理页面的“获取接入点信息”区域查看
        properties.put(PropertyKeyConst.NAMESRV_ADDR, config.getUrl());
        // 集群订阅方式 (默认) // 广播订阅方式
        if(StringUtils.isNotBlank(config.getModel()) && PropertyValueConst.BROADCASTING.equals(config.getModel()) ){
            properties.put(PropertyKeyConst.MessageModel, PropertyValueConst.BROADCASTING);
        }

        return ONSFactory.createConsumer(properties);
    }
}
