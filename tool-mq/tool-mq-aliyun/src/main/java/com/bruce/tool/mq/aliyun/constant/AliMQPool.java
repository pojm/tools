package com.bruce.tool.mq.aliyun.constant;

import com.aliyun.openservices.ons.api.Consumer;
import com.aliyun.openservices.ons.api.Producer;
import com.bruce.tool.mq.aliyun.config.AliMQConfig;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 14:10 2019-02-14
 */
@NoArgsConstructor(staticName = "build")
public class AliMQPool {

    @Getter
    private Map<String, AliMQConfig> configs = Maps.newHashMap();
    @Getter
    private Map<String, Producer> senders = Maps.newHashMap();
    @Getter
    private Map<String, Consumer> receivers = Maps.newHashMap();

    public void config(String code,AliMQConfig config){
        this.configs.put(code,config);
    }

    public void sender(String code,Producer producer){
        this.senders.put(code,producer);
    }

    public void receiver(String code,Consumer consumer){
        this.receivers.put(code,consumer);
    }
}
