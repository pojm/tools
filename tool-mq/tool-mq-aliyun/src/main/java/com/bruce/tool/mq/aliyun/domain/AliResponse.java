package com.bruce.tool.mq.aliyun.domain;

import lombok.Builder;
import lombok.Getter;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 16:14 2019-02-14
 */
@Builder
public class AliResponse {
    @Getter
    private String topic;
    @Getter
    private String messageId;
    @Getter
    private byte[] body;
}
