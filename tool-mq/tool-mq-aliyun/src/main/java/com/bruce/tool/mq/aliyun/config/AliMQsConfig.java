package com.bruce.tool.mq.aliyun.config;

import com.bruce.tool.mq.aliyun.constant.AliMQPool;
import com.bruce.tool.mq.aliyun.util.AliMQUtils;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 11:26 2019-02-14
 */
@Data
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "tool.mqs.aliyun")
public class AliMQsConfig {

    private List<AliMQConfig> configs = new ArrayList<>();

    @Bean
    public AliMQPool init(){
        return initConfig();
    }

    /**初始化配置**/
    private AliMQPool initConfig(){
        AliMQPool pool = AliMQPool.build();
        for (AliMQConfig config : configs) {
            String code = config.getCode();
            pool.config(code,config);
            pool.sender(code, AliMQUtils.initSender(config));
            pool.receiver(code, AliMQUtils.initReceiver(config));
        }
        return pool;
    }

}
