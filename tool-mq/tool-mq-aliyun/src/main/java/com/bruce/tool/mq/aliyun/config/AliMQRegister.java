package com.bruce.tool.mq.aliyun.config;

import org.springframework.context.annotation.ComponentScan;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 11:27 2019-02-14
 */
@ComponentScan(value = {"com.bruce.tool.mq.aliyun"})
public class AliMQRegister {
}
