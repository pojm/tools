package com.bruce.tool.mq.aliyun.handler;

import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.Consumer;
import com.bruce.tool.mq.aliyun.domain.AliResponse;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 16:15 2019-02-14
 */
public interface AliMQHandler {

    default void handle(Consumer consumer){}

    default Action handle(AliResponse response){return null;}

}
