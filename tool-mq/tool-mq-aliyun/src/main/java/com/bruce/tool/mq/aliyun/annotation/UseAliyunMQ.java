package com.bruce.tool.mq.aliyun.annotation;

import com.bruce.tool.mq.aliyun.config.AliMQRegister;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 11:24 2019-02-14
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(AliMQRegister.class)
public @interface UseAliyunMQ {
}
