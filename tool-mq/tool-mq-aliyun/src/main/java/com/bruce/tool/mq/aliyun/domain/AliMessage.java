package com.bruce.tool.mq.aliyun.domain;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 11:04 2019-02-14
 */
@Data
@Builder
public class AliMessage {
    /**消息唯一标识key**/
    private String key;
    /**业务唯一标识serviceCode**/
    @NotNull(message = "业务唯一标识为空,终止消息发送!")
    private String code;
    /**消息体**/
    @NotNull(message = "消息体为空,终止消息发送!")
    private byte[] body;
}
