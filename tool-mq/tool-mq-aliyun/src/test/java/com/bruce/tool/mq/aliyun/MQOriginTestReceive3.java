package com.bruce.tool.mq.aliyun;

import com.aliyun.openservices.ons.api.Action;
import com.aliyun.openservices.ons.api.Consumer;
import com.aliyun.openservices.ons.api.ONSFactory;
import com.aliyun.openservices.ons.api.PropertyKeyConst;

import java.util.Properties;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 10:50 2019-02-14
 */
public class MQOriginTestReceive3 {

    public static void main(String[] args) {
        Properties properties = new Properties();
        // 您在控制台创建的 Group ID
        properties.put(PropertyKeyConst.GROUP_ID, "GID_COIN_TRADE_CHANCE");
        // AccessKey 阿里云身份验证，在阿里云服务器管理控制台创建
        properties.put(PropertyKeyConst.AccessKey, "LTAIPxCarEmQubAT");
        // SecretKey 阿里云身份验证，在阿里云服务器管理控制台创建
        properties.put(PropertyKeyConst.SecretKey, "hFkBPZWmokHeKFLemh1m7t3mF1TXfr");
        // 设置 TCP 接入域名，进入控制台的实例管理页面的“获取接入点信息”区域查看
        properties.put(PropertyKeyConst.NAMESRV_ADDR, "http://MQ_INST_1580272050230807_BaWbqJ2c.mq-internet-access.mq-internet.aliyuncs.com:80");
        // 集群订阅方式 (默认)
        // properties.put(PropertyKeyConst.MessageModel, PropertyValueConst.CLUSTERING);
        // 广播订阅方式
        // properties.put(PropertyKeyConst.MessageModel, PropertyValueConst.BROADCASTING);
        Consumer consumer = ONSFactory.createConsumer(properties);
        //订阅全部 Tag
        consumer.subscribe("hello", "hello", (message, context) -> {
            System.out.println("Receive: " + message);
            return Action.CommitMessage;
        });
        consumer.start();
        System.out.println("Consumer Started");
    }
}
