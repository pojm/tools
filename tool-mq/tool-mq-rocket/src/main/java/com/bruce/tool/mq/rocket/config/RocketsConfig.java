package com.bruce.tool.mq.rocket.config;

import com.bruce.tool.mq.rocket.constant.RocketPool;
import com.bruce.tool.mq.rocket.util.RocketUtils;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 11:26 2019-02-14
 */
@Data
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "tool.mqs.rocket")
public class RocketsConfig {

    private List<RocketConfig> configs = new ArrayList<>();

    @Bean
    public RocketPool init(){
        return initConfig();
    }

    /**初始化配置**/
    private RocketPool initConfig(){
        RocketPool pool = RocketPool.build();
        for (RocketConfig config : configs) {
            String code = config.getCode();
            pool.config(code,config);
            pool.sender(code, RocketUtils.initSender(config));
            pool.receiver(code, RocketUtils.initReceiver(config));
        }
        return pool;
    }

}
