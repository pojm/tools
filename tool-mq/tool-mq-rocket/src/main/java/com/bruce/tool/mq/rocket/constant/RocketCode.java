package com.bruce.tool.mq.rocket.constant;

import lombok.Getter;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 09:57 2019-02-21
 */
public enum RocketCode {

    MQERROR_CONFIG_NULL("MQ001","消息配置为空"),
    MQERROR_SENDER_NULL("MQ002","消息发送者为空"),
    MQERROR_SEND_EXCEPTION("MQ003","消息发送失败"),
    MQERROR_RECEIVE_NULL("MQ004","消息接收者为空"),
    MQERROR_RECEIVE_EXCEPTION("MQ005","消息接收失败"),

    ;
    @Getter
    private String code;
    @Getter
    private String message;

    RocketCode(String code, String message) {
        this.code = code;
        this.message = message;
    }
}
