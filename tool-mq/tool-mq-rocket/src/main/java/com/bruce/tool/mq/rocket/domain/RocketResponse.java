package com.bruce.tool.mq.rocket.domain;

import lombok.Builder;
import lombok.Getter;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 10:03 2019-02-21
 */
@Builder
public class RocketResponse {
    @Getter
    private String topic;
    @Getter
    private String messageId;
    @Getter
    private byte[] body;
    /**消息创建时间**/
    private long timestamp;
}
