package com.bruce.tool.mq.rocket.handler;

import com.bruce.tool.mq.rocket.domain.RocketResponse;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 16:15 2019-02-14
 */
public interface RocketHandler {

    default void handle(DefaultMQPushConsumer consumer){}

    default ConsumeConcurrentlyStatus handle(RocketResponse response){return null;}

}
