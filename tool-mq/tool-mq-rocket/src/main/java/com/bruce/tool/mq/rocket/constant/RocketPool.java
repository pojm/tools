package com.bruce.tool.mq.rocket.constant;

import com.bruce.tool.mq.rocket.config.RocketConfig;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.producer.DefaultMQProducer;

import java.util.Map;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 14:10 2019-02-14
 */
@NoArgsConstructor(staticName = "build")
public class RocketPool {

    @Getter
    private Map<String, RocketConfig> configs = Maps.newHashMap();
    @Getter
    private Map<String, DefaultMQProducer> senders = Maps.newHashMap();
    @Getter
    private Map<String, DefaultMQPushConsumer> receivers = Maps.newHashMap();

    public void config(String code,RocketConfig config){
        this.configs.put(code,config);
    }

    public void sender(String code,DefaultMQProducer producer){
        this.senders.put(code,producer);
    }

    public void receiver(String code,DefaultMQPushConsumer consumer){
        this.receivers.put(code,consumer);
    }
}
