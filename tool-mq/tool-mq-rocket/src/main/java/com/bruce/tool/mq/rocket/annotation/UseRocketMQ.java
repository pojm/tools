package com.bruce.tool.mq.rocket.annotation;

import com.bruce.tool.mq.rocket.config.RocketRegister;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 11:24 2019-02-14
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(RocketRegister.class)
public @interface UseRocketMQ {
}
