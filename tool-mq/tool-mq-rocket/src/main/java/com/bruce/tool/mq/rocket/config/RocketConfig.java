package com.bruce.tool.mq.rocket.config;

import lombok.Data;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 11:26 2019-02-14
 */
@Data
public class RocketConfig {
    /**业务编码,用于区分消息队列的用途**/
    private String code;
    private String url;
    private String group;
    private String topic;
    private String tags;
    /**消息发送模式:集群(CLUSTERING)/广播(BROADCASTING)**/
    private String model;
}
