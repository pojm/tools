package com.bruce.tool.mq.rocket.domain;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 10:03 2019-02-21
 */
@Data
public class RocketMessage {
    /**消息唯一标识key**/
    private String key;
    /**业务唯一标识serviceCode**/
    @NotNull(message = "业务唯一标识为空,终止消息发送!")
    private String code;
    /**消息体**/
    @NotNull(message = "消息体为空,终止消息发送!")
    private byte[] body;
    /**消息创建时间**/
    private long timestamp;
}
