package com.bruce.tool.mq.rocket;

import com.bruce.tool.common.util.SpringBeanUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * 消息队列独立项目
 * 功能 :
 * 所有消息,都经过这个节点,用于存储消息副本
 *
 * 消息日志打印规则:
 * 1.系统中不打印消息完整内容(完整内容可以根据messageid到阿里云查询)
 * 2.系统中打印messageid,和业务关键信息,用于关联查询
 * 3.测试环境,开发环境和正式环境,通过tag方式来做业务区分
 *
 * 注意事项:
 * 消息队列调用方式
 *
 * @author : Bruce(刘正航) 上午12:49 2018/2/11
 */
@SpringBootApplication
public class RocketApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(RocketApplication.class, args);
        SpringBeanUtils.setApplicationContext(context);
    }
}
