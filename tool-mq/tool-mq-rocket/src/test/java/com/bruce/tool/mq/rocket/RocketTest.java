package com.bruce.tool.mq.rocket;

import com.bruce.tool.common.util.DateUtils;
import com.bruce.tool.mq.rocket.core.RocketReceiver;
import com.bruce.tool.mq.rocket.core.RocketSender;
import com.bruce.tool.mq.rocket.domain.RocketMessage;
import com.bruce.tool.mq.rocket.domain.RocketResponse;
import com.bruce.tool.mq.rocket.handler.RocketHandler;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 10:53 2019-02-21
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RocketApplication.class)
public class RocketTest {

    @Autowired
    private RocketSender rocketSender;

    @Autowired
    private RocketReceiver rocketReceiver;

    @Test
    public void send(){
        RocketMessage message = new RocketMessage();
        message.setCode("111");
        message.setKey(UUID.randomUUID().toString());
        message.setTimestamp(DateUtils.create().getMillis());
        message.setBody("Hello Rocket".getBytes(StandardCharsets.UTF_8));
        rocketSender.execute(message);
    }

    @Test
    public void receive(){
        rocketReceiver.execute("111", new RocketHandler() {
            @Override
            public ConsumeConcurrentlyStatus handle(RocketResponse response) {
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
    }
}
