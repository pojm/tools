####Rabbit使用说明
#####Rabbit安装(mac)
```
1.安装之前更新brew
brew update
2.安装rabbitmq服务
brew install rabbitmq
3.启动rabbitmq服务
rabbitmq-server start
4.访问rabbitmq后台
访问地址:http://127.0.0.1:15672
默认账号:guest/guest
```
#####Rabbit使用(用户管理)
```
1.添加用户
rabbitmqctl add_user username password;
2.删除用户
rabbitmqctl delete_user username;
3.修改密码
rabbitmqctl change_password username password;
4.验证用户密码是否正确
rabbitmqctl authenticate_user username password;
5.查看用户列表
rabbitmqctl list_users;
6.授权用户(可以给一个用户,授予多个角色)
  用户角色:
    超级管理员(可登陆):administrator
        启用management plugin的情况下
            可查看所有信息
            可操作用户信息
            可操作策略信息
    监控者(可登陆):monitoring
        启用management plugin的情况下
            可查看队列节点相关信息(进程数,内存使用情况,磁盘使用情况)
    策略制定者(可登陆):policymaker
        启用management plugin的情况下
            可管理policy
            不可查看节点信息
    普通管理者(可登陆):management
        启用management plugin的情况下
            无法查看节点信息
            无法操作策略信息
    其他角色(不可登陆)
        属于普通的生产者和消费者    
  授权角色:
    rabbitmqctl set_user_tags username role_name;
    rabbitmqctl set_user_tags username role_name rolename2 rolename3;
  删除角色:
    rabbitmqctl set_user_tags username;
7.设置vhost
    添加
        rabbitmqctl add_vhost test
    删除
        rabbitmqctl delete_vhost test
    查询
        rabbitmqctl list_vhosts
    设置跟踪
        rabbitmqctl list_vhosts hostname tracing
8.设置权限
    增加权限
    #set_permissions [-p vhost] user conf write read
    rabbitmqctl set_permissions -p vhost admin ".*" ".*" ".*"
    清除权限
    #clear_permissions [-p vhost] username
    rabbitmqctl clear_permissions -p vhost admin
    权限列表
    #list_permissions [-p vhost]
    rabbitmqctl list_permissions -p vhost
    查询用户权限
    rabbitmqctl list_user_permissions admin
    
    设置topic权限
    #set_topic_permissions [-p vhost] user exchange write read
    rabbitmqctl set_topic_permissions -p vhost admin amq.topic “^{username}-.*” “^{username}-.*”
    rabbitmqctl set_topic_permissions -p vhost admin amq.topic “admin.*” “admin.*”
    清除topic权限
    #clear_topic_permissions [-p vhost] username [exchange]
    rabbitmqctl clear_topic_permissions -p vhost admin amq.topic
    查询topic权限
    #list_topic_permissions [-p vhost]
    rabbitmqctl list_topic_permissions -p vhost
    查询用户topic权限
    rabbitmqctl list_user_topic_permissions admin
9.rabbitmqctl命令行说明
    http://www.rabbitmq.com/rabbitmqctl.8.html
10.
```