package com.bruce.tool.office.excel.util;

import com.bruce.tool.office.excel.constant.RowErrorInfo;
import com.bruce.tool.office.excel.constant.SheetInfo;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Objects;
import java.util.Set;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 11:14 AM 2018/12/5
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CellValidUtils {

    /**
     * 数据对象,hibernate-validate校验
     **/
    public static <T> void validExcelLineData(SheetInfo sheetInfo, Integer rowNum, T t) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<T>> violations = validator.validate(t);
        if (violations != null && !violations.isEmpty()) {
            RowErrorInfo errorInfo = null;
            if( sheetInfo.getErrorInfos().size() - 1 > rowNum ){
                errorInfo = sheetInfo.getErrorInfos().get(rowNum);
            }
            if( Objects.isNull(errorInfo) ){
                errorInfo = new RowErrorInfo();
                sheetInfo.getErrorInfos().put(rowNum,errorInfo);
            }
            for (ConstraintViolation<T> info : violations) {
                errorInfo.getColumnErrors().add(info.getMessage());
            }
        }
    }
}
