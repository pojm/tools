package com.bruce.tool.office.excel.constant;

import com.google.common.collect.Lists;
import lombok.Data;

import java.util.List;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 6:39 PM 2018/11/27
 */
@Data
public class RowErrorInfo {
    private Integer row;
    private List<String> columnErrors = Lists.newArrayList();
}
