package com.bruce.tool.office.excel.extend;

import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Color;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFColor;

import java.util.Arrays;
import java.util.Objects;

/**
 * 功能 :
 * 从输入流加载Excel文件
 * @author : Bruce(刘正航) 下午3:10 2018/3/7
 */
public class ExcelToHtmlUtils extends org.apache.poi.hssf.converter.ExcelToHtmlUtils {

    public static final String EMPTY = "";

    /**
     * Creates a map (i.e. two-dimensional array) filled with ranges. Allow fast
     * retrieving {@link CellRangeAddress} of any cell, if cell is contained in
     * range.
     *
     * @see #getMergedRange(CellRangeAddress[][], int, int)
     */
    public static CellRangeAddress[][] buildMergedRangesMap( Sheet sheet ) {
        CellRangeAddress[][] mergedRanges = new CellRangeAddress[1][];
        for ( final CellRangeAddress cellRangeAddress : sheet.getMergedRegions() ) {
            final int requiredHeight = cellRangeAddress.getLastRow() + 1;
            if ( mergedRanges.length < requiredHeight ) {
                CellRangeAddress[][] newArray = new CellRangeAddress[requiredHeight][];
                System.arraycopy( mergedRanges, 0, newArray, 0, mergedRanges.length );
                mergedRanges = newArray;
            }

            for ( int r = cellRangeAddress.getFirstRow(); r <= cellRangeAddress.getLastRow(); r++ ) {
                final int requiredWidth = cellRangeAddress.getLastColumn() + 1;

                CellRangeAddress[] rowMerged = mergedRanges[r];
                if ( rowMerged == null ) {
                    rowMerged = new CellRangeAddress[requiredWidth];
                    mergedRanges[r] = rowMerged;
                } else {
                    final int rowMergedLength = rowMerged.length;
                    if ( rowMergedLength < requiredWidth ) {
                        final CellRangeAddress[] newRow = new CellRangeAddress[requiredWidth];
                        System.arraycopy( rowMerged, 0, newRow, 0, rowMergedLength );

                        mergedRanges[r] = newRow;
                        rowMerged = newRow;
                    }
                }

                Arrays.fill( rowMerged, cellRangeAddress.getFirstColumn(), cellRangeAddress.getLastColumn() + 1, cellRangeAddress );
            }
        }
        return mergedRanges;
    }

    public static String getColor( Color colorr ) {
        String result = "0";
        if( colorr instanceof HSSFColor ){
            HSSFColor color = (HSSFColor)colorr;
            result = getColor(color.getTriplet());
        }
        if( colorr instanceof XSSFColor ){
            XSSFColor color = (XSSFColor)colorr;
            result = getColor(getDefaultRGB(color.getIndex()));
        }
        return result;
    }

    public static String getColor( short[] rgbs ){
        String result = null;
        if(Objects.isNull(rgbs) || rgbs.length == 0 ){ return result; }
        StringBuilder stringBuilder = new StringBuilder( 7 );
        stringBuilder.append( '#' );
        for ( short s :  rgbs) {
            if ( s < 10 ) { stringBuilder.append( '0' ); }
            stringBuilder.append( Integer.toHexString( (int)s ) );
        }

        result = stringBuilder.toString();

        String x = getColor(result);
        if (x != null) { return x; }

        return result;
    }

    public static String getColor( byte[] rgbs ){
        String result = null;
        if(Objects.isNull(rgbs) || rgbs.length == 0 ){ return result; }
        StringBuilder stringBuilder = new StringBuilder( 7 );
        stringBuilder.append( '#' );

        for ( byte s :  rgbs) {
            if ( s < 10 ) { stringBuilder.append( '0' ); }
            stringBuilder.append( Integer.toHexString( (int)s ) );
        }

        result = stringBuilder.toString();

        String x = getColor(result);
        if (x != null) { return x; }

        return result;
    }

    public static short[] getDefaultRGB(int index){
        HSSFColor hssfColor = HSSFColor.getIndexHash().get(index);
        if (hssfColor == null) { return null; }
        return hssfColor.getTriplet();
    }

    private static String getColor(String result) {
        if ( "#ffffff".equals(result) ){ return "white"; }

        if ( "#c0c0c0".equals(result) ) { return "silver"; }

        if ( "#808080".equals(result) ) { return "gray"; }

        if ( "#000000".equals(result) ) { return "black"; }
        return null;
    }

}
