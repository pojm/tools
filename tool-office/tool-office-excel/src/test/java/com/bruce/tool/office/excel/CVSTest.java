package com.bruce.tool.office.excel;

import com.bruce.tool.common.util.DateUtils;
import com.bruce.tool.common.util.LogUtils;
import com.bruce.tool.common.util.string.JsonUtils;
import com.bruce.tool.office.excel.core.CSV;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Map;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 9:56 PM 2018/12/6
 */
@Slf4j
public class CVSTest {

    private List<Object> datas = Lists.newArrayList();

    @Before
    public void init(){
        for ( int i = 0; i< 101; i++ ) {
            Map data = Maps.newHashMap();
            data.put("name","测试测试测试"+i);
            data.put("params","https://blog.csdn.net/gaolu/article/details/52708177导入模板格式固定，不得删除标题行，不得对标题行做任何编辑"+i);
            data.put("paramType",i % 2);
            data.put("url","https://blog.csdn.net/gaolu/article/details/52708177"+i);
            data.put("requestType","POST"+i);
            data.put("expectCode","0"+i);
            data.put("result","展示的结果"+i);
            data.put("price",(3.45689 * i));
            data.put("date", DateUtils.create().getMillis());
            datas.add(data);
        }
    }

    @Test
    /**使用格式化类作为表头**/
    public void exportUseClass() throws Exception {
        // 说明:导出的时候,不管data是List<Map>还是List<Class>,都能够用Class的方式导出
        CSV.create()
                .datas(datas)
                .titles(Interface2.class)
                .exportToStream(new FileOutputStream(System.getProperty("user.home")+"/Desktop/test.csv"));
    }

    @Test
    /**使用格式化类作为表头**/
    public void exportUseString() throws Exception {
        // 说明:导出的时候,不管data是List<Map>还是List<Class>,都能够用Class的方式导出
        CSV.create()
                .datas(datas)
                .titles("接口名称:name,请求url地址:url,请求参数json数据:params,参数请求类型:paramType,请求方式:requestType,期望的返回结果code:expectCode,完整的返回结果:result,价格:price,日期:date:yyyy-MM-dd HH:mm:ss")
                .exportToStream(new FileOutputStream(System.getProperty("user.home")+"/Desktop/test.csv"));
    }

    @Test
    public void importUseClass() throws Exception {
        List datas = CSV.create()
                .titles(Interface2.class)
                .stream(new FileInputStream(System.getProperty("user.home")+"/Desktop/test.csv"))
                .importFromStream();
        LogUtils.info(log,JsonUtils.objToStr(datas));
    }


    @Test
    public void importUseString() throws Exception {
        List datas = CSV.create()
                .titles("接口名称:name,请求url地址:url,请求参数json数据:params,参数请求类型:paramType,请求方式:requestType,期望的返回结果code:expectCode,完整的返回结果:result,价格:price,日期:date:yyyy-MM-dd HH:mm:ss")
                .stream(new FileInputStream(System.getProperty("user.home")+"/Desktop/test.csv"))
                .importFromStream();
        LogUtils.info(log,JsonUtils.objToStr(datas));
    }

}
