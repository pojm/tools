### tool-office-excel工具包使用说明
#### 项目地址
```
https://gitee.com/run2dream/tools/tree/master/tool-office/tool-office-excel
```
#### maven依赖
```
<dependency>
    <groupId>com.bruce.tool</groupId>
    <artifactId>tool-office-excel</artifactId>
    <version>7.0.RELEASE</version>
</dependency>
```
#### 基本功能(支持xls和xlsx)
```
1.excel文件导出
    支持自定义title导出
        2种不同的方式,设置excel表头
    支持指定模板文件导出
        指定excel模板文件流
2.excel文件导入
    支持自定义title导入
        2种不同的方式,设置excel表头
    支持指定列,指定行导入(无title)
3.特殊功能
    支持excel内容转html(支持xls和xlsx)
    支持导出单元格下拉值设置
```
#### 功能样例-格式化类说明-源码
```
public class Interface2 {
    /**
     * 接口名称
     */
    @Header(name = "接口名称",prompt = "测试提示内容",dropdown = {"3213","djdjd","哈哈"},order = "1",align = HorizontalAlignment.LEFT)
    @NotNull(message = "接口名称不能为空")
    @Length(min = 1,max = 15,message = "接口名称长度大于1")
    private String name;
    /**
     * 请求url地址
     */
    @Header(name = "请求url地址",order = "2",prompt = "测试提示内容")
    @NotNull(message = "接口地址不能为空")
    @Length(min = 10,max = 100,message = "接口地址大于10")
    private String url;
    /**
     * 请求参数json数据
     */
    @Header(name = "请求参数json数据",order = "4")
    private String params;
    /**
     * 参数请求类型：json/form
     */
    @Header(name = "参数请求类型")
    private Integer paramType;
    /**
     * 请求方式：get/post
     */
    @Header(name = "请求方式",order = "10")
    private String requestType;
    /**
     * 期望的返回结果code
     */
    @Header(name = "期望的返回结果code",order = "-1")
    private String expectCode;
    /**
     * 完整的返回结果
     */
    @Header(name = "完整的返回结果",order = "-2")
    private String result;

    @Header(name ="价格", format = Format.DECIMAL_2, regex = "[0-9]+(\\.[0-9]{0,5})?" , align = HorizontalAlignment.LEFT)
    private Double price;

    @Header(name = "日期",type = Date.class, format = "yyyy-MM-dd")
    private String date;

}
```
#### 功能样例-"格式化类"说明-补充
```
@Header注解:
1.模板类,字段名和Header注解的name值形成对应关系.
2.Header注解中,order可设置字段导出时候的顺序
3.模板中format可实现对excel单元格的格式化
4.模板中regex可实现对excel单元格值的校验
5.每一个字段都支持Hibernate-validate校验注解
6.支持对日期格式的自动格式化
7.支持对日期的指定格式模板的格式化
8.支持单元格对齐方式设置
```
#### 功能样例-导出-用"格式化类"作为模板
```
datas数据列表,自己初始化
FileOutputStream out = new FileOutputStream(System.getProperty("user.home")+"/Desktop/test5.xls");
        Map<String,List<Object>> handler = Maps.newHashMap();
        handler.put("请求url地址",Lists.newArrayList("百度地址","阿里地址","本地地址","吃瓜地址"));
        Excels.Export()
                .config(Version.XLS,200)
                .addSheet(datas,Interface2.class,handler,"东成西就","操作说明：\n" +
                        "1、导入模板格式固定，不得删除标题行，不得对标题行做任何编辑，不得插入、删除标题列，不得调整标题行和列的位置。\n" +
                        "2、导入模板内容仅包括：楼号、单元/楼层、房间号、房间面积、房东姓名、房东手机号、房东身份证号码，7项，不得任意添加其他导入项目，所有项均为必填项。\n" +
                        "3、【楼号】：支持中文、数字和英文，如果是社区，需要输入“11号楼”或“11栋”之类的内容，如果是写楼楼，可以输入“A座”之类的内容； 但无论输入什么内容，必须所有内容统一，否则为显示为2个楼号；\n" +
                        "4、【单元/楼层】【房间号】：仅支持数字和英文；   最终展示的结果为是“5#402”； 如果是社区，建议是“单元号#房间号”，如果是楼层，建议是“楼层#房间号”；\n" +
                        "4、【房间面职】：主要为了区分房间的户型，不需要精确输入房间面积；\n" +
                        "5、【房东姓名】：支持汉字、英文、数字；【房东手机号】：仅支持11位阿拉伯数字，数字前后、之间不得有空格或其他字符；   【房东身份证号】：身份证号码必须是正确的身份证号，错误的号码将无法上传；\n" +
                        "6、导入模板各项内容不符合编辑规则的，导入时，系统将自动跳过，并计为导入失败数据；")
                .addSheet(datas,Interface2.class,"西就东成")
                .toStream(out);
```
#### 功能样例-导出-用字符串title作为模板
```
FileOutputStream out = new FileOutputStream(System.getProperty("user.home")+"/Desktop/test5.xls");
        String titles = "接口名称:name,请求url地址:url,请求参数json数据:params,参数请求类型:paramType,请求方式:requestType,期望的返回结果code:expectCode,完整的返回结果:result,价格:price,日期:date";
        Excels.Export()
                .config(Version.XLS,99)
                .addSheet(datas,titles,"东成西就","操作说明：\n" +
                        "1、导入模板格式固定，不得删除标题行，不得对标题行做任何编辑，不得插入、删除标题列，不得调整标题行和列的位置。\n" +
                        "2、导入模板内容仅包括：楼号、单元/楼层、房间号、房间面积、房东姓名、房东手机号、房东身份证号码，7项，不得任意添加其他导入项目，所有项均为必填项。\n" +
                        "3、【楼号】：支持中文、数字和英文，如果是社区，需要输入“11号楼”或“11栋”之类的内容，如果是写楼楼，可以输入“A座”之类的内容； 但无论输入什么内容，必须所有内容统一，否则为显示为2个楼号；\n" +
                        "4、【单元/楼层】【房间号】：仅支持数字和英文；   最终展示的结果为是“5#402”； 如果是社区，建议是“单元号#房间号”，如果是楼层，建议是“楼层#房间号”；\n" +
                        "4、【房间面职】：主要为了区分房间的户型，不需要精确输入房间面积；\n" +
                        "5、【房东姓名】：支持汉字、英文、数字；【房东手机号】：仅支持11位阿拉伯数字，数字前后、之间不得有空格或其他字符；   【房东身份证号】：身份证号码必须是正确的身份证号，错误的号码将无法上传；\n" +
                        "6、导入模板各项内容不符合编辑规则的，导入时，系统将自动跳过，并计为导入失败数据；")
                .addSheet(datas,titles,"西就东成")
                .toStream(out);
```
#### 功能样例-导出-用excel文件模板作为表头
```
template文件里边包含表头
FileInputStream template = new FileInputStream(System.getProperty("user.home")+"/Desktop/test5.xls");
        FileOutputStream out = new FileOutputStream(System.getProperty("user.home")+"/Desktop/test6.xls");
        Excels.Export()
                .addSheet(datas,template)
                .addSheet(datas,template)
                .toStream(out);
```
#### 功能样例-导入-用"格式化类"作为模板
```
List result = Excels.Import()
                .config(new FileInputStream(System.getProperty("user.home")+"/Desktop/test1.xls"))
                .addSheet(Interface2.class,0)
                .addSheet(Interface2.class,1)
                .addSheet(Interface2.class)
                .onedata();
        LogUtils.info(log,JsonUtils.objToStr(result));
```
#### 功能样例-导入-用字符串title作为模板
```
String titles = "接口名称:name,请求url地址:url,请求参数json数据:params,参数请求类型:paramType,请求方式:requestType,期望的返回结果code:expectCode,完整的返回结果:result,价格:price";
        List result = Excels.Import()
                .config(new FileInputStream(System.getProperty("user.home")+"/Desktop/testone.xlsx"))
                .addSheet(titles)
                .onedata();
        LogUtils.info(log,JsonUtils.objToStr(result));
```
#### 功能样例-导入-指定列
```
List result = Excels.Import()
                .config(new FileInputStream(System.getProperty("user.home")+"/Desktop/test1.xls"))
                .addSheet(new Integer[]{1,2,3})
                .onedata();
        LogUtils.info(log,JsonUtils.objToStr(result));
```
#### 功能样例-导入-指定行-指定列
```
List result = Excels.Import()
                .config(new FileInputStream(System.getProperty("user.home")+"/Desktop/test1.xls"))
                .addSheet(new Integer[]{2,3,4},new Integer[]{1,2,3})
                .onedata();
        LogUtils.info(log,JsonUtils.objToStr(result));
```
#### 功能样例-导入-指定行列范围(矩阵)
```
List result = Excels.Import()
                .config(new FileInputStream(System.getProperty("user.home")+"/Desktop/test1.xls"))
                .addSheet(2,4,1,3)
                .onedata();
        LogUtils.info(log,JsonUtils.objToStr(result));
```