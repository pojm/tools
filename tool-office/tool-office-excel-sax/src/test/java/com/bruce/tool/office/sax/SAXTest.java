package com.bruce.tool.office.sax;

import org.apache.poi.hssf.eventusermodel.FormatTrackingHSSFListener;
import org.apache.poi.hssf.eventusermodel.HSSFEventFactory;
import org.apache.poi.hssf.eventusermodel.HSSFRequest;
import org.apache.poi.hssf.eventusermodel.MissingRecordAwareHSSFListener;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * 功能 : 
 *
 * @author : Bruce(刘正航) 22:41 2019-05-28
 */
public class SAXTest {
    public static void main(String[] args) throws IOException {
        LineListener lineListener = new LineListener();
        FileInputStream template = new FileInputStream(System.getProperty("user.home")+"/Desktop/test5.xls");
        POIFSFileSystem pfs = new POIFSFileSystem(template);
        MissingRecordAwareHSSFListener listener = new MissingRecordAwareHSSFListener(lineListener);
        FormatTrackingHSSFListener formatListener = new FormatTrackingHSSFListener(listener);
        HSSFEventFactory factory = new HSSFEventFactory();
        HSSFRequest request = new HSSFRequest();
            // 计算公式
            request.addListenerForAllRecords(formatListener);
            // 不计算公式
//            EventWorkbookBuilder.SheetRecordCollectingListener workbookBuildingListener = new EventWorkbookBuilder.SheetRecordCollectingListener(formatListener);
//            request.addListenerForAllRecords(workbookBuildingListener);
        factory.processWorkbookEvents(request, pfs);
    }
}
