package com.bruce.tool.office.sax;

public class RowReader{
	
	private Integer curRow=-1;
	public void getRows(int sheetIndex, int curRow, String[] row) {
		if(this.curRow!=curRow) {
			System.out.print("sheet:"+sheetIndex+"-row:"+curRow+" ");
			for (String cell : row) {
				System.out.print(cell + "  ");
			}
			System.out.println();
			this.curRow=curRow;
		}
		
	}
}