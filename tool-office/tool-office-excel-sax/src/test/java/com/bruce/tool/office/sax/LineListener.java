package com.bruce.tool.office.sax;

import org.apache.poi.hssf.eventusermodel.HSSFListener;
import org.apache.poi.hssf.record.*;

/**
 * 功能 : 
 *
 * @author : Bruce(刘正航) 22:33 2019-05-28
 */
public class LineListener implements HSSFListener {

    @Override
    public void processRecord(final Record record) {
        switch(record.getSid()){
            case BoundSheetRecord.sid:
//                System.out.println(record);
                break;
            case BOFRecord.sid:
                BOFRecord bofRecord = (BOFRecord) record;
//                System.out.println(bofRecord);
                break;
            case SSTRecord.sid:
                // 数据存储的记录
                SSTRecord sstRecord = (SSTRecord) record;
                System.out.println(sstRecord);
                break;
            case BlankRecord.sid:
                // 空白的单元格记录
//                System.out.println(record);
                break;
            case BoolErrRecord.sid:
                // bool类型单元格
//                System.out.println(record);
                break;
            case FormulaRecord.sid:
                // 有公式的单元格
//                System.out.println(record);
                break;
            case StringRecord.sid:
                // 单元格中,公式的字符串
//                System.out.println(record);
                break;
            case LabelRecord.sid:
                System.out.println(record);
                break;
            case LabelSSTRecord.sid:
                // 单元格为字符串
                System.out.println(record);
                break;
            case NumberRecord.sid:
                // 单元格为数字
//                System.out.println(record);
                break;

            default:
                break;
        }
    }
}
