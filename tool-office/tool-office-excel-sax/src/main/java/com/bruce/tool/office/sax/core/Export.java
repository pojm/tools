package com.bruce.tool.office.sax.core;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * 功能 :
 * @author : Bruce(刘正航) 4:09 下午 2019/12/1
 */
@Slf4j
@NoArgsConstructor(staticName = "create",access = AccessLevel.PROTECTED)
public class Export {
}
