package com.bruce.tool.office.sax.constant;

import lombok.Getter;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 3:58 PM 2018/11/27
 */
public enum Version {
    XLS("2003"),
    XLSX("2007"),
    ;
    @Getter
    private String value;

    Version(String value) {
        this.value = value;
    }
}
