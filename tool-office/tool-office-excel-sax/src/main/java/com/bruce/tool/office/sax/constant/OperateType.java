package com.bruce.tool.office.sax.constant;

import lombok.Getter;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 4:15 PM 2018/12/5
 */
public enum OperateType {
    ACTIVE("激活的Sheet"),
    ASSIGN("指定的Sheet"),
    ALL("所有的Sheet"),

    ;
    @Getter
    private String desc;

    OperateType(String desc) {
        this.desc = desc;
    }
}
