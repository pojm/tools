package com.bruce.tool.office.sax.core;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 00:08 2019-01-22
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Excels {

    public static Export Export(){
        return Export.create();
    }

    public static Import Import(){
        return Import.create();
    }
}
