package com.bruce.tool.office.sax.constant;

import lombok.Getter;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 5:36 PM 2018/11/27
 */
public enum ErrorCode {
    VALID_ERROR("00001","校验异常"),
    EXPORT_ERROR("00002","导入异常"),

    ;
    @Getter
    private String code;
    @Getter
    private String message;

    ErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }}
