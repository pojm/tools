package com.bruce.tool.mybatis.service.manager;


import com.bruce.tool.common.util.ClassUtils;
import com.bruce.tool.common.util.string.StringUtils;
import com.bruce.tool.mybatis.orm.mapper.BaseMapper;
import com.bruce.tool.mybatis.orm.query.CriteriaQuery;
import com.bruce.tool.mybatis.orm.util.TableUtils;
import com.bruce.tool.mybatis.service.dto.Page;
import com.bruce.tool.mybatis.service.form.Groupable;
import com.bruce.tool.mybatis.service.form.Idsable;
import com.bruce.tool.mybatis.service.form.Limitable;
import com.bruce.tool.mybatis.service.form.Orderable;
import com.github.pagehelper.PageHelper;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

/**
 * CURD功能实现
 * F代表Form
 * R代表Result
 * D代表Domain
 * 功能点 :
 * 1.CRUD简单实现
 * 2.复杂查询方法实现(分页查询/不分页查询)
 * @author : Bruce(刘正航) 下午1:34 2018/5/9
 */
public abstract class BaseManager<F,D>{

    protected abstract Class<D> clazz();

    /**
     * @return
     */
    protected abstract BaseMapper<D> baseMapper();
    protected abstract CriteriaQuery buildCriteria(F form);

    ///////////////////////////////////////////////增///////////////////////////////////////////////////////////
    public Integer save(D domain){
        return baseMapper().save(domain);
    }
    public Integer saveAll(List<D> list){ return baseMapper().saveAll(list); }
    public Integer saveBySQL(String sql){ return baseMapper().saveBySQL(sql); }

    ///////////////////////////////////////////////删///////////////////////////////////////////////////////////
    public Integer deleteByDomainId(D domain){ return baseMapper().deleteByDomainId(domain);}
    public Integer deleteById(Object id){
        return baseMapper().deleteById(clazz(),id);
    }
    public Integer deleteByColumn(String column, Object value){ return baseMapper().deleteByColumn(clazz(),column,value); }
    public Integer deleteByColumns(D domain){return baseMapper().deleteByColumns(domain);}
    public Integer deleteBySQL(String sql){return baseMapper().deleteBySQL(sql);}
    public Integer markDeleteById(Object id){return baseMapper().markDeleteById(clazz(),id);}
    public Integer markDeleteByIdDiy(Object id,String mark){return baseMapper().markDeleteByIdDiy(clazz(),id,mark);}

    ///////////////////////////////////////////////改///////////////////////////////////////////////////////////
    public Integer update(D domain){return baseMapper().update(domain);}
    public Integer updateBySelective(D domain){return baseMapper().updateBySelective(domain);}
    public Integer updateAll(List<D> list){return baseMapper().updateAll(list);}
    public Integer updateBySQL(String sql){return baseMapper().updateBySQL(sql);}

    ///////////////////////////////////////////////查///////////////////////////////////////////////////////////
    public D findByDomain(D domain){return baseMapper().findByDomainId(domain);}
    public D findById(Object id){return baseMapper().findById(clazz(),id);}
    public D findByColumn(String key,Object value){return baseMapper().findByColumn(clazz(),key,value);}
    public D findByColumns(D domain){return baseMapper().findByColumns(domain);}
    public Map findByColumnsTableCriteria(String columns,String table, F form){return baseMapper().findByColumnsTableCriteria(columns, table, buildCriteria2(form));}
    public Map findBySQLCriteria(String sql, F form){return baseMapper().findBySQLCriteria(sql, buildCriteria2(form));}
    public Map findBySQL(String sql){return baseMapper().findBySQL(sql);}
    public List<D> findByIds(List<Object> ids){return baseMapper().findsByIds(clazz(),ids);}
    public List<D> findByColumnCollection(List<Object> ids,String key){return baseMapper().findsByColumnCollection(clazz(),ids,key);}
    public List<D> findsByColumn(String key, Object value){return baseMapper().findsByColumn(clazz(), key, value);}
    public List<D> findsByColumns(D domain){return baseMapper().findsByColumns(domain);}
    public List<D> findsByCriteria(F form){return baseMapper().findsByCriteria(clazz(), buildCriteria2(form));}
    public List<Map> findsByColumnsTableCriteria(String columns, String table, F form){return baseMapper().findsByColumnsTableCriteria(columns, table, buildCriteria2(form));}
    public List<Map> findsBySQLCriteria(String sql, F form){return baseMapper().findsBySQLCriteria(sql, buildCriteria2(form));}
    public List<Map> findsBySQL(String sql){return baseMapper().findsBySQL(sql);}

    ///////////////////////////////////////////////分页查询///////////////////////////////////////////////////////////
    public Page<D> findPageByColumn(String key, Object value,Integer page,Integer size){
        return PageHandler.<D>build().handle(() -> PageHelper.startPage(page,size).doSelectPage(() -> baseMapper().findsByColumn(clazz(), key, value)));
    }
    public Page<D> findPageByColumns(D domain,Integer page,Integer size){
        return PageHandler.<D>build().handle(() -> PageHelper.startPage(page,size).doSelectPage(() -> baseMapper().findsByColumns(domain)));
    }
    public Page<D> findPageByCriteria(F form,Integer page,Integer size){
        return PageHandler.<D>build().handle(() -> PageHelper.startPage(page,size).doSelectPage(() -> baseMapper().findsByCriteria(clazz(), buildCriteria2(form))));
    }
    public Page<Map> findPageByColumnsTableCriteria(String columns, String table, F form,Integer page,Integer size){
        return PageHandler.<Map>build().handle(() -> PageHelper.startPage(page,size).doSelectPage(() -> baseMapper().findsByColumnsTableCriteria(columns, table, buildCriteria2(form))));
    }
    public Page<Map> findPageBySQLCriteria(String sql, F form,Integer page,Integer size){
        return PageHandler.<Map>build().handle(() -> PageHelper.startPage(page,size).doSelectPage(() -> baseMapper().findsBySQLCriteria(sql, buildCriteria2(form))));
    }
    public Page<Map> findPageBySQL(String sql,Integer page,Integer size){
        return PageHandler.<Map>build().handle(() -> PageHelper.startPage(page,size).doSelectPage(() -> baseMapper().findsBySQL(sql)));
    }

    public List<D> findNextByColumn(String key, Object value,Integer page,Integer size){
        return PageHelper.startPage(page,size,false).doSelectPage(() -> baseMapper().findsByColumn(clazz(), key, value));
    }
    public List<D> findNextByColumns(D domain,Integer page,Integer size){
        return PageHelper.startPage(page,size,false).doSelectPage(() -> baseMapper().findsByColumns(domain));
    }
    public List<D> findNextByCriteria(F form,Integer page,Integer size){
        return PageHelper.startPage(page,size,false).doSelectPage(() -> baseMapper().findsByCriteria(clazz(), buildCriteria2(form)));
    }
    public List<Map> findNextByColumnsTableCriteria(String columns, String table, F form,Integer page,Integer size){
        return PageHelper.startPage(page,size,false).doSelectPage(() -> baseMapper().findsByColumnsTableCriteria(columns, table, buildCriteria2(form)));
    }
    public List<Map> findNextBySQLCriteria(String sql, F form,Integer page,Integer size){
        return PageHelper.startPage(page,size,false).doSelectPage(() -> baseMapper().findsBySQLCriteria(sql, buildCriteria2(form)));
    }
    public List<Map> findNextBySQL(String sql,Integer page,Integer size){
        return PageHelper.startPage(page,size,false).doSelectPage(() -> baseMapper().findsBySQL(sql));
    }

    /**传递form表单特殊属性**/
    private final CriteriaQuery buildCriteria2(F form){
        CriteriaQuery query = buildCriteria(form);
        // 判断是否包含groupby
        if( form instanceof Groupable){
            Groupable groupable = (Groupable)form;
            String groupBy = groupable.getGroupBy();
            if(StringUtils.isNotBlank(groupBy)){
                query.groupBy(groupBy);
            }
        }
        // 判断是否有排序要求
        if( form instanceof Orderable){
            Orderable orderable = (Orderable) form;
            String orderBy = orderable.getOrderBy();
            if(StringUtils.isNotBlank(orderBy)){
                query.orderBy(orderBy);
            }
        }
        // 判断是否包含limit
        if( form instanceof Limitable){
            Limitable limitable = (Limitable) form;
            String limit = limitable.getLimit();
            if(StringUtils.isNotBlank(limit)){
                query.limit(limit);
            }
        }
        // 判断是否包含ids集合
        if(!(form instanceof Idsable)){
            return query;
        }
        // 判断是否包含对应的ids集合数据
        Idsable idsable = (Idsable) form;
        List<Object> ids = idsable.getIds();
        if(CollectionUtils.isEmpty(ids)){
           return query;
        }

        // 追加ids集合数据到sql
        Field field = TableUtils.getPrimary(clazz());
        String primaryKey = TableUtils.getPrimaryKey(clazz());
        Class<?> clazz = ClassUtils.getFieldType(clazz(),field.getName());
        if( String.class.equals(clazz) ){
            query.and().where(" " + primaryKey + " in ('"+StringUtils.join(ids,"','")+"') ");
        }else if( ClassUtils.isNumberType(clazz) ){
            query.and().where(" " + primaryKey + " in ("+StringUtils.join(ids,",")+") ");
        }
        return query;
    }
}

