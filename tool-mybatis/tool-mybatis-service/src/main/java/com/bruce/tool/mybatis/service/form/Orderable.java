package com.bruce.tool.mybatis.service.form;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 09:21 2019-02-13
 */
public interface Orderable {
    String getOrderBy();
}
