package com.bruce.tool.mybatis.service.manager;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 23:18 2019-01-31
 */
public interface Select<D> {
    com.github.pagehelper.Page<D> execute();
}
