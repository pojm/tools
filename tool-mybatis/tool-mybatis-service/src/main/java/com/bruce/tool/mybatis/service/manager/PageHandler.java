package com.bruce.tool.mybatis.service.manager;

import com.bruce.tool.mybatis.service.dto.Page;
import lombok.NoArgsConstructor;

/**
 * 功能 :
 * 分页转换
 * @author : Bruce(刘正航) 23:20 2019-01-31
 */
@NoArgsConstructor(staticName = "build")
public class PageHandler<D> {

    /**分页查询**/
    public Page<D> handle(Select<D> select){
        // 增加 其他参数,比如排序等
        com.github.pagehelper.Page<D> data = select.execute();

        //两个page之间转换
        return Page.<D>builder()
                .page(data.getPageNum()).size(data.getPageSize()).total(data.getTotal())
                .list(data.getResult()).build();
    }
}
