package com.bruce.tool.mybatis.service.form;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 22:37 2019-01-31
 */
public interface Pageable {
    Integer getPage();
    Integer getSize();
}
