package com.bruce.tool.mybatis.service.form;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 08:26 2019-02-13
 */
public interface Limitable {
    String getLimit();
}
