package com.bruce.tool.mybatis.service.form;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 18:03 2019-02-12
 */
public interface Groupable {
    String getGroupBy();
}
