package com.bruce.tool.mybatis.service.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 下午2:32 2018/4/13
 */
@Data
@Builder
public class Page<T> {

    private Integer page;
    private Integer size;
    private Long total;
    private List<T> list;

}
