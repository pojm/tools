package com.bruce.tool.mybatis.generator.plugin;

import com.google.common.collect.Lists;
import org.mybatis.generator.api.GeneratedXmlFile;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.*;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * 注解方式:生成Dao
 * 功能 : 
 * 1.继承CRUD接口
 * 2.增加包扫描注解
 * 3.清除原有接口自带的方法
 * @author : Bruce(刘正航) 11:57 2019-05-11
 */
public class AnnotationDaoPlugin extends PluginAdapter {

    private final List<String> imports = Lists.newArrayList(
            "org.apache.ibatis.annotations.Mapper",
            "com.bruce.tool.mybatis.orm.mapper.BaseMapper",
            "org.springframework.stereotype.Component"
    );

    private final List<String> generatedKeysImports = Lists.newArrayList(
            "com.bruce.tool.mybatis.orm.provider.SaveProvider",
            "org.apache.ibatis.annotations.InsertProvider",
            "org.apache.ibatis.annotations.Options"
    );

    /**表主键常量**/
    private static final String ID = "id";

    private final List<String> annotations = Lists.newArrayList("@Mapper","@Component");

    private static final String INTERFACE_MAPPER = "BaseMapper";

    @Override
    public boolean validate(final List<String> warnings) {
        return true;
    }

    @Override
    public boolean sqlMapGenerated(GeneratedXmlFile sqlMap, IntrospectedTable introspectedTable) {
        return false;
    }

    /**生成实体**/
    @Override
    public boolean clientGenerated(Interface interfaze, IntrospectedTable introspectedTable) {
        interfaze.addImportedType(new FullyQualifiedJavaType(introspectedTable.getBaseRecordType()));
        interfaze.addSuperInterface(new FullyQualifiedJavaType(INTERFACE_MAPPER+"<"+new FullyQualifiedJavaType(introspectedTable.getBaseRecordType()).getShortName()+">"));
        // 清空所有方法(getter/setter方法)
        interfaze.getMethods().clear();
        for (String needImport : imports) {
            interfaze.addImportedType(new FullyQualifiedJavaType(needImport));
        }
        for (String annotation : annotations) {
            interfaze.addAnnotation(annotation);
        }
        List<IntrospectedColumn> columns = introspectedTable.getPrimaryKeyColumns();
        if(!CollectionUtils.isEmpty(columns) && columns.size() == 1){
            IntrospectedColumn column = columns.get(0);
            String primaryKey = column.getActualColumnName();
            if( !ID.equals(primaryKey) ){
                for (String needImport : generatedKeysImports) {
                    interfaze.addImportedType(new FullyQualifiedJavaType(needImport));
                }
                Method method = new Method("save");
                method.setReturnType(new FullyQualifiedJavaType("java.lang.Integer"));
                Parameter parameter = new Parameter(new FullyQualifiedJavaType(introspectedTable.getBaseRecordType()),"obj");
                method.addParameter(parameter);
                method.addAnnotation("@Override");
                method.addAnnotation("@InsertProvider(type = SaveProvider.class, method = \"save\")");
                String primaryField = column.getJavaProperty();
                method.addAnnotation("@Options(useGeneratedKeys = true,keyColumn = \""+primaryKey+"\",keyProperty = \""+primaryField+"\")");
                interfaze.addMethod(method);
            }
        }
        return true;
    }

}
