package com.bruce.tool.mybatis.orm.mapper;

import com.bruce.tool.mybatis.orm.domain.EquipmentDict;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

@Mapper
@Component
public interface EquipmentDictMapper extends BaseMapper<EquipmentDict> {
}