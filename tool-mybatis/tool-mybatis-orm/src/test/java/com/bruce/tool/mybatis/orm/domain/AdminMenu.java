package com.bruce.tool.mybatis.orm.domain;

import com.bruce.tool.mybatis.orm.annotation.Column;
import com.bruce.tool.mybatis.orm.annotation.Id;
import com.bruce.tool.mybatis.orm.annotation.Table;
import lombok.Data;

import java.io.Serializable;

/**
 * 菜单管理 
 */
@Data
@Table("admin_menu")
public class AdminMenu implements Serializable {
    /**
     * 菜单id
     */
    @Id("id")
    private Integer id;

    /**
     * 父级菜单ID
     */
    @Column("parent_id")
    private Integer parentId;

    /**
     * 菜单名称
     */
    @Column("menu_name")
    private String menuName;

    /**
     * 菜单链接
     */
    @Column("href")
    private String href;

    /**
     * 删除标记
     */
    @Column("is_delete")
    private Long isDelete;
}