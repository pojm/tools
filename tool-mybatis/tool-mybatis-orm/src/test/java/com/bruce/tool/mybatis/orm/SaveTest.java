package com.bruce.tool.mybatis.orm;

import com.bruce.tool.OrmApplication;
import com.bruce.tool.common.util.SpringBeanUtils;
import com.bruce.tool.common.util.string.JsonUtils;
import com.bruce.tool.mybatis.orm.domain.AdminMenu;
import com.bruce.tool.mybatis.orm.mapper.AdminMenuMapper;
import io.jsonwebtoken.lang.Assert;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 22:00 2019-01-20
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = OrmApplication.class)
public class SaveTest extends AbstractJUnit4SpringContextTests {

    @Autowired
    private AdminMenuMapper adminMenuMapper;

    @Before
    public void init(){
        SpringBeanUtils.setApplicationContext(applicationContext);
    }

    @Test
    public void save(){
        String json = "{\"parentId\":1,\"menuName\":\"用户管理\",\"href\":\"auth/user/index\",\"isDelete\":0}";
        AdminMenu menu = JsonUtils.strToObj(json,AdminMenu.class);
        Integer count = adminMenuMapper.save(menu);
        Assert.isTrue(count > 0);
    }

    @Test
    public void saveAll(){
        List<AdminMenu> menus = Lists.newArrayList();
        String json = "{\"parentId\":1,\"menuName\":\"用户管理\",\"href\":\"auth/user/index\",\"isDelete\":0}";
        AdminMenu menu = JsonUtils.strToObj(json,AdminMenu.class);
        menus.add(menu);
        Integer count = adminMenuMapper.saveAll(menus);
        Assert.isTrue(count > 0);
    }

    @Test
    public void saveBySQL(){
        String sql = "insert into admin_menu(parent_id,menu_name,href)values(1,'用户管理测试','auth/user/index/test')";
        Integer count = adminMenuMapper.saveBySQL(sql);
        Assert.isTrue(count > 0);
    }

}
