package com.bruce.tool.mybatis.orm;

import com.bruce.tool.common.util.LogUtils;
import com.bruce.tool.common.util.string.JsonUtils;
import com.bruce.tool.mybatis.orm.util.TableUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 13:22 2019-02-20
 */
@Slf4j
public class TableUtilsTest {

    @Test
    public void getPrimaryKey(){
        LogUtils.info(log,TableUtils.getPrimaryKey(Region.class));
        LogUtils.info(log,TableUtils.getField(Region.class,"level").getName());
        Region region = new Region();
        LogUtils.info(log, JsonUtils.objToStr(TableUtils.getFieldsWithValue(region)));

        System.out.println("com.bruce.tool.mybatis.orm.mapper.AdminMenuMapper.findMenuById".replaceAll("",""));
    }
}
