package com.bruce.tool.mybatis.orm;

import com.bruce.tool.mybatis.orm.annotation.Column;
import com.bruce.tool.mybatis.orm.annotation.Id;
import com.bruce.tool.mybatis.orm.annotation.Table;
import lombok.Data;

/**
 * 地址码表-基础表 
 */
@Data
@Table("global_region")
public class Region {
    /**
     * 
     */
    @Id("id")
    private Integer id;

    /**
     * 地址级别(全国0,省1,市2,区3,乡镇4)
     */
    @Column("level")
    private Integer level;

    /**
     * 是否是直辖市(1是,0不是)
     */
    @Column("is_direct")
    private Integer isDirect = 0;

    /**
     * 父节点编码
     */
    @Column("pcode")
    private String pcode;

    /**
     * 当前地址节点编码
     */
    @Column("code")
    private String code;

    /**
     * 地址名称
     */
    @Column("name")
    private String name;

    /**
     * 地址抓取路径
     */
    @Column("href")
    private String href;

    /**
     * 地址路径父页面路径
     */
    @Column("referer")
    private String referer;

    /**
     * 经度
     */
    @Column("longitude")
    private Double longitude;

    /**
     * 维度
     */
    @Column("latitude")
    private Double latitude;
}