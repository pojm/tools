package com.bruce.tool.mybatis.orm.mapper;

import com.bruce.tool.mybatis.orm.domain.AdminMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 01:12 2019-01-23
 */
@Mapper
@Component
public interface AdminMenuMapper extends BaseMapper<AdminMenu> {

    @Select("select * from admin_menu where id=#{id}")
    public String findMenuByIdForSql(Integer id);
}
