package com.bruce.tool.mybatis.orm.config;

import com.alibaba.druid.filter.logging.Slf4jLogFilter;
import com.alibaba.druid.pool.DruidDataSource;
import com.bruce.tool.orm.mybatis.interceptor.SqlJointInterceptor;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.session.SqlSessionFactory;
import org.assertj.core.util.Lists;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;

/**
 * @Prject: bqmart-elephant
 * @Package: cn.bqmart.config
 * @Description: TODO
 * @author: wuyujia
 * @Date: 2017/12/5 20:00
 */
@Configuration
@SuppressWarnings("all")
@EnableTransactionManagement
@MapperScan(basePackages = "com.bqmart.tool.mybatis.orm.mapper", sqlSessionFactoryRef = "cloudSqlSessionFactory")
public class CloudDruidConfig {

    @Autowired
    private SqlJointInterceptor sqlJointInterceptor;

    @ConfigurationProperties(prefix = "datasource.cloud")
    @Bean
    public Properties cloudDataSourceProperties() {
        return new Properties();
    }

    @Bean
    public Slf4jLogFilter logFilter(){
        Slf4jLogFilter filter = new Slf4jLogFilter();
        // 链接的时候
        filter.setConnectionLogEnabled(false);
        filter.setDataSourceLogEnabled(false);
        filter.setResultSetLogEnabled(false);
        // SQL格式化
        filter.setStatementSqlPrettyFormat(true);
        // 执行的时候
        filter.setStatementExecutableSqlLogEnable(true);
        // 执行之后
        filter.setStatementExecuteAfterLogEnabled(false);
        // 执行参数打印
        filter.setStatementParameterSetLogEnabled(false);
        // 执行之前-准备工作之后
        filter.setStatementPrepareAfterLogEnabled(false);
        // 执行之前-准备工作之后
        filter.setStatementPrepareCallAfterLogEnabled(false);
        // 关闭的时候
        filter.setStatementCloseAfterLogEnabled(false);
        filter.setConnectionCloseAfterLogEnabled(false);
        return filter;
    }

    @Bean(name = "cloudDataSource")
    public DataSource cloudDataSource(@Autowired @Qualifier("cloudDataSourceProperties") Properties properties) throws SQLException {
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setDbType(properties.getProperty("dbType"));
        dataSource.setUsername(properties.getProperty("username"));
        dataSource.setPassword(properties.getProperty("password"));
        dataSource.setUrl(properties.getProperty("url"));
        dataSource.setDriverClassName(properties.getProperty("driver-class-name"));
        dataSource.setMaxActive(Integer.valueOf((String) properties.get("maxActive")));
        dataSource.setMinIdle(Integer.valueOf((String) properties.get("minIdle")));
        dataSource.setInitialSize(Integer.valueOf((String) properties.get("initialSize")));
        dataSource.setMaxWait(Integer.parseInt(properties.get("maxWait").toString()));
        dataSource.setProxyFilters(Lists.newArrayList(logFilter()));
        dataSource.setFilters("stat");
        return dataSource;
    }

    @Bean(name = "cloudSqlSessionFactory")
    public SqlSessionFactory cloudSqlSessionFactory(@Qualifier("cloudDataSource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean bean = new SqlSessionFactoryBean();
        bean.setDataSource(dataSource);
        sqlJointInterceptor.setMethodPrefix("remoteQuery");
        sqlJointInterceptor.setMethodSuffix("ForSql");
        bean.setPlugins(new Interceptor[]{sqlJointInterceptor});
        // mybatis的配置  替代mybatis-config.xml文件
        org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();
        configuration.setMapUnderscoreToCamelCase(true);
        bean.setConfiguration(configuration);
        return bean.getObject();
    }

    @Bean
    public SqlSessionTemplate cloudSqlSessionTemplate(@Qualifier("cloudSqlSessionFactory") SqlSessionFactory sessionFactory) {
        return new SqlSessionTemplate(sessionFactory);
    }
}
