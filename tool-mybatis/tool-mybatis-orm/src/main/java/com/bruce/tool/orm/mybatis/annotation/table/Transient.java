package com.bruce.tool.orm.mybatis.annotation.table;

import java.lang.annotation.*;

/**
 * 功能 :
 * 忽略指定字段
 * @author : Bruce(刘正航) 上午11:44 2018/4/10
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
@Documented
@Inherited
public @interface Transient {
}