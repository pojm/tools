package com.bruce.tool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 功能 :
 * @author : Bruce(刘正航) 22:22 2019-09-17
 */
@SpringBootApplication(scanBasePackages = {"com.bruce.tool"})
public class OrmApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrmApplication.class, args);
    }
}
