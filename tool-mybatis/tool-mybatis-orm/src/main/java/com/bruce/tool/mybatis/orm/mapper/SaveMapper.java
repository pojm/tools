package com.bruce.tool.mybatis.orm.mapper;

import com.bruce.tool.mybatis.orm.provider.SaveProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 功能 :
 * mybatis保存数据接口
 * 1.save(domain)
 * 2.saveAll(List<domain> domains)
 * 3.saveBySQL(sql)
 * @author : Bruce(刘正航) 21:14 2019-01-20
 */
public interface SaveMapper<T> {
    @InsertProvider(type = SaveProvider.class, method = "save")
    @Options(useGeneratedKeys=true)
    Integer save(T obj);

    @InsertProvider(type = SaveProvider.class, method = "saveAll")
    Integer saveAll(@Param("list") List<T> list);

    @InsertProvider(type = SaveProvider.class, method = "saveBySQL")
    Integer saveBySQL(@Param("sql") String sql);
}
