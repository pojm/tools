package com.bruce.tool.mybatis.orm.mapper;

import com.bruce.tool.mybatis.orm.provider.FindProvider;
import com.bruce.tool.mybatis.orm.query.CriteriaQuery;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.SelectProvider;

import java.util.List;
import java.util.Map;

/**
 * 功能 :
 * mybatis基础查询接口
 * 1.findById(domain)
 * 2.findById(Class,id)
 * 3.findByColumn(Class,key,value)
 * 4.findsByColumn(Class,key,value)
 * 5.findByColumns(domain) //domain中有的值,都算查询条件,可以有多个字段,可以只有一个字段
 * 6.findsByColumns(domain) //domain中有的值,都算查询条件,可以有多个字段,可以只有一个字段
 * 7.findBySQL(sql)
 *
 * 8.findsByCriteria(Class,CriteriaQuery)
 * 9.findColumnsByCriteria(column,table,CriteriaQuery)
 * 10.findTablesByCriteria(sql,CriteriaQuery)
 * 11.findsBySQL(sql)
 *
 * @author : Bruce(刘正航) 17:01 2019-01-20
 */
public interface FindMapper<T> {
    /**
     * 基础查询方法, 根据主键查询一条记录
     */
    @SelectProvider(type = FindProvider.class, method = "findByDomainId")
    T findByDomainId(T domain);

    /**
     * 基础查询方法, 根据主键查询一条记录
     */
    @SelectProvider(type = FindProvider.class, method = "findById")
    T findById(@Param("clazz") Class<?> clazz,@Param("id") Object id);

    /**
     * 基础查询方法, 根据传入字段查询一条记录
     */
    @SelectProvider(type = FindProvider.class, method = "findByColumn")
    T findByColumn(@Param("clazz") Class<?> clazz,@Param("column") String key,@Param("value") Object value);

    /**
     * 基础查询方法, 根据多个传入字段查询一条记录
     */
    @SelectProvider(type = FindProvider.class, method = "findByColumns")
    T findByColumns(T domain);

    /**
     * 基础查询方法,根据条件,查询多条记录,自定义返回字段和表,可以用在多表联查上,可用join left join等
     */
    @SelectProvider(type = FindProvider.class, method = "findByColumnsTableCriteria")
    Map findByColumnsTableCriteria(@Param("columns") String columns,@Param("table") String table, @Param("criteria") CriteriaQuery criteria);

    /**
     * 基础查询方法,根据条件,查询多条记录,自定义返回字段和表,可以用在多表联查上,可用join left join等
     */
    @SelectProvider(type = FindProvider.class, method = "findBySQLCriteria")
    Map findBySQLCriteria(@Param("sql") String sql, @Param("criteria") CriteriaQuery criteria);

    /**
     * 基础查询方法,根据条件,查询多条记录,自定义返回字段和表,可以用在多表联查上,可用join left join等
     */
    @SelectProvider(type = FindProvider.class, method = "findBySQL")
    Map findBySQL(@Param("sql") String sql);

    /**
     * 基础查询方法, 根据传入多个字段查询一条记录
     */
    @SelectProvider(type = FindProvider.class, method = "findByColumn")
    List<T> findsByColumn(@Param("clazz") Class<?> clazz, @Param("column") String column, @Param("value") Object value);

    /**
     * 基础查询方法, 根据传入多个字段,查询多条记录
     */
    @SelectProvider(type = FindProvider.class, method = "findByColumns")
    List<T> findsByColumns(T domain);

    /**
     * 根据表主键集合,查询多条数据,没有主键的表,无法调用此方法
     */
    @SelectProvider(type = FindProvider.class, method = "findsByIds")
    List<T> findsByIds(@Param("clazz") Class<?> clazz, @Param("values") List<Object> values);

    /**
     * 基础查询方法, 根据传入多个字段,查询多条记录
     */
    @SelectProvider(type = FindProvider.class, method = "findsByColumnCollection")
    List<T> findsByColumnCollection(@Param("clazz") Class<?> clazz, @Param("values") List<Object> values, @Param("column") String column);

    /**
     * 基础查询方法,根据条件,查询多条记录
     */
    @SelectProvider(type = FindProvider.class, method = "findsByCriteria")
    List<T> findsByCriteria(@Param("clazz") Class<T> clazz, @Param("criteria") CriteriaQuery criteria);

    /**
     * 基础查询方法,根据条件,查询多条记录,自定义返回字段
     */
    @SelectProvider(type = FindProvider.class, method = "findsByColumnsTableCriteria")
    List<Map> findsByColumnsTableCriteria(@Param("columns") String columns, @Param("table") String table, @Param("criteria") CriteriaQuery criteria);

    /**
     * 基础查询方法,根据条件,查询多条记录,自定义返回字段和表,可以用在多表联查上
     */
    @SelectProvider(type = FindProvider.class, method = "findBySQLCriteria")
    List<Map> findsBySQLCriteria(@Param("sql") String sql, @Param("criteria") CriteriaQuery criteria);

    /**
     * 基础查询方法,根据条件,查询多条记录,自定义返回字段和表,可以用在多表联查上,可用join left join等
     */
    @SelectProvider(type = FindProvider.class, method = "findBySQL")
    List<Map> findsBySQL(@Param("sql") String sql);
}
