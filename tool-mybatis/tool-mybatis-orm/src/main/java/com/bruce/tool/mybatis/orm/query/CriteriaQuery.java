package com.bruce.tool.mybatis.orm.query;

import com.google.common.collect.Lists;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 22:51 2019-01-22
 */
@NoArgsConstructor(staticName = "create")
public class CriteriaQuery {

    @Getter(value = AccessLevel.PUBLIC)
    private List<String> ands = Lists.newArrayList();
    @Getter(value = AccessLevel.PUBLIC)
    private List<String> ors = Lists.newArrayList();
    @Getter(value = AccessLevel.PUBLIC)
    private List<String> groupBys = Lists.newArrayList();
    @Getter(value = AccessLevel.PUBLIC)
    private List<String> orderBys = Lists.newArrayList();
    @Getter(value = AccessLevel.PUBLIC)
    private List<String> limits = Lists.newArrayList();

    private And and;
    private Or or;

    public And and(){
        if (Objects.isNull(this.and)) {
            this.and = this.new And(this);
        }
        return this.and;
    }

    public Or or(){
        if (Objects.isNull(this.or)) {
            this.or = this.new Or(this);
        }
        return this.or;
    }

    public CriteriaQuery groupBy(String sql){
        groupBys.add(sql);
        return this;
    }

    public CriteriaQuery orderBy(String sql){
        orderBys.add(sql);
        return this;
    }

    public CriteriaQuery limit(String sql){
        limits.add(sql);
        return this;
    }

    public class And {

        private CriteriaQuery query;

        private And(CriteriaQuery query){
            this.query = query;
        }

        public CriteriaQuery where(String sql) {
            ands.add(sql);
            return this.query;
        }

    }

    public class Or {

        private CriteriaQuery query;

        private Or(CriteriaQuery query){
            this.query = query;
        }

        public CriteriaQuery where(String sql) {
            ors.add(sql);
            return this.query;
        }

    }

}
