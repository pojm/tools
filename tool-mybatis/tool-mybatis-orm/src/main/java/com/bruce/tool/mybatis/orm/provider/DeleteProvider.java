package com.bruce.tool.mybatis.orm.provider;

import com.bruce.tool.common.exception.BaseRuntimeException;
import com.bruce.tool.mybatis.orm.constant.SQLCode;
import com.bruce.tool.mybatis.orm.util.ColumnUtils;
import com.bruce.tool.mybatis.orm.util.TableUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 00:45 2019-01-23
 */
public class DeleteProvider {

    private static final String ID_PLACEHOLDER = " = #{id}";
    public static final String BEGIN_PLACEHOLDER = " = #{";
    public static final String END_PLACEHOLDER = "}";

    /**
     * 根据ID查询一条记录
     */
    public String deleteByDomainId(Object domain) {
        Class<?> clazz = domain.getClass();
        if( Objects.isNull(clazz) ){
            throw new BaseRuntimeException(SQLCode.FE004.getCode(),"查询异常:传入类为空");
        }
        String tableName = TableUtils.getTableName(clazz);
        String primary = TableUtils.getPrimaryKey(clazz);
        if(com.bruce.tool.common.util.string.StringUtils.isBlank(primary) ){
            throw new BaseRuntimeException(SQLCode.FE001.getCode(),"查询异常:当前表无主键,不能使用findById(domain)方法查询");
        }

        SQL sql = new SQL();
        sql.DELETE_FROM(tableName);
        sql.AND().WHERE(primary + BEGIN_PLACEHOLDER +primary+ END_PLACEHOLDER);
        return sql.toString();
    }


    public String deleteById(Map<String, Object> params) {
        Class clazz = getParamClass(params);
        String tableName = TableUtils.getTableName(clazz);
        String id = TableUtils.getPrimaryKey(clazz);
        SQL sql = new SQL();
        sql.DELETE_FROM(tableName);
        sql.WHERE(id + ID_PLACEHOLDER);
        return sql.toString();
    }

    public String deleteByColumn(Map<String, Object> params) {
        Class clazz = getParamClass(params);
        String column = (String) params.get("column");
        String tableName = TableUtils.getTableName(clazz);
        SQL sql = new SQL();
        sql.DELETE_FROM(tableName);
        sql.WHERE(column + BEGIN_PLACEHOLDER + "value" + END_PLACEHOLDER);
        return sql.toString();
    }

    public String deleteByColumns(Object domain) {
        if( Objects.isNull(domain) ){
            throw new BaseRuntimeException(SQLCode.FE004.getCode(),"查询异常:传入对象为空,终止查询");
        }
        Class<?> clazz = domain.getClass();
        String tableName = TableUtils.getTableName(clazz);
        List<Field> fields = TableUtils.getFieldsWithValue(domain);
        if(CollectionUtils.isEmpty(fields)){
            throw new BaseRuntimeException(SQLCode.FE003.getCode(),"查询异常:传入对象的所有字段都没有值,无法完成查询");
        }
        SQL sql = new SQL();
        sql.DELETE_FROM(tableName);
        for (Field field : fields) {
            sql.AND().WHERE(ColumnUtils.fetchColumnName(field) + BEGIN_PLACEHOLDER + field.getName() + END_PLACEHOLDER);
        }
        return sql.toString();
    }

    public String deleteBySQL(Map<String,Object> params){
        String sql = (String) params.get("sql");
        if( com.bruce.tool.common.util.string.StringUtils.isBlank(sql) ){
            throw new BaseRuntimeException(SQLCode.FE004.getCode(),"查询异常:传入字段和表拼接为空");
        }
        return sql;
    }

    public String markDeleteById(Map<String, Object> params) {
        Class clazz = getParamClass(params);
        String tableName = TableUtils.getTableName(clazz);
        String id = TableUtils.getPrimaryKey(clazz);
        SQL sql = new SQL();
        sql.UPDATE(tableName);
        sql.SET("is_delete = "+ System.currentTimeMillis());
        sql.WHERE(id + ID_PLACEHOLDER);
        return sql.toString();
    }

    public String markDeleteByIdDiy(Map<String, Object> params) {
        Class clazz = getParamClass(params);
        String mark = (String) params.get("mark");
        if(StringUtils.isBlank(mark) ){
            throw new BaseRuntimeException(SQLCode.FE004.getCode(),"标记删除字段不能为空!");
        }
        String tableName = TableUtils.getTableName(clazz);
        String id = TableUtils.getPrimaryKey(clazz);
        SQL sql = new SQL();
        sql.UPDATE(tableName);
        sql.SET("is_delete = "+ System.currentTimeMillis());
        sql.WHERE(id + ID_PLACEHOLDER);
        return sql.toString();
    }

    private Class getParamClass(Map<String, Object> params) {
        return (Class) params.get("clazz");
    }
}
