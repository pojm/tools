package com.bruce.tool.mybatis.orm.mapper;

import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;

/**
 * 功能 :
 * mybatis删除数据功能
 * 1.deleteById(id)
 * 2.deleteByColumn(domain) //domain中有的值,都作为条件
 * 3.deleteByColumn(Class,key,value)
 * 4.deleteBySQL(sql)
 * 5.markDeleteById(id)
 * 6.markDeleteById(id,mark)
 * @author : Bruce(刘正航) 21:19 2019-01-20
 */
public interface DeleteMapper<T> {

    /**根据domain值,删除数据**/
    @DeleteProvider(type = com.bruce.tool.mybatis.orm.provider.DeleteProvider.class, method = "deleteByDomainId")
    Integer deleteByDomainId(T domain);

    /**
     * 物理删除一条记录
     */
    @DeleteProvider(type = com.bruce.tool.mybatis.orm.provider.DeleteProvider.class, method = "deleteById")
    Integer deleteById(@Param("clazz") Class<T> clazz, @Param("id") Object id);

    /**
     * 根据指定字段,指定的值,物理删除一条记录
     */
    @DeleteProvider(type = com.bruce.tool.mybatis.orm.provider.DeleteProvider.class, method = "deleteByColumn")
    Integer deleteByColumn(@Param("clazz") Class<T> clazz, @Param("column") String column, @Param("value") Object value);

    /**根据domain中有值的字段,删除记录**/
    @DeleteProvider(type = com.bruce.tool.mybatis.orm.provider.DeleteProvider.class, method = "deleteByColumns")
    Integer deleteByColumns(T domain);

    /**使用纯sql执行删除语句**/
    @DeleteProvider(type = com.bruce.tool.mybatis.orm.provider.DeleteProvider.class, method = "deleteBySQL")
    Integer deleteBySQL(@Param("sql") String sql);

    /**
     * 逻辑删除一条记录(默认标记字段:is_delete)
     */
    @UpdateProvider(type = com.bruce.tool.mybatis.orm.provider.DeleteProvider.class, method = "markDeleteById")
    Integer markDeleteById(@Param("clazz") Class<T> clazz, @Param("id") Object id);

    /**
     * 逻辑删除一条记录(自定义标记字段)
     */
    @UpdateProvider(type = com.bruce.tool.mybatis.orm.provider.DeleteProvider.class, method = "markDeleteByIdDiy")
    Integer markDeleteByIdDiy(@Param("clazz") Class<T> clazz, @Param("id") Object id,@Param("mark") String mark);

}
