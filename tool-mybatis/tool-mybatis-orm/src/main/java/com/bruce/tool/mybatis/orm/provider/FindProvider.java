package com.bruce.tool.mybatis.orm.provider;

import com.bruce.tool.common.util.ClassUtils;
import com.bruce.tool.common.util.string.StringUtils;
import com.bruce.tool.mybatis.orm.exception.OrmNullException;
import com.bruce.tool.mybatis.orm.query.CriteriaQuery;
import com.bruce.tool.mybatis.orm.util.ColumnUtils;
import com.bruce.tool.mybatis.orm.util.TableUtils;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 功能 :
 * mybatis基础查询接口
 * 1.findByDomainId(domain)
 * 2.findById(Class,id)
 * 3.findByColumn(Class,key,value)
 * 4.findsByColumn(Class,key,value)
 * 5.findByColumns(domain) //domain中有的值,都算查询条件,可以有多个字段,可以只有一个字段
 * 6.findsByColumns(domain) //domain中有的值,都算查询条件,可以有多个字段,可以只有一个字段
 *
 * 7.findsByCriteria(Class,CriteriaQuery)
 * 8.findsByCriteria(column,table,CriteriaQuery)
 * 9.findsByCriteria(column,table,CriteriaQuery)
 * 10.findsByCriteria(sql,CriteriaQuery)
 * 11.findsByCriteria(sql)
 *
 * @author : Bruce(刘正航) 17:01 2019-01-20
 */
public class FindProvider {

    private static final String ID_PLACEHOLDER = " = #{id}";
    public static final String BEGIN_PLACEHOLDER = " = #{";
    public static final String END_PLACEHOLDER = "}";

    /**
     * 根据ID查询一条记录
     */
    public String findByDomainId(Object domain) {
        Class<?> clazz = domain.getClass();
        if( Objects.isNull(clazz) ){
            throw new OrmNullException();
        }
        String tableName = TableUtils.getTableName(clazz);
        String columnMapping = TableUtils.getColumnMapping(clazz);
        String primary = TableUtils.getPrimaryKey(clazz);
        if(StringUtils.isBlank(primary) ){
            throw new OrmNullException("查询异常:当前表无主键,不能使用findById(domain)方法查询");
        }
        SQL sql = new SQL();
        sql.SELECT(columnMapping);
        sql.FROM(tableName);
        sql.AND().WHERE(primary + BEGIN_PLACEHOLDER +primary + END_PLACEHOLDER);
        return sql.toString();
    }

    /**
     * 根据ID查询一条记录
     */
    public String findById(Map<String, Object> params) {
        Class<?> clazz = getParamClass(params);
        if( Objects.isNull(clazz) ){
            throw new OrmNullException();
        }
        String tableName = TableUtils.getTableName(clazz);
        String columnMapping = TableUtils.getColumnMapping(clazz);
        String primary = TableUtils.getPrimaryKey(clazz);
        if(StringUtils.isBlank(primary) ){
            throw new OrmNullException("查询异常:当前表无主键,不能使用findById(domain)方法查询");
        }
        SQL sql = new SQL();
        sql.SELECT(columnMapping);
        sql.FROM(tableName);
        sql.AND().WHERE(primary + ID_PLACEHOLDER);
        return sql.toString();
    }

    /**
     * 根据ID查询一条记录
     */
    public String findByColumn(Map<String, Object> params) {
        Class<?> clazz = getParamClass(params);
        if( Objects.isNull(clazz) ){
            throw new OrmNullException();
        }
        String tableName = TableUtils.getTableName(clazz);
        String columnMapping = TableUtils.getColumnMapping(clazz);
        String column = (String) params.get("column");
        if( StringUtils.isBlank(column) ){
            throw new OrmNullException("查询异常:传入键值为空");
        }
        SQL sql = new SQL();
        sql.SELECT(columnMapping);
        sql.FROM(tableName);
        sql.AND().WHERE(ColumnUtils.fetchColumnName(column) + BEGIN_PLACEHOLDER + "value" + END_PLACEHOLDER);
        return sql.toString();
    }

    /**
     * 根据domain中的条件,查询数据
     */
    public String findByColumns(Object domain) {
        if( Objects.isNull(domain) ){
            throw new OrmNullException();
        }
        Class<?> clazz = domain.getClass();
        String tableName = TableUtils.getTableName(clazz);
        String columnMapping = TableUtils.getColumnMapping(clazz);
        List<Field> fields = TableUtils.getFieldsWithValue(domain);
        if(CollectionUtils.isEmpty(fields)){
            throw new OrmNullException("查询异常:传入对象的所有字段都没有值,无法完成查询");
        }
        SQL sql = new SQL();
        sql.SELECT(columnMapping);
        sql.FROM(tableName);
        for (Field field : fields) {
            sql.AND().WHERE(ColumnUtils.fetchColumnName(field) + BEGIN_PLACEHOLDER +field.getName() + END_PLACEHOLDER);
        }
        return sql.toString();
    }

    /**
     * 根据主键集合,查询数据结果集
     */
    public String findsByIds(Map<String, Object> params){
        Class<?> clazz = getParamClass(params);
        if( Objects.isNull(clazz) ){
            throw new OrmNullException();
        }

        String tableName = TableUtils.getTableName(clazz);
        String columnMapping = TableUtils.getColumnMapping(clazz);
        Field primary = TableUtils.getPrimary(clazz);
        String primaryKey = TableUtils.getPrimaryKey(clazz);
        if( Objects.isNull(primary) ){
            throw new OrmNullException("查询异常:传入字段没有对应的数据库表字段,终止查询");
        }
        SQL sql = new SQL();
        sql.SELECT(columnMapping);
        sql.FROM(tableName);
        List<Object> values = (List<Object>) params.get("values");
        if(CollectionUtils.isEmpty(values)){
            sql.AND().WHERE("1!=1");
        } else {
            if( ClassUtils.isStringType(primary.getType()) ){
                sql.AND().WHERE(primaryKey + " in ('"+StringUtils.join(values,"','")+"') ");
            }else if(ClassUtils.isNumberType(primary.getType())){
                sql.AND().WHERE(primaryKey + " in ("+StringUtils.join(values,",")+") ");
            }else{
                sql.AND().WHERE("1!=1");
            }
        }
        return sql.toString();
    }

    /**
     * 根据主键集合,查询数据结果集,只支持Integer或者String的集合
     */
    public String findsByColumnCollection(Map<String, Object> params){
        Class<?> clazz = getParamClass(params);
        if( Objects.isNull(clazz) ){
            throw new OrmNullException();
        }

        String tableName = TableUtils.getTableName(clazz);
        String columnMapping = TableUtils.getColumnMapping(clazz);
        String column = (String) params.get("column");
        if( StringUtils.isBlank(column) ){
            throw new OrmNullException("查询异常:传入字段名为空,终止查询");
        }
        Field field = TableUtils.getFieldByColumn(clazz,column);
        if( Objects.isNull(field) ){
            throw new OrmNullException("查询异常:传入字段没有对应的数据库表字段,终止查询");
        }
        SQL sql = new SQL();
        sql.SELECT(columnMapping);
        sql.FROM(tableName);
        List<Object> values = (List<Object>) params.get("values");
        if(CollectionUtils.isEmpty(values)){
            sql.AND().WHERE("1!=1");
        } else {
            if( ClassUtils.isStringType(field.getType()) ){
                sql.AND().WHERE(column + " in ('"+StringUtils.join(values,"','")+"') ");
            }else if(ClassUtils.isNumberType(field.getType())){
                sql.AND().WHERE(column + " in ("+StringUtils.join(values,",")+") ");
            }else{
                sql.AND().WHERE("1!=1");
            }
        }
        return sql.toString();
    }

    /**
     * 自行封装的多条件查询
     */
    public String findByColumnsTableCriteria(Map<String, Object> params) {
        String columns = (String) params.get("columns");
        if( StringUtils.isBlank(columns) ){
            throw new OrmNullException("查询异常:传入字段为空,终止查询");
        }
        String table = (String) params.get("table");
        if( StringUtils.isBlank(table) ){
            throw new OrmNullException("查询异常:传入表明为空,终止查询");
        }
        CriteriaQuery criteria = getCriteriaQuery(params);
        SQL sql = new SQL();
        sql.SELECT(columns);
        sql.FROM(table);
        appendConditions(criteria, sql);
        List<String> sqls = criteria.getLimits();
        if(CollectionUtils.isEmpty(sqls)){
            return sql.toString();
        }
        return sql.toString() + " limit " + sqls.get(0);
    }

    /**
     * 自行封装的多条件查询
     */
    public String findsByCriteria(Map<String, Object> params) {
        Class clazz = getParamClass(params);
        if( Objects.isNull(clazz) ){
            throw new OrmNullException("查询异常:传入类为空,终止查询");
        }
        CriteriaQuery criteria = getCriteriaQuery(params);
        String tableName = TableUtils.getTableName(clazz);
        String columnMapping = TableUtils.getColumnMapping(clazz);
        SQL sql = new SQL();
        sql.SELECT(columnMapping);
        sql.FROM(tableName);
        appendConditions(criteria, sql);
        List<String> sqls = criteria.getLimits();
        if(CollectionUtils.isEmpty(sqls)){
            return sql.toString();
        }
        return sql.toString() + " limit " + sqls.get(0);
    }

    /**
     * 自行封装的多条件查询
     */
    public String findsByColumnsTableCriteria(Map<String, Object> params) {
        return this.findByColumnsTableCriteria(params);
    }

    /**
     * 自行封装的多条件查询
     */
    public String findBySQLCriteria(Map<String, Object> params) {
        StringBuilder sql = new StringBuilder((String) params.get("sql"));
        if( StringUtils.isBlank(sql) ){
            throw new OrmNullException("查询异常:传入字段和表拼接为空");
        }
        sql.append(" where true ");
        CriteriaQuery criteria = getCriteriaQuery(params);
        for (String bsql : criteria.getAnds()) {
            sql.append(" and (" + bsql + ")");
        }
        for (String bsql : criteria.getOrs()) {
            sql.append(" or (" + bsql + ")");
        }
        for (String bsql : criteria.getOrderBys()) {
            sql.append(" order by " + bsql);
        }
        for (String bsql : criteria.getGroupBys()) {
            sql.append(" group by " + bsql);
        }
        return sql.toString();
    }

    /**
     * 根据sql查询
     */
    public String findBySQL(Map<String, Object> params){
        String sql = (String) params.get("sql");
        if( StringUtils.isBlank(sql) ){
            throw new OrmNullException("查询异常:传入字段和表拼接为空");
        }
        return sql;
    }

    private void appendConditions(CriteriaQuery criteria, SQL sql) {
        for (String sqls : criteria.getAnds()) {
            sql.AND().WHERE(sqls);
        }
        for (String sqls : criteria.getOrs()) {
            sql.OR().WHERE(sqls);
        }
        for (String sqls : criteria.getOrderBys()) {
            sql.ORDER_BY(sqls);
        }
        for (String sqls : criteria.getGroupBys()) {
            sql.GROUP_BY(sqls);
        }
    }

    private Class<?> getParamClass(Map<String, Object> params) {
        return (Class<?>) params.get("clazz");
    }

    private CriteriaQuery getCriteriaQuery(Map<String, Object> params) {
        CriteriaQuery criteria = (CriteriaQuery) params.get("criteria");
        if (Objects.isNull(criteria)) {
            throw new OrmNullException("查询异常:传入条件对象为空,终止查询");
        }
        return criteria;
    }
}
