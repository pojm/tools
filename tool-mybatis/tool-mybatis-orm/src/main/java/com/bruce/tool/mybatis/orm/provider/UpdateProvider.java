package com.bruce.tool.mybatis.orm.provider;

import com.bruce.tool.common.exception.BaseRuntimeException;
import com.bruce.tool.common.util.string.StringUtils;
import com.bruce.tool.mybatis.orm.builder.BatchUpdateSQLBuilder;
import com.bruce.tool.mybatis.orm.constant.SQLCode;
import com.bruce.tool.mybatis.orm.util.ColumnUtils;
import com.bruce.tool.mybatis.orm.util.TableUtils;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 00:36 2019-01-23
 */
public class UpdateProvider {

    /**
     * 更新一条记录,保存空值
     */
    public String update(Object domain) {
        return update(domain,true);
    }

    /**
     * 更新一条记录,不保存空值
     */
    public String updateBySelective(Object domain) {
        return update(domain,false);
    }

    /**
     * 批量更新记录
     */
    public String updateAll(Map<String, Object> params) {
        List list = (List) params.get("list");
        if(CollectionUtils.isEmpty(list)){
            return null;
        }
        Object item = list.get(0);
        Class clazz = item.getClass();
        String tableName = TableUtils.getTableName(clazz);
        String primaryKey = TableUtils.getPrimaryKey(clazz);
        BatchUpdateSQLBuilder builder = new BatchUpdateSQLBuilder();
        builder.setUpdateTable(tableName, primaryKey);
        builder.setData(list);
        return builder.toString();
    }

    public String updateBySQL(Map<String,Object> params){
        String sql = (String) params.get("sql");
        if( StringUtils.isBlank(sql) ){
            throw new BaseRuntimeException(SQLCode.FE004.getCode(),"查询异常:传入字段和表拼接为空");
        }
        return sql;
    }

    /*
     ****************************************私有方法区*******************************************
                   _               _                           _    _                 _
                  (_)             | |                         | |  | |               | |
      _ __   _ __  _ __   __ __ _ | |_  ___   _ __ ___    ___ | |_ | |__    ___    __| |
     | '_ \ | '__|| |\ \ / // _` || __|/ _ \ | '_ ` _ \  / _ \| __|| '_ \  / _ \  / _` |
     | |_) || |   | | \ V /| (_| || |_|  __/ | | | | | ||  __/| |_ | | | || (_) || (_| |
     | .__/ |_|   |_|  \_/  \__,_| \__|\___| |_| |_| |_| \___| \__||_| |_| \___/  \__,_|
     | |
     |_|
     ****************************************私有方法区*******************************************
     */

    /**更新语句**/
    private String update(Object domain,boolean saveNull) {
        Class<?> beanClass = domain.getClass();
        String tableName = TableUtils.getTableName(beanClass);
        List<Field> fields = TableUtils.getFields(beanClass);
        StringBuilder updateSql = new StringBuilder();
        updateSql.append("UPDATE ").append(tableName).append(" set ");
        try {
            Field primaryKey = null;
            String primaryColumn = null;
            for (Field field : fields){
                String columnName = ColumnUtils.fetchColumnName(field);
                if(ColumnUtils.isPrimaryKey(field)){
                    primaryKey = field;
                    primaryColumn = columnName;
                    continue;
                }
                field.setAccessible(true);
                Object object = field.get(domain);
                if( Objects.nonNull(object) ){
                    updateSql.append(columnName).append("=").append("#{" + field.getName() + "}").append(",");
                    continue;
                }
                if( saveNull ){
                    updateSql.append(columnName).append("=").append("#{" + field.getName() + "}").append(",");
                }
            }
            if( updateSql.toString().endsWith(",") ){
                updateSql = updateSql.replace(updateSql.length()-1,updateSql.length(),"");
            }
            if( Objects.isNull(primaryKey) ){
                throw new BaseRuntimeException(SQLCode.FE004.getCode(),"要更新的表,缺少主键,无法通过此方法完成数据更新");
            }
            updateSql.append(" where ").append(primaryColumn).append("=").append("#{" + primaryKey.getName() + "}");
        } catch (Exception e) {
            throw new BaseRuntimeException(SQLCode.FE004.getCode(),"get update sql is exceptoin:" + e);
        }
        return updateSql.toString();
    }
}
