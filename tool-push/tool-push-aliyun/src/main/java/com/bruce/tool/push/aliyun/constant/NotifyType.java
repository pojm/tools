package com.bruce.tool.push.aliyun.constant;

import lombok.Getter;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 15:04 2019-01-25
 */
public enum NotifyType {
    NONE("静音"),
    VIBRATE("震动"),
    SOUND("声音"),
    BOTH("声音和震动"),
    ;
    @Getter
    private String name;

    NotifyType(String name) {
        this.name = name;
    }

}
