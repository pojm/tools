package com.bruce.tool.push.aliyun.constant;

import lombok.Getter;

/**
 * 功能 :
 * 推送异常
 * @author : Bruce(刘正航) 16:01 2019-01-25
 */
public enum PushErrorCode {
    PE0001("PE0001","推送配置异常"),

    ;
    @Getter
    private String code;
    @Getter
    private String message;

    PushErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

}
