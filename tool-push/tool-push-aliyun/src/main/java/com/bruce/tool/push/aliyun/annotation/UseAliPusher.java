package com.bruce.tool.push.aliyun.annotation;

import com.bruce.tool.push.aliyun.config.AliPusherRegister;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 09:27 2019-01-31
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(AliPusherRegister.class)
public @interface UseAliPusher {
}
