package com.bruce.tool.push.aliyun.core;

import com.bruce.tool.push.aliyun.constant.Env;
import com.bruce.tool.push.aliyun.constant.MessageType;
import com.bruce.tool.push.aliyun.constant.NotifyType;
import com.bruce.tool.push.aliyun.constant.OpenType;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 11:26 AM 2018/12/3
 */
@Data
public class Message {

    /**消息类型 MESSAGE NOTICE**/
    @NotNull(message = "推送类型不能为空")
    private MessageType messageType;
    /**消息标题**/
    @NotNull(message = "推送消息标题不能为空")
    private String title;
    /**消息内容**/
    @NotNull(message = "推送消息内容不能为空")
    private String message;
    /**
     * 推送目标
     * 根据Target来设定，如Target=device, 则对应的值为 设备id1,设备id2. 多个值使用逗号分隔.(帐号与设备有一次最多100个的限制)
     */
    private String targetValue;
    /**消息有效期**/
    private Long expire;
    /**延迟推送配置**/
    private Long delay;
    /**离线是否保存消息**/
    private Boolean storeOffline = true;

    ////////////////////ios相关配置//////////////////////////

    /**"DEV","PRODUCT"**/
    private Env iOSApnsEnv = Env.DEV;
    private Integer iOSBadge = 5;
    private String iOSMusic = "default";
    private String iOSSubTitle = "iOS10 subtitle";
    // 扩展属性,必须是key/value的json格式,"{\"_ENV_\":\"DEV\",\"k2\":\"v2\"}"
    //通知的扩展属性(注意 : 该参数要以json map的格式传入,否则会解析出错)

    private String iOSExt;

    ////////////////////Android相关配置//////////////////////////
    /**android设备,通知方式:声音/震动**/
    private NotifyType notifyType = NotifyType.NONE;
    /**android音乐**/
    private String androidMusic = "default";
    /**通知栏自定义样式0-100**/
    private Integer notificationBarType = 1;
    /**通知栏自定义样式0-100**/
    private Integer notificationBarPriority = 1;

    @NotNull(message = "Android打开类型不能为空")
    private OpenType openType = OpenType.NONE;
    private String androidActivity;
    private String androidUrl;

    /**扩展属性,必须是key/value的json格式**/
    private String androidExt;

    ////////////////////小米Activity单独属性//////////////////////////

    private String xiaomiActivity;
    private String xiaoMiNotifyTitle;
    private String xiaoMiNotifyBody;
}
