package com.bruce.tool.push.aliyun.constant;

import lombok.Getter;

/**
 * 功能 :
 * 推送目标: device:推送给设备; account:推送给指定帐号,tag:推送给自定义标签; all: 推送给全部
 * @author : Bruce(刘正航) 11:30 AM 2018/12/3
 */
public enum Target {

    DEVICE("device"),
    ACCOUNT("account"),
    ALIAS("alias"),
    TAG("tag"),
    ALL("all"),

    ;
    @Getter
    private String value;

    Target(String value) {
        this.value = value;
    }
}
