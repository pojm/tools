package com.bruce.tool.push.aliyun.config;

import com.bruce.tool.push.aliyun.constant.Env;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 11:20 AM 2018/12/3
 */
@Data
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "tool.pusher.aliyun")
public class AliyunPusherConfig {
    /**区域标识**/
    @NotNull(message = "区域标识必须配置")
    private String regionId;
    /**此处可以有多个appkey,用","分割**/
    @NotNull(message = "APP对应的key必须配置")
    private String appKeys;
    @NotNull(message = "推送的唯一标识必须配置")
    private String accessKeyId;
    @NotNull(message = "推送的秘钥必须配置")
    private String accessKeySecret;
    /**消息有效期**/
    private Long expire;
    /**延迟发送的时间:秒**/
    private Long delay;

    // ios配置
    private String iOSApnsEnv = Env.DEV.name();// "DEV","PRODUCT"

}
