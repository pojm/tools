package com.bruce.tool.push.aliyun.constant;

import lombok.Getter;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 11:33 AM 2018/12/3
 */
public enum DeviceType {
    IOS(0),
    ANDROID(1),
    ALL(3),
    ;
    @Getter
    private Integer value;

    DeviceType(Integer value) {
        this.value = value;
    }
}
