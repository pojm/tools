package com.bruce.tool.push.aliyun.constant;

import lombok.Getter;

/**
 * 功能 :
 * 消息类型 MESSAGE NOTICE
 * @author : Bruce(刘正航) 11:24 AM 2018/12/3
 */
public enum MessageType {
    MESSAGE(0,"消息"),
    NOTICE(1,"通知")
    ;

    @Getter
    private Integer value;
    @Getter
    private String desc;

    MessageType(Integer value, String desc) {
        this.value = value;
        this.desc = desc;
    }
}
