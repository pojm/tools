package com.bruce.tool.push.aliyun.constant;

import lombok.Getter;

/**
 * 功能 :
 * 点击通知后动作,1:打开应用 2: 打开应用Activity 3:打开 androidUrl 4 : 无跳转逻辑
 * @author : Bruce(刘正航) 11:50 AM 2018/12/3
 */
public enum OpenType {

    APPLICATION("1","打开应用"),
    ACTIVITY("2","打开AndroidActivity"),
    URL("3","打开url地址"),
    NONE("4","无跳转逻辑"),

    ;
    @Getter
    private String value;
    @Getter
    private String desc;

    OpenType(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }
}
