package com.bruce.tool.push.aliyun.config;

import org.springframework.context.annotation.ComponentScan;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 09:28 2019-01-31
 */
@ComponentScan(value = {"com.bruce.tool.push.aliyun"})
public class AliPusherRegister {
}
