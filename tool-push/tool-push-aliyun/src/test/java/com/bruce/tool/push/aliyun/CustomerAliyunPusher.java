package com.bruce.tool.push.aliyun;

import com.bruce.tool.common.util.LogUtils;
import com.bruce.tool.push.aliyun.core.AliyunPusher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 3:57 PM 2018/12/3
 */
@Slf4j
@Component
public class CustomerAliyunPusher extends AliyunPusher {
    @Override
    public Long getAppKey(List<String> keys) {
        if(CollectionUtils.isEmpty(keys)){
            LogUtils.error(log,"{}","缺少AppKey配置");
            return null;
        }
        return Long.valueOf(keys.get(0));
    }
}
