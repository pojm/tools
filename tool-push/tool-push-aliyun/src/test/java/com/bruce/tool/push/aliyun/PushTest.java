package com.bruce.tool.push.aliyun;

import com.bruce.tool.push.aliyun.constant.MessageType;
import com.bruce.tool.push.aliyun.constant.OpenType;
import com.bruce.tool.push.aliyun.core.Message;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 1:42 PM 2018/12/3
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@WebAppConfiguration
public class PushTest {

    @Autowired
    private CustomerAliyunPusher pusher;

    @Test
    public void push() {
        Message message = new Message();
        message.setMessageType(MessageType.NOTICE);
        message.setOpenType(OpenType.APPLICATION);
        message.setTitle("测试推送");
        message.setMessage("测试");
        Boolean result = pusher.push2device(message);
        System.out.println(result);
    }
}
