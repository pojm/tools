package com.bruce.tool.mail.constant;

import com.bruce.tool.common.exception.ErrorCode;
import com.bruce.tool.common.exception.I18N;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 12:58 2019-02-20
 */
@AllArgsConstructor
public enum MailErrorCode {
    //
    MAIL_ERROR("M0001","网络异常","network error"),

    ;
    @Getter
    private String code;
    @Getter
    private String message;
    @Getter
    /**英文错误信息**/
    private String messageENUS;

    public static String getMessageByCode(String code, I18N i18N){
        ErrorCode target = search(code);
        if( null == target ){
            return "";
        }
        return getMessageByLanguage(i18N,target.getMessage(),target.getMessageENUS());
    }

    public static ErrorCode search(String code){
        ErrorCode target = null;
        for (ErrorCode errorCode  : ErrorCode.values() ) {
            if( errorCode.getCode().equals(code) ){
                target = errorCode;
            }
        }
        return target;
    }

    public static String getMessageByLanguage(I18N i18N,String ...messages) {
        I18N[] ns = I18N.values();
        Integer size = messages.length;
        String message = messages[0];
        for ( int i = 0,length = ns.length;i<length;i++ ) {
            if( !i18N.equals(ns[i]) ){
                continue;
            }

            if( i < size - 1 ){
                message =  messages[i];
            }else{
                // 如果没有对应消息,默认返回中文
                message = messages[0];
            }
        }
        return message;
    }
}
