package com.bruce.tool.mail.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 6:34 PM 2018/12/8
 */
@Data
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "tool.mail")
public class MailConfig {

    private String host = "";
    private String username = "";
    private String password = "";
    /**收件人**/
    private String to = "";
    /**抄送人**/
    private String copy = "";
    /**密送人**/
    private String secret = "";

}
