package com.bruce.tool.mail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 上午12:49 2018/1/7
 */
@SpringBootApplication(scanBasePackages = {"com.bruce.tool.mail"})
public class NewEmailApplication {

    public static void main(String[] args) {
        SpringApplication.run(NewEmailApplication.class, args);
    }
}
