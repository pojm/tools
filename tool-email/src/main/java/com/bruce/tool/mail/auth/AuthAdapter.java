package com.bruce.tool.mail.auth;

import lombok.AllArgsConstructor;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * 邮箱登陆校验
 * @author : Bruce(刘正航) 上午14:10 2018/2/27
 */
@AllArgsConstructor
public class AuthAdapter extends Authenticator {

	private String username;
	private String password;

	@Override
	protected PasswordAuthentication getPasswordAuthentication() {
		return new PasswordAuthentication(username, password);
	}
}