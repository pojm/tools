package com.bruce.tool.mail.annotation;

import com.bruce.tool.mail.config.EmailRegister;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 11:24 2019-02-14
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(EmailRegister.class)
public @interface UseEmail {
}
