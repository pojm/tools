package com.bruce.tool.mail.constant;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 6:42 PM 2018/12/8
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constants {

    /**
     * 收件人分隔符
     */
    public static final String SPLIT_SEPARATOR = ";";
}
