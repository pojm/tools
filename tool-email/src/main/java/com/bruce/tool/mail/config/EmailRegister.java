package com.bruce.tool.mail.config;

import org.springframework.context.annotation.ComponentScan;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 11:27 2019-02-14
 */
@ComponentScan(value = {"com.bruce.tool.mail"})
public class EmailRegister {
}
