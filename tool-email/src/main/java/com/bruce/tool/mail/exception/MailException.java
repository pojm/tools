package com.bruce.tool.mail.exception;

import com.bruce.tool.common.exception.BaseRuntimeException;
import com.bruce.tool.mail.constant.MailErrorCode;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 11:43 2019-02-20
 */
public class MailException extends BaseRuntimeException {
    public MailException(Throwable e) {
        super(e);
    }

    public MailException(String message) {
        super(MailErrorCode.MAIL_ERROR.getCode(),message);
    }
}
