package com.bruce.tool.mail;

import com.bruce.tool.office.excel.annotation.Header;
import com.bruce.tool.office.excel.constant.Format;
import lombok.Data;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * 接口实体类
 *
 * @author Bruce(刘正航)
 */
@Data
public class Interface2 {
    /**
     * 接口名称
     */
    @Header(name = "接口名称",order = "1",align = HorizontalAlignment.LEFT)
    @NotNull(message = "接口名称不能为空")
    @Length(min = 1,max = 15,message = "接口名称长度大于1")
    private String name;
    /**
     * 请求url地址
     */
    @Header(name = "请求url地址",order = "2")
    @NotNull(message = "接口地址不能为空")
    @Length(min = 10,max = 100,message = "接口地址大于10")
    private String url;
    /**
     * 请求参数json数据
     */
    @Header(name = "请求参数json数据",order = "4")
    private String params;
    /**
     * 参数请求类型：json/form
     */
    @Header(name = "参数请求类型")
    private Integer paramType;
    /**
     * 请求方式：get/post
     */
    @Header(name = "请求方式",order = "10")
    private String requestType;
    /**
     * 期望的返回结果code
     */
    @Header(name = "期望的返回结果code",order = "-1")
    private String expectCode;
    /**
     * 完整的返回结果
     */
    @Header(name = "完整的返回结果",order = "-2")
    private String result;

    @Header(name ="价格", format = Format.DECIMAL_2, regex = "[0-9]+(\\.[0-9]{0,5})?" , align = HorizontalAlignment.LEFT)
//    @ExcelUtils.Header(name ="价格", format = "0.00", regex = "[0-9]*(\\.[0-9]{0,5})?")
    private Double price;

    @Header(name = "日期", format = "yyyy-MM-dd HH:mm:ss")
    private String date;

}
