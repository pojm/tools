##### 邮件模块-说明
##### 初衷:
```
    1.方便对邮件api的使用.
    2.屏蔽掉复杂的参数设置,简化调用流程.
    3.
```
####1.配置方式:
```
spring:
  mail:
    host: smtp.beiquan.org
    username: liuzhenghang@beiquan.org
    password: bHpoMzEzOU8p
    to: 2633040924@QQ.com
    copy: 2633040924@QQ.com
    secret: 2633040924@QQ.com
```
####2.调用方式:
```
    a.依赖注入
    @Autowired
    private MailAdapter mailAdapter;
    b.在方法中调用
    mailAdapter
            .addTo("nichiailzh@163.com") // 添加收件人
            .addSubject("倍全生活项目前端进度0325") // 添加邮件主题
            .addAttachment("倍全生活项目前端进度0325",System.getProperty("user.home")+"/Desktop/test1.xls") // 添加邮件附件
            .appendToContent(true) // 是否将附件内容追加到邮件内容中(仅仅支持2003版本的excel)
            .send(); //执行邮件发送
```
