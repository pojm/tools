package com.bruce.tool.netty.socket.util;

import io.netty.buffer.ByteBuf;

import java.util.Objects;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 11:04 2019-03-10
 */
public class SocketUtils {

    public static String transfer(ByteBuf buf) {
        if(Objects.isNull(buf) ){
            return "";
        }
        String str;
        // 处理堆缓冲区
        if(buf.hasArray()) {
            str = new String(buf.array(), buf.arrayOffset() + buf.readerIndex(), buf.readableBytes());
        } else { // 处理直接缓冲区以及复合缓冲区
            byte[] bytes = new byte[buf.readableBytes()];
            buf.getBytes(buf.readerIndex(), bytes);
            str = new String(bytes, 0, buf.readableBytes());
        }
        return str;
    }
}
