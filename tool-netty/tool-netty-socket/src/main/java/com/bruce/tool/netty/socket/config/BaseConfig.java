package com.bruce.tool.netty.socket.config;

import lombok.Data;

/**
 * 功能 :
 * @author : Bruce(刘正航) 3:08 下午 2019/11/16
 */
@Data
public class BaseConfig {
    private String ping = "ping";
    private String pong = "pong";
}
