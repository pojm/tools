package com.bruce.tool.netty.socket.core;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 13:04 2019-03-10
 */
public interface MessageListener {
    void listen(String message);
}
