package com.bruce.tool.netty.socket.annotation;

import com.bruce.tool.netty.socket.config.NettySocketRegister;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 09:27 2019-01-31
 */
@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Import(NettySocketRegister.class)
public @interface UseNettySocket {
}
