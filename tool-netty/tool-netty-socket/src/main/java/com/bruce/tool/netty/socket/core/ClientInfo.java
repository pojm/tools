package com.bruce.tool.netty.socket.core;

import lombok.*;

import java.io.Serializable;

/**
 * 功能 :
 * @author : Bruce(刘正航) 10:36 下午 2019/11/15
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ClientInfo implements Serializable {
    /**客户端唯一标识**/
    @Getter@Setter
    private String clientId;
    @Getter@Setter
    private String clientIp;
    @Getter@Setter
    private Integer port;
    /**1服务器,2客户端**/
    @Getter@Setter
    private Integer clientType;
}
