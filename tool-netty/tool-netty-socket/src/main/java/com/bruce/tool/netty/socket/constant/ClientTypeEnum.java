package com.bruce.tool.netty.socket.constant;

import lombok.Getter;

/**
 * 功能 :
 * @author : Bruce(刘正航) 3:39 下午 2019/11/16
 */
public enum ClientTypeEnum {
    SERVICE(1),
    CLIENT(2),
    ;
    @Getter
    private Integer code;

    ClientTypeEnum(Integer code) {
        this.code = code;
    }

    public static ClientTypeEnum getByCode(Integer code){
        for (ClientTypeEnum clientTypeEnum : values()){
            if(clientTypeEnum.getCode().equals(code)){
                return clientTypeEnum;
            }
        }
        return null;
    }
}
