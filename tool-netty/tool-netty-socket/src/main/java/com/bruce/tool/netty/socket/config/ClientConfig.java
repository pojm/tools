package com.bruce.tool.netty.socket.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 10:32 2019-03-10
 */
@Data
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "tool.netty.socket")
public class ClientConfig extends BaseConfig{
    private String host;
    private Integer port;
    private Integer retry;
    private Integer timeout;
    /**客户端唯一标识**/
    private Integer type;
}
