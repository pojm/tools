package com.bruce.tool.netty.socket.constant;

import lombok.Getter;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 15:19 2019-02-14
 */
public enum NettyErrorCode {

    NTERROR_CONFIG_NULL("NE001","监听器未初始化"),

    ;
    @Getter
    private String code;
    @Getter
    private String message;

    NettyErrorCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

}
