package com.bruce.tool.netty.socket.core;

import lombok.*;

import java.io.Serializable;

/**
 * 功能 :
 * @author : Bruce(刘正航) 10:14 下午 2019/11/15
 */
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Message implements Serializable {
    @Getter@Setter
    private ClientInfo from;
    @Getter@Setter
    private ClientInfo to;
    @Getter@Setter
    private String body;
}
