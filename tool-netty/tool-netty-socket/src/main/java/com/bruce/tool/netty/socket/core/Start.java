package com.bruce.tool.netty.socket.core;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 10:37 2019-03-10
 */
public interface Start {
    void run() throws Exception;
}
