package com.bruce.tool.netty.socket;

import com.bruce.tool.common.exception.ExceptionUtils;
import com.bruce.tool.common.util.LogUtils;
import com.bruce.tool.netty.socket.core.SocketClient;
import com.bruce.tool.netty.socket.core.SocketServer;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 11:13 2019-03-10
 */
@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NettySocketApplication.class)
public class SocketTest {

    @Autowired
    SocketServer server;

    @Autowired
    SocketClient client;

    private ExecutorService service = Executors.newFixedThreadPool(2);

    // Junit跑线程的2个方式:方式1
    @Test
    public void server() {
        service.execute(() -> {
            try {
                server.run();
                log.info("执行成功");
            } catch (Exception e) {
                ExceptionUtils.printStackTrace(e);
            }
        });
        try {
            Thread.currentThread().join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private CountDownLatch countDownLatch = new CountDownLatch(1);

    public class RunableTask implements Runnable {
        private CountDownLatch countDownLatch;
        public RunableTask(CountDownLatch countDownLatch) {
            this.countDownLatch = countDownLatch;
        }
        @Override
        public void run() {
            try {
                server.run();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                countDownLatch.countDown();
            }
        }
    }

    // Junit跑线程的2个方式:方式2
    @Test
    public void server2(){
        service.execute(new RunableTask(countDownLatch));
        try {
            countDownLatch.await();
            log.info("执行成功");
        } catch (InterruptedException e) {
            ExceptionUtils.printErrorInfo(e);
        }
    }

    /**客户端的服务者**/
    @Test
    public void clientServer() {
        service.execute(() -> {
            client.addListener(message -> {
                LogUtils.info(log,"收到消息反馈:{}",message);
            });
            client.run();
        });
        try {
            Thread.currentThread().join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
