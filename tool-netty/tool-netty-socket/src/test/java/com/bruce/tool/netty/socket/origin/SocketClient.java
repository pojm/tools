package com.bruce.tool.netty.socket.origin;

import com.bruce.tool.common.exception.ExceptionUtils;
import com.bruce.tool.common.util.file.IOUtils;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

@Slf4j
public class SocketClient {
 
	public static void main(String[] args){
		Socket s = null;
		OutputStream os = null;
		try {
			s = new Socket("127.0.0.1", 10001);
			os = s.getOutputStream();
			os.write("Can you read me?".getBytes());

			InputStream is = s.getInputStream();
			byte[] buf = new byte[1000];
			int length = is.read(buf);

			log.info("Information from " + s.getLocalAddress().getHostAddress() + " : " + new String(buf, 0, length));

			s.close();
		} catch (IOException e) {
			ExceptionUtils.printErrorInfo(e);
		} finally {
			IOUtils.closeQuietly(os,s);
		}
	}
}