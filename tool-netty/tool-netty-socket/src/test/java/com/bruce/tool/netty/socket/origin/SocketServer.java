package com.bruce.tool.netty.socket.origin;

import com.bruce.tool.common.exception.ExceptionUtils;
import com.bruce.tool.common.util.file.IOUtils;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

@Slf4j
public class SocketServer {
 
	public static void main(String[] args){
		ServerSocket ss = null;
		Socket s = null;
		InputStream is = null;
		try {
			ss = new ServerSocket(10001);

			s = ss.accept();
			is = s.getInputStream();
			byte[] buf = new byte[1000];
			int length = is.read(buf);

			log.info("Information from " + s.getLocalAddress().getHostAddress() + " : " + new String(buf, 0, length));

			OutputStream os = s.getOutputStream();
			os.write("Very clear.".getBytes());

			s.close();
		} catch (IOException e) {
			ExceptionUtils.printErrorInfo(e);
		} finally {
			IOUtils.closeQuietly(is,s,ss);
		}
	}

}