package com.bruce.tool.netty.socket.netty.chat3;

import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * 功能 :
 *
 * @author : Bruce(刘正航) 下午10:54 2018/8/19
 */
public class QueueHolder {

    private static ConcurrentLinkedQueue<String> queue = new ConcurrentLinkedQueue<>();

    public static void push(String message){
        queue.offer(message);
    }

    public static String pop(){
        return queue.poll();
    }
}